<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

?>

<div class="seo-view">
    <h2>Метатеги и заголовок страницы</h2>
     <div class="col-md-12  seoimage">
        <?php echo  Html::img($model->getImageUrl(),['style' => 'width:100%;']); ?>     
    </div>
    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'slug',
            'title_ru',
            'keywords_ru',
            'description_ru:html',
            
        ],
    ]) ?>
    
    <p class="text-center">
        <?php echo Html::a(Yii::t('backend', 'Обновить'), ['/seo/update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
    
</div>
