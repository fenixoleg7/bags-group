<?php
namespace backend\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\Widget;
use common\models\Seo;

/**
 * Class Menu
 * @package backend\components\widget
 */
class SeoWidget extends Widget
{
    /**
     * @var string text block key
     */
    public $key;

    /**
     * @return string
     */
    public function run()
    {
        
        $model =  Seo::findOne(['id' => $this->key]);
        if ($model) {
            return $this->render('seowidget', [
                'model' => $model,
            ]);
        }
        else {
            $content="SEO параметры для данной страницы отсутствуют";
            return $content;
        }
        
    }
    
}
