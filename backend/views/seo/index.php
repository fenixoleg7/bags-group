<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SeoSeorch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Seos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a(Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Seo',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'slug',
            'title_ru',
            'description_ru:ntext',
            'keywords_ru',
            // 'text:ntext',
            // 'thumbnail_base_url:url',
            // 'thumbnail_path',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
