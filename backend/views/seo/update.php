<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Seo */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Seo',
]) . ' ' . $model->title_ru;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Seos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title_ru, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="seo-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
