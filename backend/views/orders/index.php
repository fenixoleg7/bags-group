<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   
<?php Pjax::begin(); ?>    
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'product_id',
            'product_name',
            'product_kod',
            'product_prise',
            'product_quantity',
            'total_cost',
            [
                
                'attribute' => 'currency',
                'format' => 'html',
                //'label' => 'Name',
            ],
            //'currency',
            'client_name',
            'client_phone',
            'client_email:email',
            [
                'class'=>\common\grid\EnumColumn::className(),
                'label'=>'Зааказ',
                'attribute'=>'status',
                'enum'=>[
                    Yii::t('backend', 'Новый'),
                    Yii::t('backend', 'Принятый')]
            ],
            //'client_message:ntext',
            // 'status',
            // 'created_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}'
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
