<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CatalogCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Catalog Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Catalog Category'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'thumbnail_path',
                'format' => 'raw',
                'label' => 'Баннер',
                'value' => function ($model) {
                    return $model->thumbnail_path ? Html::img($model->getImageUrl(), ['style'=>'width: 50%']) : null;
                }
            ],
            'slug',
            'title_ru',
            //'title_en',
            //'title_de',
            // 'keywords_ru',
            // 'keywords_de',
            // 'keywords_en',
            // 'description_ru',
            // 'description_en',
            // 'description_de',
            // 'body_ru:ntext',
            // 'body_en:ntext',
            // 'body_de:ntext',
             [
                'attribute' => 'parent_id',
                'value'=>function ($model) {
                    return $model->parent ? $model->parent->title_ru : null;
                },
                'filter'=>\yii\helpers\ArrayHelper::map(\common\models\CatalogCategory::find()->all(), 'id', 'title_ru')
                ],
            // 'thumbnail_base_url:url',
            // 'thumbnail_path',
            // 'status',
            // 'created_at',
            // 'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update} {delete}'
            ]
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
