<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Products'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'thumbnail_path_1',
                'format' => 'raw',
                'label' => 'Баннер',
                'value' => function ($model) {
                    return $model->thumbnail_path_1 ? Html::img($model->getImageUrl(), ['style'=>'width: 100%']) : null;
                }
            ],
           
            'slug',
            'title_ru',
            //'title_en',
            //'title_de',
            // 'keywords_ru',
            // 'keywords_de',
            // 'keywords_en',
            // 'description_ru',
            // 'description_en',
            // 'description_de',
            // 'body_ru:ntext',
            // 'body_en:ntext',
            // 'body_de:ntext',
             'kod',
            // 'size',
            // 'capacity',
            // 'price_1',
            // 'price_2',
            // 'price_3',
            // 'price_4',
            // 'price_5',
             [
                'attribute' => 'categoty_id',
                'value'=>function ($model) {
                    return $model->category ? $model->category->title_ru : null;
                },
                'filter'=>\yii\helpers\ArrayHelper::map(\common\models\CatalogCategory::find()->all(), 'id', 'title_ru')
                ],
            // 'thumbnail_base_url:url',
             
            // 'thumbnail_path_2',
            // 'thumbnail_path_3',
            // 'thumbnail_path_4',
            // 'thumbnail_path_5',
            // 'thumbnail_path_6',
            // 'updater_id',
             //'status',


            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update} {delete}'
            ]
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
