<?php

use trntv\filekit\widget\Upload;
use trntv\yii\datetime\DateTimeWidget;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;


/* @var $this yii\web\View */
/* @var $model common\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(['id' => 'blog-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
      <li class="active"><a href="#ru" data-toggle="tab">RU</a></li>
      <li><a href="#en" data-toggle="tab">EN</a></li>
      <li><a href="#de" data-toggle="tab">DE</a></li>
      <li><a href="#pl" data-toggle="tab">PL</a></li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane fade in active" id="ru">

       <?php echo $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

          <?php echo $form->field($model, 'keywords_ru')->textInput(['maxlength' => true]) ?>

          <?php echo $form->field($model, 'description_ru')->textInput(['maxlength' => true]) ?>

          <?php /*echo $form->field($model, 'body_ru')->widget(
                \yii\imperavi\Widget::className(),
                [
                    'plugins' => ['fullscreen', 'fontcolor', 'video'],
                    'options' => [
                        'minHeight' => 400,
                        'maxHeight' => 400,
                        'buttonSource' => true,
                        'convertDivs' => false,
                        'removeEmptyTags' => false,
                        'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                    ]
                ]
            ) */
             echo $form->field($model, 'body_ru')->widget(TinyMce::className(), [
                'options' => ['rows' => 6],
                'language' => 'ru',
                'clientOptions' => [
                    'plugins' => [
                        'advlist autolink lists link charmap hr preview pagebreak',
                        'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                        'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                    ],
                    'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                    'external_filemanager_path' => '/admin/plugins/responsivefilemanager/filemanager/',
                    'filemanager_title' => 'Responsive Filemanager',
                    'external_plugins' => [
                        //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                        'filemanager' => '/admin/plugins/responsivefilemanager/filemanager/plugin.min.js',
                        //Иконка/кнопка загрузки файла в панеле иснструментов.
                        'responsivefilemanager' => '/admin/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                    ],        
                ]
            ]);
            ?>
      </div>
      <div class="tab-pane fade" id="en">

          <?php echo $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

          <?php echo $form->field($model, 'keywords_en')->textInput(['maxlength' => true]) ?>

          <?php echo $form->field($model, 'description_en')->textInput(['maxlength' => true]) ?>

          <?php /*echo $form->field($model, 'body_en')->widget(
               \yii\imperavi\Widget::className(),
                [
                    'plugins' => ['fullscreen', 'fontcolor', 'video'],
                    'options' => [
                        'minHeight' => 400,
                        'maxHeight' => 400,
                        'buttonSource' => true,
                        'convertDivs' => false,
                        'removeEmptyTags' => false,
                        'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                    ]
                ]
            ) */
                 echo $form->field($model, 'body_en')->widget(TinyMce::className(), [
                'options' => ['rows' => 6],
                'language' => 'ru',
                'clientOptions' => [
                    'plugins' => [
                        'advlist autolink lists link charmap hr preview pagebreak',
                        'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                        'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                    ],
                    'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                    'external_filemanager_path' => '/admin/plugins/responsivefilemanager/filemanager/',
                    'filemanager_title' => 'Responsive Filemanager',
                    'external_plugins' => [
                        //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                        'filemanager' => '/admin/plugins/responsivefilemanager/filemanager/plugin.min.js',
                        //Иконка/кнопка загрузки файла в панеле иснструментов.
                        'responsivefilemanager' => '/admin/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                    ],        
                ]
            ]);
            ?>
      </div>
      <div class="tab-pane fade" id="de">
          
          <?php echo $form->field($model, 'title_de')->textInput(['maxlength' => true]) ?>

          <?php echo $form->field($model, 'keywords_de')->textInput(['maxlength' => true]) ?>

          <?php echo $form->field($model, 'description_de')->textInput(['maxlength' => true]) ?>

          <?php /*echo $form->field($model, 'body_de')->widget(
                \yii\imperavi\Widget::className(),
                [
                    'plugins' => ['fullscreen', 'fontcolor', 'video'],
                    'options' => [
                        'minHeight' => 400,
                        'maxHeight' => 400,
                        'buttonSource' => true,
                        'convertDivs' => false,
                        'removeEmptyTags' => false,
                        'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                    ]
                ]
            ) */
             echo $form->field($model, 'body_de')->widget(TinyMce::className(), [
                'options' => ['rows' => 6],
                'language' => 'ru',
                'clientOptions' => [
                    'plugins' => [
                        'advlist autolink lists link charmap hr preview pagebreak',
                        'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                        'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                    ],
                    'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                    'external_filemanager_path' => '/admin/plugins/responsivefilemanager/filemanager/',
                    'filemanager_title' => 'Responsive Filemanager',
                    'external_plugins' => [
                        //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                        'filemanager' => '/admin/plugins/responsivefilemanager/filemanager/plugin.min.js',
                        //Иконка/кнопка загрузки файла в панеле иснструментов.
                        'responsivefilemanager' => '/admin/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                    ],        
                ]
            ]);
            ?>

      </div>
      <div class="tab-pane fade" id="pl">
          
          <?php echo $form->field($model, 'title_pl')->textInput(['maxlength' => true]) ?>

          <?php echo $form->field($model, 'keywords_pl')->textInput(['maxlength' => true]) ?>

          <?php echo $form->field($model, 'description_pl')->textInput(['maxlength' => true]) ?>

          <?php /*echo $form->field($model, 'body_de')->widget(
                \yii\imperavi\Widget::className(),
                [
                    'plugins' => ['fullscreen', 'fontcolor', 'video'],
                    'options' => [
                        'minHeight' => 400,
                        'maxHeight' => 400,
                        'buttonSource' => true,
                        'convertDivs' => false,
                        'removeEmptyTags' => false,
                        'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                    ]
                ]
            ) */
             echo $form->field($model, 'body_pl')->widget(TinyMce::className(), [
                'options' => ['rows' => 6],
                'language' => 'ru',
                'clientOptions' => [
                    'plugins' => [
                        'advlist autolink lists link charmap hr preview pagebreak',
                        'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                        'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                    ],
                    'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                    'external_filemanager_path' => '/admin/plugins/responsivefilemanager/filemanager/',
                    'filemanager_title' => 'Responsive Filemanager',
                    'external_plugins' => [
                        //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                        'filemanager' => '/admin/plugins/responsivefilemanager/filemanager/plugin.min.js',
                        //Иконка/кнопка загрузки файла в панеле иснструментов.
                        'responsivefilemanager' => '/admin/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                    ],        
                ]
            ]);
            ?>

      </div>
    </div>

     <?php echo $form->field($model, 'slug')
        ->hint(Yii::t('backend', 'If you\'ll leave this field empty, slug will be generated automatically'))
        ->textInput(['maxlength' => true]) ?>
        
    <div class="row">
        <div class="col-md-4 col-sm-4">
            <?= $form->field($model, 'kod')->textInput() ?>
        </div>
        <div class="col-md-4 col-sm-4">
            <?= $form->field($model, 'size')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-sm-4">
            <?= $form->field($model, 'capacity')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12"><h3><?= Yii::t('backend', 'Count/Price'); ?></h3></div>
    </div>
        <div class="row">
        <div class="col-md-4 col-sm-4">
             <?= $form->field($model, 'price_1')->textInput() ?>
             <?= $form->field($model, 'price_2')->textInput() ?>
        </div>
        <div class="col-md-4 col-sm-4">
            <?= $form->field($model, 'price_3')->textInput() ?>
            <?= $form->field($model, 'price_4')->textInput() ?>
        </div>
        <div class="col-md-4 col-sm-4">
            <?= $form->field($model, 'price_5')->textInput() ?>
        </div>
    </div>
    
    
    
     <div class="row">
            <div class="col-md-2 col-sm-2">
                <?php echo $form->field($model, 'slide_1')->widget(
                    Upload::className(),
                    [
                        'url' => ['/file-storage/upload'],
                        'maxFileSize' => 5000000, // 5 MiB
                        
                    ]);
                ?>
            </div>
            <div class="col-md-2 col-sm-2">
                <?php echo $form->field($model, 'slide_2')->widget(
                    Upload::className(),
                    [
                        'url' => ['/file-storage/upload'],
                        'maxFileSize' => 5000000, // 5 MiB
                    ]);
                ?>
            </div>
            <div class="col-md-2 col-sm-2">
                <?php echo $form->field($model, 'slide_3')->widget(
                    Upload::className(),
                    [
                        'url' => ['/file-storage/upload'],
                        'maxFileSize' => 5000000, // 5 MiB
                    ]);
                ?>
            </div>
            <div class="col-md-2 col-sm-2">
                <?php echo $form->field($model, 'slide_4')->widget(
                    Upload::className(),
                    [
                        'url' => ['/file-storage/upload'],
                        'maxFileSize' => 5000000, // 5 MiB
                    ]);
                ?>
            </div>
            <div class="col-md-2 col-sm-2">
                <?php echo $form->field($model, 'slide_5')->widget(
                    Upload::className(),
                    [
                        'url' => ['/file-storage/upload'],
                        'maxFileSize' => 5000000, // 5 MiB
                    ]);
                ?>
            </div>
            <div class="col-md-2 col-sm-2">
                <?php echo $form->field($model, 'slide_6')->widget(
                    Upload::className(),
                    [
                        'url' => ['/file-storage/upload'],
                        'maxFileSize' => 5000000, // 5 MiB
                    ]);
                ?>
            </div>
        </div>
    
    <?php echo $form->field($model, 'category_id')->dropDownList(\yii\helpers\ArrayHelper::map(
            $categories,
            'id',
            'title_ru'
        ), ['prompt'=>'']) ?>

    <?php echo $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
