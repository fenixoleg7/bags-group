<?php
/**
 * @author Eugene Terentev <eugene@terentev.net>
 * @var $model common\models\TimelineEvent
 */
?>
<div class="timeline-item">
    <span class="time">
        <i class="fa fa-clock-o"></i>
        <?php echo Yii::$app->formatter->asRelativeTime($model->created_at) ?>
    </span>

    <h3 class="timeline-header bg-danger">
        <?php echo Yii::t('backend', 'You have new order!') ?>
    </h3>

    <div class="timeline-body">
        <?php echo Yii::t('backend', 'New order №{identity} was registered at {created_at}', [
            'identity' => $model->data['product_kod'],
            'created_at' => Yii::$app->formatter->asDatetime($model->data['created_at'])
        ]) ?>
    </div>
    <dl class="dl-horizontal">
      <dt><?php echo Yii::t('backend','Username') ?></dt><dd><?php echo $model->data['client_name']; ?></dd>
      <dt><?php echo Yii::t('backend','Product') ?></dt><dd><?php echo $model->data['name']; ?></dd>
      <dt><?php echo Yii::t('backend','Product Quantity') ?></dt><dd><?php echo $model->data['quantity']; ?></dd>
      <dt><?php echo Yii::t('backend','Total Cost') ?></dt><dd><?php echo $model->data['total_cost'].$model->data['currency'] ?></dd>
    </dl>

    <div class="timeline-footer text-center">
        <?php echo \yii\helpers\Html::a(
            Yii::t('backend', 'View order'),
            ['/orders/view', 'id' => $model->data['id']],
            ['class' => 'btn btn-success btn-sm']
        ) ?>
    </div>
</div>