<?php
use yii\i18n\Formatter;
/**
 * @author Eugene Terentev <eugene@terentev.net>
 * @var $model common\models\TimelineEvent
 */
?>
<div class="timeline-item">
    <span class="time">
        <i class="fa fa-clock-o"></i>
        <?php echo Yii::$app->formatter->asRelativeTime($model->created_at) ?>
    </span>

    <h3 class="timeline-header bg-info">
        <?php echo Yii::t('backend', 'You have new message!') ?>
    </h3>

    <div class="timeline-body">
        <?php echo Yii::t('backend', 'New message') ?>
    </div>
    <dl class="dl-horizontal">
      <dt><?php echo Yii::t('backend','Username') ?></dt><dd><?php echo $model->data['name']; ?></dd>
      <dt><?php echo Yii::t('backend','E-mail') ?></dt><dd><?php echo $model->data['email']; ?></dd>
      <dt><?php echo Yii::t('backend','Phone') ?></dt><dd><?php echo $model->data['phone']; ?></dd>
      <dt><?php echo Yii::t('backend','Message') ?></dt><dd><p style="word-wrap: break-word;"><?php echo $model->data['message']; ?></p></dd>
    </dl>

    
</div>