<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Widgets */

$this->title = Yii::t('backend', 'Создание {modelClass}', [
    'modelClass' => 'Widgets',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Виджеты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widgets-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
