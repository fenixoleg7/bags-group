<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Widgets */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Widgets',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Widgets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="widgets-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
