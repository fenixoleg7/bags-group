<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Widgets */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="widgets-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <div class="col-md-12">
            <?php echo $form->field($model, 'status')->checkbox() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?php echo $form->field($model, 'name')->dropDownList([
                'Новые обзоры' => 'Новые обзоры',
                'Последние новости' => 'Последние новости',
                'Популярные обзоры' => 'Популярные обзоры ',
                'Популярные новости' => 'Популярные новости ',
            ],
            [
                'prompt' => 'Выберите один вариант'
            ]); ?> 
        </div>
        <div class="col-md-4">
            <?php echo $form->field($model, 'page')->dropDownList([
                '1' => 'Главная страница',
                '2' => 'Все новости',
                '3' => 'Страницы новостей',
                '4' => 'Игровые обзоры',
                '5' => 'Страницы обзоров',
                '6' => 'Страница поиска',
            ],
            [
                'prompt' => 'Выберите один вариант'
            ]); ?>
        </div>
        <div class="col-md-4">
            <?php echo $form->field($model, 'quantity')->dropDownList([
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
            ],
            [
                'prompt' => 'Выберите один вариант'
            ]); ?>
        </div>
        
    </div>
    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
