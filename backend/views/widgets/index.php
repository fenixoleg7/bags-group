<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\WidgetsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Менеджер виджетов');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widgets-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a(Yii::t('backend', 'Создать {modelClass}', [
    'modelClass' => 'Widgets',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'class'=>\common\grid\EnumColumn::className(),
                'attribute'=>'page',
                'enum'=>[
                    Yii::t('backend', 'Нет размещения'),
                    Yii::t('backend', 'Главная страница'),
                    Yii::t('backend', 'Все новости'),
                    Yii::t('backend', 'Страницы новостей'),
                    Yii::t('backend', 'Игровые обзоры'),
                    Yii::t('backend', 'Страницы обзоров'),
                    Yii::t('backend', 'Страница поиска'),
                ]
            ],
            'quantity',
            [
                'class'=>\common\grid\EnumColumn::className(),
                'attribute'=>'status',
                'enum'=>[
                    Yii::t('backend', 'Не опубликовано'),
                    Yii::t('backend', 'Опубликовано')
                ]
            ],
            // 'created_at',
            // 'updated_at',

            [
            'class' => 'yii\grid\ActionColumn',
            'template'=>'{update} {delete}'
            ],
        ],
    ]); ?>

</div>
