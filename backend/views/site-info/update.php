<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteInfo */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Site Info',
]);

?>
<div class="site-info-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
