<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SiteInfo */

$this->title = Yii::t('backend','Site info');

?>
<div class="site-info-view">

    <h1><?= Yii::t('backend','Site info'); ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name_site',
            'name_company',
            'adress:raw',
            'email:email',
            'phone_1',
            'phone_2',
            'fax',
            'skype_url:url',
            'linkedIn_url:url',
            'Facebook_url:url',
            'xing_url:url',
    
            [
                'attribute' => 'logo_heder_path',
                'value' =>$model->getHederLogo(),
                'format' => ['image',['width'=>'100','height'=>'100']],
                    
            ],
            [
                'attribute' => 'logo_footer_path',
                'value' =>$model->getFooterLogo(),
                'format' => ['image',['width'=>'100','height'=>'100']],
                    
            ],
            'currency_1',
            'currency_2'
        ],
    ]) ?>

</div>

<style>
    .table-striped > tbody > tr:nth-of-type(odd) {
    background-color: #84BDCE;
}
</style>