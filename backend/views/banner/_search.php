<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SearchBanner */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="banner-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php echo $form->field($model, 'id') ?>

    <?php echo $form->field($model, 'base_url') ?>

    <?php echo $form->field($model, 'path') ?>

    <?php echo $form->field($model, 'type') ?>

    <?php echo $form->field($model, 'url') ?>

    <?php  echo $form->field($model, 'name_ru') ?>

    <?php // echo $form->field($model, 'caption') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'page') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
