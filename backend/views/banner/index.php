<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchBanner */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баннеры';
$this->params['breadcrumbs'][] = $this->title;
$page_ar = array(
    '0' => 'Главная страница сайта',
    '1' => 'Главная страница новостей',
    '2' => 'На странице новостей',
    '3' => 'Главная страница всех обзоров',
    '4' => 'На странице обзора',
)
?>
<div class="banner-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Создание Баннера', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'path',
                'format' => 'raw',
                'label' => 'Баннер',
                'value' => function ($model) {
                    return $model->path ? Html::img($model->getImageUrl(), ['style'=>'width: 100%']) : null;
                }
            ],
            'name_ru',
            [
                'format' => 'html',
                'attribute' => 'caption_ru',
                'options' => ['style' => 'width: 20%']
            ],
            [
                'class'=>\common\grid\EnumColumn::className(),
                'attribute'=>'status',
                'enum'=>[
                    Yii::t('backend', 'Не опубликовано'),
                    Yii::t('backend', 'Опубликовано')]
            ],
            [
                'class'=>\common\grid\EnumColumn::className(),
                'attribute'=>'page',
                'enum'=>[
                    Yii::t('backend', 'Главная страница сайта'),
                    Yii::t('backend', 'Главная страница новостей'),
                    Yii::t('backend', 'На странице новостей'),
                    Yii::t('backend', 'Главная страница всех обзоров'),
                    Yii::t('backend', 'На странице обзора')]
                   
            ],
            

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>

</div>
