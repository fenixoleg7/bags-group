<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Banner */

$this->title = 'Создание банера';
$this->params['breadcrumbs'][] = ['label' => 'Банеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
