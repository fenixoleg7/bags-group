<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use trntv\filekit\widget\Upload;

/* @var $this yii\web\View */
/* @var $model app\models\Banner */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="banner-form">

    
    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-12"><?php echo $form->errorSummary($model); ?></div>
        </div>
        <div class="row">
         <div class="col-md-12"> 
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
              <li class="active"><a href="#ru" data-toggle="tab">RU</a></li>
              <li><a href="#en" data-toggle="tab">EN</a></li>
              <li><a href="#de" data-toggle="tab">DE</a></li>
              <li><a href="#pl" data-toggle="tab">PL</a></li>
            </ul>
            
            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane fade in active" id="ru">

                    <?php echo $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

                    <?php echo $form->field($model, 'caption_ru')->widget(\yii\imperavi\Widget::className(),
                        [
                            'plugins' => ['fullscreen', 'fontcolor', 'video'],
                            'options' => [
                                'minHeight' => 200,
                                'maxHeight' => 200,
                                'buttonSource' => true,
                                'convertDivs' => false,
                                'removeEmptyTags' => false,
                                'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                            ]
                        ]
                    ) ?>

                    <?php echo $form->field($model, 'image')->widget(
                        Upload::className(),
                        [
                            'url' => ['/file-storage/upload'],
                            'maxFileSize' => 5000000, // 5 MiB
                            
                        ]);
                    ?>
              </div>
              <div class="tab-pane fade" id="en">

                    <?php echo $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

                    <?php echo $form->field($model, 'caption_en')->widget(\yii\imperavi\Widget::className(),
                        [
                            'plugins' => ['fullscreen', 'fontcolor', 'video'],
                            'options' => [
                                'minHeight' => 200,
                                'maxHeight' => 200,
                                'buttonSource' => true,
                                'convertDivs' => false,
                                'removeEmptyTags' => false,
                                'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                            ]
                        ]
                    ) ?>
                   
                    <?php echo $form->field($model, 'image_en')->widget(
                    Upload::className(),
                    [
                        'url' => ['/file-storage/upload'],
                        'maxFileSize' => 5000000, // 5 MiB
                        
                    ]);
                ?>
              </div>
              <div class="tab-pane fade" id="de">
                    <?php echo $form->field($model, 'name_de')->textInput(['maxlength' => true]) ?>

                    <?php echo $form->field($model, 'caption_de')->widget(\yii\imperavi\Widget::className(),
                        [
                            'plugins' => ['fullscreen', 'fontcolor', 'video'],
                            'options' => [
                                'minHeight' => 200,
                                'maxHeight' => 200,
                                'buttonSource' => true,
                                'convertDivs' => false,
                                'removeEmptyTags' => false,
                                'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                            ]
                        ]
                    ) ?>
                    <?php echo $form->field($model, 'image_de')->widget(
                        Upload::className(),
                        [
                            'url' => ['/file-storage/upload'],
                            'maxFileSize' => 5000000, // 5 MiB
                            
                        ]);
                    ?>
               </div>
               <div class="tab-pane fade" id="pl">
                    <?php echo $form->field($model, 'name_pl')->textInput(['maxlength' => true]) ?>

                    <?php echo $form->field($model, 'caption_pl')->widget(\yii\imperavi\Widget::className(),
                        [
                            'plugins' => ['fullscreen', 'fontcolor', 'video'],
                            'options' => [
                                'minHeight' => 200,
                                'maxHeight' => 200,
                                'buttonSource' => true,
                                'convertDivs' => false,
                                'removeEmptyTags' => false,
                                'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                            ]
                        ]
                    ) ?>
                    <?php echo $form->field($model, 'image_pl')->widget(
                        Upload::className(),
                        [
                            'url' => ['/file-storage/upload'],
                            'maxFileSize' => 5000000, // 5 MiB
                            
                        ]);
                    ?>
               </div>
            </div>

            <?php echo $form->field($model, 'page')->dropDownList([
                '0' => 'Главная страница сайта',
                '1' => 'Главная страница новостей',
                '2' => 'На странице новостей',
                '3' => 'Главная страница всех продуктов',
                '4' => 'На странице продукта',
            ]);?>

            <?php echo $form->field($model, 'status')->checkbox() ?>

        
       
             
        
            <div class="form-group">
                <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
         </div> 
        </div>
   

    <?php ActiveForm::end(); ?>

</div>
