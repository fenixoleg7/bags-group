<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Banner */

$this->title = 'Редактирования баннера: ' . ' ' . $model->name_ru;
$this->params['breadcrumbs'][] = ['label' => 'Банеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_ru, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="banner-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
