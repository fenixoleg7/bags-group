<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\WidgetHomesliderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="widget-homeslider-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'path1') ?>

    <?= $form->field($model, 'path2') ?>

    <?= $form->field($model, 'path3') ?>

    <?= $form->field($model, 'path4') ?>

    <?php // echo $form->field($model, 'caption_ru_1') ?>

    <?php // echo $form->field($model, 'caption_ru_2') ?>

    <?php // echo $form->field($model, 'caption_en_1') ?>

    <?php // echo $form->field($model, 'caption_en_2') ?>

    <?php // echo $form->field($model, 'caption_de_1') ?>

    <?php // echo $form->field($model, 'caption_de_2') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'order') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
