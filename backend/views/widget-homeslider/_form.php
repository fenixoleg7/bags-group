<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use trntv\filekit\widget\Upload;

/* @var $this yii\web\View */
/* @var $model common\models\WidgetHomeslider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="widget-homeslider-form conteiner" >

    <?php $form = ActiveForm::begin(); ?>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
      <li class="active"><a href="#ru" data-toggle="tab">RU</a></li>
      <li><a href="#en" data-toggle="tab">EN</a></li>
      <li><a href="#de" data-toggle="tab">DE</a></li>
      <li><a href="#pl" data-toggle="tab">PL</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane fade in active" id="ru">

          <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'caption_ru_1')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'caption_ru_2')->textInput(['maxlength' => true]) ?>

      </div>
      <div class="tab-pane fade" id="en">

          <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'caption_en_1')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'caption_en_2')->textInput(['maxlength' => true]) ?>

      </div>
      <div class="tab-pane fade" id="de">

          <?= $form->field($model, 'title_de')->textInput(['maxlength' => true]) ?>
          
          <?= $form->field($model, 'caption_de_1')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'caption_de_2')->textInput(['maxlength' => true]) ?>

      </div>
      <div class="tab-pane fade" id="pl">

          <?= $form->field($model, 'title_pl')->textInput(['maxlength' => true]) ?>
          
          <?= $form->field($model, 'caption_pl_1')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'caption_pl_2')->textInput(['maxlength' => true]) ?>

      </div>
    </div>
        <div class="row">
          <div class="col-md-12">
            <?= $form->field($model, 'icon_span')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <?php echo $form->field($model, 'slide_1')->widget(
                    Upload::className(),
                    [
                        'url' => ['/file-storage/upload'],
                        'maxFileSize' => 5000000, // 5 MiB
                        
                    ]);
                ?>
            </div>
           <!--  <div class="col-md-3 col-sm-3">
               <?php //echo $form->field($model, 'slide_2')->widget(
                   //Upload::className(),
                   //[
                  //     'url' => ['/file-storage/upload'],
                   //    'maxFileSize' => 5000000, // 5 MiB
                   //]);
               ?>
           </div>
           <div class="col-md-3 col-sm-3">
               <?php //echo $form->field($model, 'slide_3')->widget(
                  // Upload::className(),
                 //  [
                 //     'url' => ['/file-storage/upload'],
                 //      'maxFileSize' => 5000000, // 5 MiB
                 //  ]);
               ?>
           </div> -->
            <div class="col-md-3 col-sm-3">
                <?php echo $form->field($model, 'slide_4')->widget(
                    Upload::className(),
                    [
                        'url' => ['/file-storage/upload'],
                        'maxFileSize' => 5000000, // 5 MiB
                    ]);
                ?>
            </div>
        </div>
   
    
   
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
