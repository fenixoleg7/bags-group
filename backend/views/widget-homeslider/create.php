<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WidgetHomeslider */

$this->title = Yii::t('backend', 'Create Widget Homeslider');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Widget Homesliders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widget-homeslider-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
