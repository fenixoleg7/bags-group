<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\WidgetHomesliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Widget Homesliders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widget-homeslider-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Widget Homeslider'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title_ru',
            [
                'attribute' => 'path1',
                'format' => 'raw',
                'label' => Yii::t('common', 'Path1'),
                'value' => function ($model) {
                    return $model->path1 ? Html::img($model->getImageUrl($model->path1), ['style'=>'width: 100%']) : null;
                }
            ],
          /* [
                'attribute' => 'path2',
                'format' => 'raw',
                'label' => Yii::t('common', 'Path1'),
                'value' => function ($model) {
                    return $model->path2 ? Html::img($model->getImageUrl($model->path2), ['style'=>'width: 100%']) : null;
                }
            ],
            [
                'attribute' => 'path3',
                'format' => 'raw',
                'label' => Yii::t('common', 'Path3'),
                'value' => function ($model) {
                    return $model->path3 ? Html::img($model->getImageUrl($model->path3), ['style'=>'width: 100%']) : null;
                }
            ],*/
            [
                'attribute' => 'path4',
                'format' => 'raw',
                'label' => Yii::t('common', 'Path4'),
                'value' => function ($model) {
                    return $model->path4 ? Html::img($model->getImageUrl($model->path4), ['style'=>'width: 100%']) : null;
                }
            ],
            


            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
