<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use trntv\filekit\widget\Upload;

/* @var $this yii\web\View */
/* @var $model common\models\WidgetWorkSchema */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="widget-work-schema-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
      <li class="active"><a href="#ru" data-toggle="tab">RU</a></li>
      <li><a href="#en" data-toggle="tab">EN</a></li>
      <li><a href="#de" data-toggle="tab">DE</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane fade in active" id="ru">

          <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'text_ru')->textarea(['rows' => 6]) ?>

      </div>
      <div class="tab-pane fade" id="en">

          <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'text_en')->textarea(['rows' => 6]) ?>

      </div>
      <div class="tab-pane fade" id="de">

          <?= $form->field($model, 'title_de')->textInput(['maxlength' => true]) ?>
          
          <?= $form->field($model, 'text_de')->textarea(['rows' => 6]) ?>

      </div>
    </div>
    
        <div class="row">
            <div class="col-md-offset-5 col-md-2">
                <?php echo $form->field($model, 'image')->widget(
                    Upload::className(),
                    [
                        'url' => ['/file-storage/upload'],
                        'maxFileSize' => 5000000, // 5 MiB
                        
                    ]);
                ?>
            </div>
           
        </div>
   

    <?= $form->field($model, 'step')->textInput() ?>

    

    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
