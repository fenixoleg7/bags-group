<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WidgetWorkSchema */

$this->title = Yii::t('backend', 'Create Widget Work Schema');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Widget Work Schemas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widget-work-schema-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
