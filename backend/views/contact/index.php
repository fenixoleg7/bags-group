<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Офисы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-index">

  
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить офис', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'thumbnail_path',
                'format' => 'raw',
                'label' => 'Иконка',
                'value' => function ($model) {
                    return $model->thumbnail_path ? Html::img($model->getImageUrl($model->thumbnail_path), ['style'=>'width: 100%']) : null;
                }
            ],
            'body_ru:ntext',
            
            // 'thumbnail_base_url:url',
            // 'thumbnail_path',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
