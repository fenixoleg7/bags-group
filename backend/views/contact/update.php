<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Contact */

$this->title = Yii::t('frontend', 'Обновить ', [
    'modelClass' => 'Contact',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Офис', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="contact-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
