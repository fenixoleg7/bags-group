<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WidgetAdvantages */

$this->title = Yii::t('backend', 'Create Widget Advantages');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Widget Advantages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widget-advantages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
