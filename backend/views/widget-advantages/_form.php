<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WidgetAdvantages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="widget-advantages-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
      <li class="active"><a href="#ru" data-toggle="tab">RU</a></li>
      <li><a href="#en" data-toggle="tab">EN</a></li>
      <li><a href="#de" data-toggle="tab">DE</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane fade in active" id="ru">

          <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

          
            <?php echo $form->field($model, 'text_ru')->widget(
                \yii\imperavi\Widget::className(),
                [
                    'plugins' => ['fullscreen', 'fontcolor', 'video'],
                    'options' => [
                        'minHeight' => 200,
                        'maxHeight' => 200,
                        'buttonSource' => true,
                        'convertDivs' => false,
                        'removeEmptyTags' => false,
                        
                    ]
                ]
            ) ?>
      </div>
      <div class="tab-pane fade" id="en">

          <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

          <?php echo $form->field($model, 'text_en')->widget(
                \yii\imperavi\Widget::className(),
                [
                    'plugins' => ['fullscreen', 'fontcolor', 'video'],
                    'options' => [
                        'minHeight' => 200,
                        'maxHeight' => 200,
                        'buttonSource' => true,
                        'convertDivs' => false,
                        'removeEmptyTags' => false,
                        
                    ]
                ]
            ) ?>

      </div>
      <div class="tab-pane fade" id="de">

          <?= $form->field($model, 'name_de')->textInput(['maxlength' => true]) ?>
          
          <?= $form->field($model, 'title_de')->textInput(['maxlength' => true]) ?>

          <?php echo $form->field($model, 'text_de')->widget(
                \yii\imperavi\Widget::className(),
                [
                    'plugins' => ['fullscreen', 'fontcolor', 'video'],
                    'options' => [
                        'minHeight' => 200,
                        'maxHeight' => 200,
                        'buttonSource' => true,
                        'convertDivs' => false,
                        'removeEmptyTags' => false,
                        
                    ]
                ]
            ) ?>

      </div>
    </div>
    
    <?= $form->field($model, 'percent')->textInput(['maxlength' => true]) ?>
   
    <?= $form->field($model, 'patch')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'advantes_id')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
