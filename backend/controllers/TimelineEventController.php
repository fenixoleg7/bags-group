<?php

namespace backend\controllers;

use Yii;
use backend\models\search\TimelineEventSearch;
use yii\web\Controller;
use backend\models\search\ArticleSearch;
use backend\models\search\ProductsSearch;
use backend\models\search\CatalogCategorySearch;

/**
 * Application timeline controller
 */
class TimelineEventController extends Controller
{
    public $layout = 'common';
    /**
     * Lists all TimelineEvent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TimelineEventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder'=>['created_at'=>SORT_DESC]
        ];

         $article_count = ArticleSearch::articlecount();
         $products_count = ProductsSearch::productscount();
         $category_count = CatalogCategorySearch::categorycount();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'article_count' => $article_count,
            'products_count' => $products_count,
            'category_count' => $category_count,
        ]);
    }
}
