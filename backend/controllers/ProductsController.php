<?php

namespace backend\controllers;

use Yii;
use common\models\Products;
use common\models\CatalogCategory;
use backend\models\search\ProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Intervention\Image\ImageManagerStatic;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{

    public function actions(){
            return [
                   'upload'=>[
                       'class'=>'trntv\filekit\actions\UploadAction',
                       'deleteRoute' => 'my-custom-delete', // my custom delete action for deleting just uploaded files(not yet saved)
                       'fileStorage' => 'myfileStorage', // my custom fileStorage from configuration
                       /*'multiple' => true,
                       'disableCsrf' => true,
                       'responseFormat' => Response::FORMAT_JSON,
                       'responsePathParam' => 'path',
                       'responseBaseUrlParam' => 'base_url',
                       'responseUrlParam' => 'url',
                       'responseDeleteUrlParam' => 'delete_url',
                       'responseMimeTypeParam' => 'type',
                       'responseNameParam' => 'name',
                       'responseSizeParam' => 'size',
                       'deleteRoute' => 'delete',
                       'fileStorage' => 'fileStorage', // Yii::$app->get('fileStorage')
                       'fileStorageParam' => 'fileStorage', // ?fileStorage=someStorageComponent
                       'sessionKey' => '_uploadedFiles',
                       'allowChangeFilestorage' => false,
                       'validationRules' => [
                            ...
                       ],*/
                       'on afterSave' => function($event) {
                            /* @var $file \League\Flysystem\File */
                           $file = $event->file;
                            $img = ImageManagerStatic::make($file->read())->fit(215, 215);
                            $file->put($img->encode());
                               }
                   ]
               ];
        }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'categories' => CatalogCategory::find()->active()->all(),
            ]);
        }
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'categories' => CatalogCategory::find()->active()->all(),
            ]);
        }
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
