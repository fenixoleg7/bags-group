<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SiteInfo;

/**
 * SiteInfoSearch represents the model behind the search form about `common\models\SiteInfo`.
 */
class SiteInfoSearch extends SiteInfo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name_site', 'name_company', 'adress', 'email', 'phone_1', 'phone_2', 'fax', 'skype_url', 'linkedIn_url', 'Facebook_url', 'xing_url', 'logo_base_url', 'logo_heder_path', 'logo_footer_path', 'date_1', 'date_2', 'date_3', 'date_4', 'date_5'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name_site', $this->name_site])
            ->andFilterWhere(['like', 'name_company', $this->name_company])
            ->andFilterWhere(['like', 'adress', $this->adress])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone_1', $this->phone_1])
            ->andFilterWhere(['like', 'phone_2', $this->phone_2])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'skype_url', $this->skype_url])
            ->andFilterWhere(['like', 'linkedIn_url', $this->linkedIn_url])
            ->andFilterWhere(['like', 'Facebook_url', $this->Facebook_url])
            ->andFilterWhere(['like', 'xing_url', $this->xing_url])
            ->andFilterWhere(['like', 'logo_base_url', $this->logo_base_url])
            ->andFilterWhere(['like', 'logo_heder_path', $this->logo_heder_path])
            ->andFilterWhere(['like', 'logo_footer_path', $this->logo_footer_path])
            ->andFilterWhere(['like', 'date_1', $this->date_1])
            ->andFilterWhere(['like', 'date_2', $this->date_2])
            ->andFilterWhere(['like', 'date_3', $this->date_3])
            ->andFilterWhere(['like', 'date_4', $this->date_4])
            ->andFilterWhere(['like', 'date_5', $this->date_5]);

        return $dataProvider;
    }
}
