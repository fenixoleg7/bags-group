<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\WidgetHomeslider;

/**
 * WidgetHomesliderSearch represents the model behind the search form about `common\models\WidgetHomeslider`.
 */
class WidgetHomesliderSearch extends WidgetHomeslider
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['path1', 'icon_span', 'path4', 'caption_ru_1', 'caption_ru_2', 'caption_en_1', 'caption_en_2', 'caption_de_1', 'caption_de_2'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WidgetHomeslider::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'path1', $this->path1])
            ->andFilterWhere(['like', 'icon_span', $this->icon_span])
            ->andFilterWhere(['like', 'path4', $this->path4])
            ->andFilterWhere(['like', 'caption_ru_1', $this->caption_ru_1])
            ->andFilterWhere(['like', 'caption_ru_2', $this->caption_ru_2])
            ->andFilterWhere(['like', 'caption_en_1', $this->caption_en_1])
            ->andFilterWhere(['like', 'caption_en_2', $this->caption_en_2])
            ->andFilterWhere(['like', 'caption_de_1', $this->caption_de_1])
            ->andFilterWhere(['like', 'caption_de_2', $this->caption_de_2]);

        return $dataProvider;
    }
}
