<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;

/**
 * ProductsSearch represents the model behind the search form about `common\models\Products`.
 */
class ProductsSearch extends Products
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'kod', 'category_id', 'updater_id', 'status', 'published_at', 'created_at', 'updated_at'], 'integer'],
            [['slug', 'title_ru', 'title_en', 'title_de', 'keywords_ru', 'keywords_de', 'keywords_en', 'description_ru', 'description_en', 'description_de', 'body_ru', 'body_en', 'body_de', 'size', 'capacity', 'thumbnail_base_url', 'thumbnail_path_1', 'thumbnail_path_2', 'thumbnail_path_3', 'thumbnail_path_4', 'thumbnail_path_5', 'thumbnail_path_6'], 'safe'],
            [['price_1', 'price_2', 'price_3', 'price_4', 'price_5'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Products::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'kod' => $this->kod,
            'price_1' => $this->price_1,
            'price_2' => $this->price_2,
            'price_3' => $this->price_3,
            'price_4' => $this->price_4,
            'price_5' => $this->price_5,
            'category_id' => $this->category_id,
            'updater_id' => $this->updater_id,
            'status' => $this->status,
            'published_at' => $this->published_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'title_ru', $this->title_ru])
            ->andFilterWhere(['like', 'title_en', $this->title_en])
            ->andFilterWhere(['like', 'title_de', $this->title_de])
            ->andFilterWhere(['like', 'keywords_ru', $this->keywords_ru])
            ->andFilterWhere(['like', 'keywords_de', $this->keywords_de])
            ->andFilterWhere(['like', 'keywords_en', $this->keywords_en])
            ->andFilterWhere(['like', 'description_ru', $this->description_ru])
            ->andFilterWhere(['like', 'description_en', $this->description_en])
            ->andFilterWhere(['like', 'description_de', $this->description_de])
            ->andFilterWhere(['like', 'body_ru', $this->body_ru])
            ->andFilterWhere(['like', 'body_en', $this->body_en])
            ->andFilterWhere(['like', 'body_de', $this->body_de])
            ->andFilterWhere(['like', 'size', $this->size])
            ->andFilterWhere(['like', 'capacity', $this->capacity])
            ->andFilterWhere(['like', 'thumbnail_base_url', $this->thumbnail_base_url])
            ->andFilterWhere(['like', 'thumbnail_path_1', $this->thumbnail_path_1])
            ->andFilterWhere(['like', 'thumbnail_path_2', $this->thumbnail_path_2])
            ->andFilterWhere(['like', 'thumbnail_path_3', $this->thumbnail_path_3])
            ->andFilterWhere(['like', 'thumbnail_path_4', $this->thumbnail_path_4])
            ->andFilterWhere(['like', 'thumbnail_path_5', $this->thumbnail_path_5])
            ->andFilterWhere(['like', 'thumbnail_path_6', $this->thumbnail_path_6]);

        return $dataProvider;
    }

        static function productscount()
    {
        $count = Products::find()->count();
        return $count;
    }
}
