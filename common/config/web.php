<?php
//phpinfo();
$config = [
    'components' => [
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LdS8H4UAAAAANzJfwoFtxIZqyCDAH2i_0uz2ho5',
            'secret' => '6LdS8H4UAAAAAFC_t3Rqv1-e5E85lFhx4JOq872I',
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            //'linkAssets' => true,
            'linkAssets' => false,
            'appendTimestamp' => YII_ENV_DEV
        ]
    ],
    'as locale' => [
        'class' => 'common\behaviors\LocaleBehavior',
        'enablePreferredLanguage' => true
    ]
];

if (YII_DEBUG) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        //'allowedIPs' => ['127.0.0.1', '::1', '192.168.33.1', '172.17.42.1', '172.17.0.1'],
        'allowedIPs' => ['*'],
    ];
}

if (YII_ENV_DEV) {
    $config['modules']['gii'] = [
        //'allowedIPs' => ['127.0.0.1', '::1', '192.168.33.1', '172.17.42.1', '172.17.0.1'],
        'allowedIPs' => ['*'],
    ];
}


return $config;
