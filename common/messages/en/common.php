<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'API access token' => '',
    'Email' => '',
    'Expire At' => '',
    'Not Active' => '',
    'Password' => '',
    'Roles' => '',
    'Token' => '',
    '"{attribute}" must be a valid JSON' => '"{attribute}" должен содержать валидный JSON',
    'Active' => 'Активно',
    'Article ID' => 'ID Статьи',
    'Article View' => 'Шаблон статьи',
    'Author' => 'Автор',
    'Base URL' => 'Базовый URL',
    'Base Url' => 'Базовый Url',
    'Body' => 'Текст',
    'Caption' => 'Текст',
    'Carousel ID' => 'ID карусели',
    'Category' => 'Категория',
    'Comment' => 'Комментарий',
    'Component' => 'Компонент',
    'Config' => 'Настройки',
    'Created At' => 'Создано',
    'Created at' => 'Создано',
    'Deleted' => 'Удалено',
    'Down to maintenance.' => 'Закрыто на обслуживание',
    'E-mail' => 'E-mail',
    'File Type' => 'Тип файла',
    'Firstname' => 'Имя',
    'Gender' => 'Пол',
    'ID' => 'ID',
    'Image' => 'Изображение',
    'Key' => 'Ключ',
    'Last login' => 'Последний вход',
    'Lastname' => 'Фамилия',
    'Locale' => 'Локаль',
    'Middlename' => 'Отчество',
    'Name' => 'Имя',
    'Order' => 'Порядок',
    'Page View' => 'Шаблон страницы',
    'Parent Category' => 'Родительская категория',
    'Path' => 'Путь',
    'Picture' => 'Аватар',
    'Published' => 'Опубликовано',
    'Published At' => 'Дата публикации',
    'Size' => 'Размер',
    'Slug' => 'ЧПУ',
    'Status' => 'Статус',
    'Thumbnail' => 'Эскиз изображения',
    'Title' => 'Название',
    'Type' => 'Тип',
    'Updated At' => 'Обновлено',
    'Updated at' => 'Последнее обновление',
    'Updater' => 'Обновивщий',
    'Upload Ip' => 'IP',
    'Url' => 'Url',
    'User ID' => 'ID пользователя',
    'Username' => 'Имя пользователя',
    'Value' => 'Значение',
    'Title_EN' => 'Название EN',
    'Title_DE' => 'Название DE',
    'Body_EN' => 'Текст EN',
    'Body_DE' => 'Текст DE',
    'Keywords_RU' => 'Ключевые слова',
    'Keywords_EN' => 'Ключевые слова EN',
    'Keywords_DE' => 'Ключевые слова DE',
    'Description_RU' => 'Description RU ',
    'Description_EN' => 'Description EN',
    'Description_DE' => 'Description DE ',
    'Name RU' => 'Название',
    'Name EN' => 'Название EN',
    'Name DE' => 'Название DE',
    'Title RU' => 'Загаловок',
    'Title EN' => 'Загаловок EN',
    'Title DE' => 'Загаловок DE',
    'Text RU' => 'Текст',
    'Text EN' => 'Текст EN',
    'Text DE' => 'Текст DE',
    'Path1' => 'Изображение 1',
    'Path2' => 'Изображение 2',
    'Path3' => 'Изображение 3',
    'Path4' => 'Изображение 4',
    'Caption Ru 1' => 'Загаловок в слайдере',
    'Caption Ru 2' => 'Текст в слайдере',
    'Caption EN 1' => 'Загаловок в слайдере',
    'Caption EN 2' => 'Текст в слайдере',
    'Caption DE 1' => 'Загаловок в слайдере',
    'Caption DE 2' => 'Текст в слайдере',
    'Step' => 'Этап',
    'Icon' => 'Иконка',
    'Image of heder page' => 'Эскиз изображения в заголовке сраницы',
    'Name Site' => 'Название сайта',
    'Name Company' => 'Название компании',
    'Adress' => 'Адресс',
    'Email' => 'Email',
    'Phone 1' => 'Телефон 1',
    'Phone 2' => 'Телефон 2',
    'Fax' => 'Факс',
    'Skype Url' => 'Skype',
    'LinkedIn Url' => 'LinkedIn',
    'Facebook Url' => 'Facebook',
    'Xing Url' => 'Xing',
    'Logo Heder' => 'Логотип в шапке',
    'Logo Footer' => 'Логотип в футоре',
    'Site Info' => 'Информация о сайте',
    'Kod' => 'Код',
    'Size' => 'Размер',
    'Capacity' => 'Вместительность',
    'Prise 1' => '1-1000',
    'Prise 2' => '1000-5000',
    'Prise 3' => '5000-10000',
    'Prise 4' => '10000-50000',
    'Prise 5' => 'от 50000',
    'Product ID' => 'Id продукта',
    'Product Name' => 'Название',
    'Product Kod' => 'Код',
    'Product Prise' => 'Цена',
    'Product Quantity' => 'Количество',
    'Total Cost' => 'Общая стоимость',
    'Currency' => 'Валюта',
    'Client Name' => 'Имя клиента',
    'Client Phone' => 'Телефон клиента',
    'Client Email' => 'E-mail клиента',
    'Client Message' => 'Сообщения от клиента',


];
