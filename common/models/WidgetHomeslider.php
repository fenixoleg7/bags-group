<?php

namespace common\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "widget_homeslider".
 *
 * @property integer $id
 * @property string $path1
 * @property string $path2
 * @property string $path3
 * @property string $path4
 * @property string $caption_ru_1
 * @property string $caption_ru_2
 * @property string $caption_en_1
 * @property string $caption_en_2
 * @property string $caption_de_1
 * @property string $caption_de_2
 * @property string $caption_pl_1
 * @property string $caption_pl_2
 * @property integer $status
 * @property integer $order
 * @property integer $created_at
 * @property integer $updated_at
 */
class WidgetHomeslider extends \yii\db\ActiveRecord
{

    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT = 0;

    public $slide_1;
   /* public $slide_2;
    public $slide_3;*/
    public $slide_4;
   // public $base_url = "http://storage.yii2-starter-kit.dev/source";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widget_homeslider';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'slide_1',
                'pathAttribute' => 'path1',
                'baseUrlAttribute' => 'base_url',
            ],
            /*[
                'class' => UploadBehavior::className(),
                'attribute' => 'slide_2',
                'pathAttribute' => 'path2',
                'baseUrlAttribute' => 'base_url',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'slide_3',
                'pathAttribute' => 'path3',
                'baseUrlAttribute' => 'base_url',
            ],*/
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'slide_4',
                'pathAttribute' => 'path4',
                'baseUrlAttribute' => 'base_url',
            ],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'integer'],
            [['title_ru','title_en','title_de','title_pl','base_url','path1', 'icon_span', 'path4', 'caption_ru_1', 'caption_ru_2', 'caption_en_1', 'caption_en_2', 'caption_de_1', 'caption_de_2','caption_pl_1', 'caption_pl_2'], 'string', 'max' => 254],
            [['slide_1','slide_4'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'title_ru' => Yii::t('common', 'Title RU'),
            'title_en' => Yii::t('common', 'Title EN'),
            'title_de' => Yii::t('common', 'Title DE'),
            'title_pl' => Yii::t('common', 'Title PL'),
            'path1' => Yii::t('common', 'Path1'),
            'icon_span' => Yii::t('common','Icon span'),
            'path4' => Yii::t('common', 'Path4'),
            'caption_ru_1' => Yii::t('common', 'Caption Ru 1'),
            'caption_ru_2' => Yii::t('common', 'Caption Ru 2'),
            'caption_en_1' => Yii::t('common', 'Caption En 1'),
            'caption_en_2' => Yii::t('common', 'Caption En 2'),
            'caption_de_1' => Yii::t('common', 'Caption De 1'),
            'caption_de_2' => Yii::t('common', 'Caption De 2'),
            'caption_pl_1' => Yii::t('common', 'Caption Pl 1'),
            'caption_pl_2' => Yii::t('common', 'Caption Pl 2'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),
            'slide_1' => Yii::t('common', 'Path1'),
            'slide_2' => Yii::t('common', 'Path2'),
            'slide_3' => Yii::t('common', 'Path3'),
            'slide_4' => Yii::t('common', 'Path4'),
            ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\WidgetHomesliderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\WidgetHomesliderQuery(get_called_class());
    }

     /**
     * The function return url for images
     * @return string
     */
    public function getImageUrl($patch)
    {
        //return rtrim($this->base_url, '/') . '/' . ltrim($patch, '/');
        return 'https://bags-group.de/storage/web/source' . '/' . ltrim($patch, '/');
    }
}
