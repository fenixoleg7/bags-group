<?php

namespace common\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use common\models\query\CatalogCategoryQuery;
/**
 * This is the model class for table "catalog_category".
 *
 * @property integer $id
 * @property string $slug
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_de
 * @property string $title_pl
 * @property string $keywords_ru
 * @property string $keywords_de
 * @property string $keywords_en
 * @property string $keywords_pl
 * @property string $description_ru
 * @property string $description_en
 * @property string $description_de
 * @property string $description_pl
 * @property string $body_ru
 * @property string $body_en
 * @property string $body_de
 * @property string $body_pl
 * @property integer $parent_id
 * @property string $thumbnail_base_url
 * @property string $thumbnail_path
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CatalogCategory $parent
 * @property CatalogCategory[] $catalogCategories
 * @property Products[] $products
 */
class CatalogCategory extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DRAFT = 0;

    public $thumbnail;

    public $heder_image;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalog_category';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title_ru',
                'immutable' => true
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'thumbnail',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url'
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'heder_image',
                'pathAttribute' => 'heder_image_path',
                'baseUrlAttribute' => 'thumbnail_base_url'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru'], 'required'],
            [['body_ru', 'body_en', 'body_de', 'body_pl'], 'string'],
            [['parent_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['slug', 'thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],
            [['title_ru', 'title_en', 'title_de','title_pl'], 'string', 'max' => 512],
            [['keywords_ru', 'keywords_de', 'keywords_en', 'keywords_pl','description_ru', 'description_en', 'description_de', 'description_pl'], 'string', 'max' => 255],
            ['parent_id', 'exist', 'targetClass' => CatalogCategory::className(), 'targetAttribute' => 'id'],
            [['heder_image', 'thumbnail'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'slug' => Yii::t('common', 'Slug'),
            'title_ru' => Yii::t('common', 'Title RU'),
            'title_en' => Yii::t('common', 'Title EN'),
            'title_de' => Yii::t('common', 'Title DE'),
            'title_pl' => Yii::t('common', 'Title PL'),
            'keywords_ru' => Yii::t('common', 'Keywords RU'),
            'keywords_de' => Yii::t('common', 'Keywords DE'),
            'keywords_en' => Yii::t('common', 'Keywords EN'),
            'keywords_pl' => Yii::t('common', 'Keywords PL'),
            'description_ru' => Yii::t('common', 'Description RU'),
            'description_en' => Yii::t('common', 'Description EN'),
            'description_de' => Yii::t('common', 'Description DE'),
            'description_pl' => Yii::t('common', 'Description PL'),
            'body_ru' => Yii::t('common', 'Body RU'),
            'body_en' => Yii::t('common', 'Body EN'),
            'body_de' => Yii::t('common', 'Body DE'),
            'body_pl' => Yii::t('common', 'Body PL'),
            'parent_id' => Yii::t('common', 'Parent ID'),
            'thumbnail_base_url' => Yii::t('common', 'Thumbnail Base Url'),
            'thumbnail_path' => Yii::t('common', 'Thumbnail Path'),
            'status' => Yii::t('common', 'Status'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),
        ];
    }

    public function getHederImageUrl()
    {
        return rtrim($this->thumbnail_base_url, '/') . '/' . ltrim($this->heder_image_path, '/');
    }

     /**
     * @return string
     */
    public function getImageUrl()
    {
        //return rtrim($this->thumbnail_base_url, '/') . '/' . ltrim($this->thumbnail_path, '/');
        return 'https://bags-group.de/storage/web/source' . '/' . ltrim($this->thumbnail_path, '/');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(CatalogCategory::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogCategories()
    {
        return $this->hasMany(CatalogCategory::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['category_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\CatalogCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\CatalogCategoryQuery(get_called_class());
    }

     public function getDateLang($lang)
    {
        $date_lang = array();
        $date_lang['id'] = $this->id;

        if ($lang == 'ru') {

            $date_lang['title'] = $this->title_ru;
            $date_lang['description'] = $this->description_ru;
            $date_lang['keywords'] = $this->keywords_ru;
            $date_lang['body'] = $this->body_ru;
            //$date_lang['id'] = $this->id;

        } elseif($lang == 'en') {

            $date_lang['title'] = $this->title_en;
            $date_lang['description'] = $this->description_en;
            $date_lang['keywords'] = $this->keywords_en;
            $date_lang['body'] = $this->body_en;
            //$date_lang['id'] = $this->id;

        }elseif($lang == 'de'){

            $date_lang['title'] = $this->title_de;
            $date_lang['description'] = $this->description_de;
            $date_lang['keywords'] = $this->keywords_de;
            $date_lang['body'] = $this->body_de;
            //$date_lang['id'] = $this->id;

        }elseif($lang == 'pl'){

            $date_lang['title'] = $this->title_pl;
            $date_lang['description'] = $this->description_pl;
            $date_lang['keywords'] = $this->keywords_pl;
            $date_lang['body'] = $this->body_pl;
            //$date_lang['id'] = $this->id;

        }
        
        return $date_lang;
    }
}
