<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "widgets".
 *
 * @property integer $id
 * @property string $name
 * @property integer $page
 * @property integer $quantity
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Widgets extends \yii\db\ActiveRecord
{
    const STATUS_PUBLISHED = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widgets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['page', 'quantity', 'status'], 'integer'],
            [['name'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Тип виджета',
            'page' => 'Страница размещения',
            'quantity' => 'Количество элементов',
            'status' => 'Статус',

        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\WidgetsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\WidgetsQuery(get_called_class());
    }
}
