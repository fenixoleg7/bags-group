<?php

namespace common\models;

use common\models\query\ArticleQuery;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $slug
* @property string $keywords_ru
 * @property string $keywords_en
 * @property string $keywords_de
 * @property string $description_ru
 * @property string $description_en
 * @property string $description_de
 * @property string $description_pl
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_de
 * @property string $title_pl
 * @property string $body_ru
 * @property string $body_en
 * @property string $body_de
 * @property string $body_pl
 * @property string $view
 * @property string $thumbnail_base_url
 * @property string $thumbnail_path
 * @property string $heder_image_path
 * @property array $attachments
 * @property integer $author_id
 * @property integer $updater_id
 * @property integer $category_id
 * @property integer $status
 * @property integer $published_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $author
 * @property User $updater
 * @property ArticleCategory $category
 * @property ArticleAttachment[] $articleAttachments
 */
class Article extends ActiveRecord
{
    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT = 0;

    
    /**
     * @var array
     */
    public $thumbnail;

    public $heder_image;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article}}';
    }

    /**
     * @return ArticleQuery
     */
    public static function find()
    {
        return new ArticleQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'author_id',
                'updatedByAttribute' => 'updater_id',

            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title_ru',
                'immutable' => true
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'thumbnail',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url'
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'heder_image',
                'pathAttribute' => 'heder_image_path',
                'baseUrlAttribute' => 'thumbnail_base_url'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru', 'body_ru', 'category_id'], 'required'],
            [['slug'], 'unique'],
            [['body_ru','body_en','body_de', 'body_pl',
            'keywords_ru','keywords_en','keywords_de', 'keywords_pl',
            'description_ru','description_en','description_de', 'description_pl',
            'title_en','title_de', 'title_pl'], 'string'],
            [['published_at'], 'default', 'value' => function () {
                return date(DATE_ISO8601);
            }],
            [['published_at'], 'filter', 'filter' => 'strtotime', 'skipOnEmpty' => true],
            [['category_id'], 'exist', 'targetClass' => ArticleCategory::className(), 'targetAttribute' => 'id'],
            [['author_id', 'updater_id', 'status'], 'integer'],
            [['slug', 'thumbnail_base_url', 'thumbnail_path','heder_image', 'heder_image_path'], 'string', 'max' => 1024],
            [['title_ru'], 'string', 'max' => 512],
            [['view'], 'string', 'max' => 255],
            [['heder_image', 'thumbnail'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'slug' => Yii::t('common', 'Slug'),
            'title_ru' => Yii::t('common', 'Title RU'),
            'title_en' => Yii::t('common', 'Title EN'),
            'title_de' => Yii::t('common', 'Title DE'),
            'body_ru' => Yii::t('common', 'Body_RU'),
            'body_en' => Yii::t('common', 'Body_EN'),
            'body_de' => Yii::t('common', 'Body_DE'),
            'body_pl' => Yii::t('common', 'Body_PL'),
            'keywords_ru' => Yii::t('common','Keywords_RU'),
            'keywords_de' => Yii::t('common','Keywords_DE'),
            'keywords_en' => Yii::t('common','Keywords_EN'),
            'keywords_pl' => Yii::t('common','Keywords_PL'),
            'description_ru' => Yii::t('common','Description_RU'),
            'description_en' => Yii::t('common','Description_EN'),
            'description_de' => Yii::t('common','Description_DE'),
            'description_pl' => Yii::t('common','Description_PL'),
            'view' => Yii::t('common', 'Article View'),
            'thumbnail' => Yii::t('common', 'Thumbnail'),
            'heder_image' => Yii::t('common', 'Image of heder page'),
            'author_id' => Yii::t('common', 'Author'),
            'updater_id' => Yii::t('common', 'Updater'),
            'category_id' => Yii::t('common', 'Category'),
            'status' => Yii::t('common', 'Published'),
            'published_at' => Yii::t('common', 'Published At'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ArticleCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleAttachments()
    {
        return $this->hasMany(ArticleAttachment::className(), ['article_id' => 'id']);
    }

     /**
     * @return string
     */
    public function getHederImageUrl()
    {
        //return rtrim($this->thumbnail_base_url, '/') . '/' . ltrim($this->heder_image_path, '/');
        return 'https://bags-group.de/storage/web/source' . '/' . ltrim($this->heder_image_path, '/');
    }

     /**
     * @return string
     */
    public function getImageUrl()
    {
        //return rtrim($this->thumbnail_base_url, '/') . '/' . ltrim($this->thumbnail_path, '/');
        return 'https://bags-group.de/storage/web/source' . '/' . ltrim($this->thumbnail_path, '/');
    }

     public function getDateLang($lang)
    {
        $date_lang = array();

        if ($lang == 'ru') {

            $date_lang['title'] = $this->title_ru;
            $date_lang['description'] = $this->description_ru;
            $date_lang['keywords'] = $this->keywords_ru;
            $date_lang['body'] = $this->body_ru;

        } elseif($lang == 'en') {

            $date_lang['title'] = $this->title_en;
            $date_lang['description'] = $this->description_en;
            $date_lang['keywords'] = $this->keywords_en;
            $date_lang['body'] = $this->body_en;

        }elseif($lang == 'de'){

            $date_lang['title'] = $this->title_de;
            $date_lang['description'] = $this->description_de;
            $date_lang['keywords'] = $this->keywords_de;
            $date_lang['body'] = $this->body_de;

        }elseif($lang == 'pl'){

            $date_lang['title'] = $this->title_pl;
            $date_lang['description'] = $this->description_pl;
            $date_lang['keywords'] = $this->keywords_pl;
            $date_lang['body'] = $this->body_pl;

        }
        
        return $date_lang;
    }
}
