<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;

/**
 * This is the model class for table "widget_work_schema".
 *
 * @property integer $id
 * @property integer $step
 * @property string $base_url
 * @property string $path
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_de
 * @property integer $created_at
 * @property integer $updated_at
 */
class WidgetWorkSchema extends \yii\db\ActiveRecord
{

    public $image;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widget_work_schema';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['step'], 'integer'],
            [['text_ru', 'text_en', 'text_de'], 'required'],
            [['text_ru', 'text_en', 'text_de'], 'string'],
            [['base_url', 'path'], 'string', 'max' => 1024],
            [['title_ru', 'title_en', 'title_de'], 'string', 'max' => 255],
             ['image', 'safe']
        ];
    }

     public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'image',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url',
            ]
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'image' => Yii::t('common', 'Icon'),
            'step' => Yii::t('common', 'Step'),
            'base_url' => Yii::t('common', 'Base Url'),
            'path' => Yii::t('common', 'Path'),
            'title_ru' => Yii::t('common', 'Title RU'),
            'title_en' => Yii::t('common', 'Title EN'),
            'title_de' => Yii::t('common', 'Title DE'),
            'text_ru' => Yii::t('common', 'Text RU'),
            'text_en' => Yii::t('common', 'Text EN'),
            'text_de' => Yii::t('common', 'Text DE'),
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\WidgetWorkSchemaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\WidgetWorkSchemaQuery(get_called_class());
    }

    /**
     * The function return url for images
     * @return string
     */
    public function getImageUrl()
    {
        //return rtrim($this->base_url, '/') . '/' . ltrim($this->path, '/');
        return 'https://bags-group.de/storage/web/source' . '/' . ltrim($this->path, '/');
    }
}
