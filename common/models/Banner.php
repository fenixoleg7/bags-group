<?php

namespace common\models;

use Yii;
use common\models\query\BannerQuery;
use yii\behaviors\TimestampBehavior;
use common\behaviors\CacheInvalidateBehavior;
use trntv\filekit\behaviors\UploadBehavior;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $base_url
 * @property string $path
 * @property string $path_en
 * @property string $path_de
 * @property string $path_pl
 * @property string $type
 * @property string $url
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_de
 * @property string $name_pl
 * @property string $caption_ru
 * @property string $caption_en
 * @property string $caption_de
 * @property string $caption_pl
 * @property integer $status
 * @property integer $page
 * @property integer $created_at
 * @property integer $updated_at
 */
class Banner extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;

     /**
     * @var array|null
     */
    public $image;
    public $image_en;
    public $image_de;
    public $image_pl;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%banner}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'page'], 'integer'],
            [['base_url', 'path','path_en','path_de','path_pl','url','caption_ru','caption_en', 'caption_de', 'caption_pl'], 'string', 'max' => 1024],
            [['type', 'name_ru','name_en','name_de', 'name_pl'], 'string', 'max' => 255],
            [['image','image_en','image_de', 'image_pl'], 'safe']
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'image',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url',
                'typeAttribute' => 'type'
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'image_en',
                'pathAttribute' => 'path_en',
                'baseUrlAttribute' => 'base_url',
                'typeAttribute' => 'type'
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'image_de',
                'pathAttribute' => 'path_de',
                'baseUrlAttribute' => 'base_url',
                'typeAttribute' => 'type'
            ],
            'cacheInvalidate'=>[
                'class' => CacheInvalidateBehavior::className(),
                'cacheComponent' => 'frontendCache',
                'keys' => [
                    function ($model) {
                        return [
                            Banner::className(),
                        ];
                    }
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'image' => Yii::t('common', 'Imag'),
            'image_en' => Yii::t('common', 'Imag EN'),
            'image_de' => Yii::t('common', 'Imag DE'),
            'image_pl' => Yii::t('common', 'Imag PL'),
            'base_url' => Yii::t('common', 'Base URL'),
            'path' => Yii::t('common', 'Path'),
            'type' => Yii::t('common', 'Type'),
            'url' => Yii::t('common', 'Url'),
            'name_ru' =>Yii::t('common', 'Name RU'),
            'name_en' =>Yii::t('common', 'Name EN'),
            'name_de' =>Yii::t('common', 'Name DE'),
            'name_pl' =>Yii::t('common', 'Name PL'),
            'caption_ru' => Yii::t('common', 'Text RU'),
            'caption_en' => Yii::t('common', 'Text EN'),
            'caption_de' => Yii::t('common', 'Text DE'),
            'caption_pl' => Yii::t('common', 'Text PL'),
            'status' => Yii::t('common', 'Status'),
            'page' =>  Yii::t('common', 'Page'),
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return BannerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BannerQuery(get_called_class());
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        //return rtrim($this->base_url, '/') . '/' . ltrim($this->path, '/');
        return 'https://bags-group.de/storage/web/source' . '/' . ltrim($this->path, '/');
    }

    public function getImageLangUrl($lang)
    {
        //
       if ($lang == 'ru') {

            return rtrim($this->base_url, '/') . '/' . ltrim($this->path, '/');

       } elseif($lang == 'en') {

            return rtrim($this->base_url, '/') . '/' . ltrim($this->path_en, '/');

       } elseif($lang == 'de') {

            return rtrim($this->base_url, '/') . '/' . ltrim($this->path_de, '/');

       }else{

            return rtrim($this->base_url, '/') . '/' . ltrim($this->path_pl, '/');

       }
    }

     public function getDateLang($lang)
    {
        $date_lang = array();

        if ($lang == 'ru') {

            $date_lang['title'] = $this->name_ru;
            $date_lang['caption'] = $this->caption_ru;
            $date_lang['image'] = 'https://bags-group.de/storage/web/source' . '/' . ltrim($this->path, '/');

        } elseif($lang == 'en') {

            $date_lang['title'] = $this->name_en;
            $date_lang['caption'] = $this->caption_en;
            $date_lang['image'] = 'https://bags-group.de/storage/web/source' . '/' . ltrim($this->path_en, '/');

        }elseif($lang == 'de'){

            $date_lang['title'] = $this->name_de;
            $date_lang['caption'] = $this->caption_de;
            $date_lang['image'] = 'https://bags-group.de/storage/web/source' . '/' . ltrim($this->path_de, '/');

        }elseif($lang == 'pl'){

            $date_lang['title'] = $this->name_pl;
            $date_lang['caption'] = $this->caption_pl;
            $date_lang['image'] = 'https://bags-group.de/storage/web/source' . '/' . ltrim($this->path_pl, '/');

        }
        
        return $date_lang;
    }
}
