<?php

namespace common\models;

use Yii;
use common\models\query\OrdersQuery;
use common\commands\AddToTimelineCommand;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property string $product_id
 * @property string $product_name
 * @property integer $product_kod
 * @property string $product_prise
 * @property integer $product_quantity
 * @property string $client_name
 * @property string $client_phone
 * @property string $client_email
 * @property string $client_message
 * @property integer $status
 * @property integer $created_at
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

     public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_name', 'client_name', 'client_phone', 'client_email'], 'required'],
            [['product_kod', 'product_quantity', 'status', 'created_at'], 'integer'],
            [['product_prise'], 'string','max'=>150],
            [['currency'], 'string', 'max'=>10],
            [['client_message'], 'string'],
            [['product_id', 'product_name'], 'string', 'max' => 512],
            [['client_name', 'client_phone', 'client_email'], 'string', 'max' => 254],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'product_id' => Yii::t('common', 'Product ID'),
            'product_name' => Yii::t('common', 'Product Name'),
            'product_kod' => Yii::t('common', 'Product Kod'),
            'product_prise' => Yii::t('common', 'Product Prise'),
            'product_quantity' => Yii::t('common', 'Product Quantity'),
            'total_cost' => Yii::t('common', 'Total Cost'),
            'currency' => Yii::t('common', 'Currency'),
            'client_name' => Yii::t('common', 'Client Name'),
            'client_phone' => Yii::t('common', 'Client Phone'),
            'client_email' => Yii::t('common', 'Client Email'),
            'client_message' => Yii::t('common', 'Client Message'),
            'status' => Yii::t('common', 'Status'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    /**
     * @inheritdoc
     * @return OrdersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrdersQuery(get_called_class());
    }

    /**
     * Creates order and application event
     */
     public function addToTimeline()
    {

        Yii::$app->commandBus->handle(new AddToTimelineCommand([
            'category' => 'orders',
            'event' => 'order',
            'data' => [
                'name' => $this->product_name,
                'client_name' => $this->client_name,
                'product_kod' => $this->product_kod,
                'created_at' => $this->created_at,
                'quantity' =>$this->product_quantity,
                'total_cost' => $this->total_cost,
                'currency' => $this->currency,
                'id' =>$this->id,
            ]
        ]));
   
    }
}
