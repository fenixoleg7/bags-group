<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "widget_advantages".
 *
 * @property integer $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_de
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_de
 * @property string $patch
 * @property integer $percent
 * @property integer $advantes_id
 */
class WidgetAdvantages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widget_advantages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_en', 'name_de', 'title_ru', 'title_en', 'title_de','patch'], 'string', 'max' => 255],
            [['text_ru', 'text_en', 'text_de'], 'string', 'max' => 1024],
            [['percent','advantes_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name_ru' => Yii::t('common', 'Name RU'),
            'name_en' => Yii::t('common', 'Name EN'),
            'name_de' => Yii::t('common', 'Name DE'),
            'title_ru' => Yii::t('common', 'Title RU'),
            'title_en' => Yii::t('common', 'Title EN'),
            'title_de' => Yii::t('common', 'Title DE'),
            'text_ru' => Yii::t('common', 'Text RU'),
            'text_en' => Yii::t('common', 'Text EN'),
            'text_de' => Yii::t('common', 'Text DE'),
        ];
    }
}
