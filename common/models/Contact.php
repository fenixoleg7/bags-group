<?php

namespace common\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\SluggableBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "contact".
 *
 * @property integer $id
 * @property string $body_ru
 * @property string $body_en
 * @property string $body_de
 * @property string $body_pl
 * @property string $thumbnail_base_url
 * @property string $thumbnail_path
 */
class Contact extends \yii\db\ActiveRecord
{
     public $img_contact;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact';
    }

     public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'img_contact',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body_ru', 'body_en', 'body_de', 'body_pl'], 'string'],
            [['thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],
            [['img_contact'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'body_ru' => Yii::t('common', 'Body_RU'),
            'body_en' => Yii::t('common', 'Body_EN'),
            'body_de' => Yii::t('common', 'Body_DE'),
            'body_pl' => Yii::t('common', 'Body_PL'),
            'thumbnail_base_url' => Yii::t('common', 'Thumbnail Base Url'),
            'thumbnail_path' => Yii::t('common', 'Thumbnail Path'),
        ];
    }

    public function getImageUrl($thumbnail_path)
    {
        //return @frontend . '/' . ltrim($patch, '/');
        return 'http://bags-group.de/storage/web/source' . '/' . ltrim($thumbnail_path, '/');
    }
}
