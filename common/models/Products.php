<?php

namespace common\models;

use Yii;
use common\models\query\CatalogCategoryQuery;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\SluggableBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property string $slug
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_de
 * @property string $title_pl
 * @property string $keywords_ru
 * @property string $keywords_de
 * @property string $keywords_en
 * @property string $keywords_pl
 * @property string $description_ru
 * @property string $description_en
 * @property string $description_de
 * @property string $description_pl
 * @property string $body_ru
 * @property string $body_en
 * @property string $body_de
 * @property string $body_pl
 * @property integer $kod
 * @property string $size
 * @property string $capacity
 * @property string $price_1
 * @property string $price_2
 * @property string $price_3
 * @property string $price_4
 * @property string $price_5
 * @property integer $category_id
 * @property string $thumbnail_base_url
 * @property string $thumbnail_path_1
 * @property string $thumbnail_path_2
 * @property string $thumbnail_path_3
 * @property string $thumbnail_path_4
 * @property string $thumbnail_path_5
 * @property string $thumbnail_path_6
 * @property integer $updater_id
 * @property integer $status
 * @property integer $published_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CatalogCategory $category
 */
class Products extends \yii\db\ActiveRecord
{
    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT = 0;

    public $slide_1;
    public $slide_2;
    public $slide_3;
    public $slide_4;
    public $slide_5;
    public $slide_6;

    public $heder_image;

    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

     public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title_ru',
                'immutable' => true
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'slide_1',
                'pathAttribute' => 'thumbnail_path_1',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'slide_2',
                'pathAttribute' => 'thumbnail_path_2',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'slide_3',
                'pathAttribute' => 'thumbnail_path_3',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'slide_4',
                'pathAttribute' => 'thumbnail_path_4',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'slide_5',
                'pathAttribute' => 'thumbnail_path_5',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'slide_6',
                'pathAttribute' => 'thumbnail_path_6',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_en', 'title_de'], 'required'],
            [['body_ru', 'body_en', 'body_de', 'body_pl'], 'string'],
            [['kod', 'category_id', 'updater_id', 'status', 'published_at', 'created_at', 'updated_at'], 'integer'],
            [['price_1', 'price_2', 'price_3', 'price_4', 'price_5'], 'string', 'max' => 50],
            [[ 'thumbnail_base_url', 'thumbnail_path_1', 'thumbnail_path_2', 'thumbnail_path_3', 'thumbnail_path_4', 'thumbnail_path_5', 'thumbnail_path_6'], 'string', 'max' => 1024],
            [['title_ru', 'title_en', 'title_de', 'title_pl'], 'string', 'max' => 512],
            [['keywords_ru', 'keywords_de', 'keywords_en', 'keywords_pl', 'description_ru', 'description_en', 'description_de', 'description_pl', 'size', 'capacity'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatalogCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['slide_1', 'slide_2','slide_3','slide_4','slide_5','slide_6','heder_image'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            //'slug' => Yii::t('common', 'Slug'),
            'title_ru' => Yii::t('common', 'Title RU'),
            'title_en' => Yii::t('common', 'Title EN'),
            'title_de' => Yii::t('common', 'Title DE'),
            'keywords_ru' => Yii::t('common', 'Keywords RU'),
            'keywords_de' => Yii::t('common', 'Keywords DE'),
            'keywords_en' => Yii::t('common', 'Keywords EN'),
            'keywords_pl' => Yii::t('common', 'Keywords PL'),
            'description_ru' => Yii::t('common', 'Description RU'),
            'description_en' => Yii::t('common', 'Description EN'),
            'description_de' => Yii::t('common', 'Description DE'),
            'description_pl' => Yii::t('common', 'Description PL'),
            'body_ru' => Yii::t('common', 'Body RU'),
            'body_en' => Yii::t('common', 'Body En'),
            'body_de' => Yii::t('common', 'Body DE'),
            'body_pl' => Yii::t('common', 'Body PL'),
            'kod' => Yii::t('common', 'Kod'),
            'size' => Yii::t('common', 'Size'),
            'capacity' => Yii::t('common', 'Capacity'),
            'prise_1' => Yii::t('common', 'Prise 1'),
            'prise_2' => Yii::t('common', 'Prise 2'),
            'prise_3' => Yii::t('common', 'Prise 3'),
            'prise_4' => Yii::t('common', 'Prise 4'),
            'prise_5' => Yii::t('common', 'Prise 5'),
            'category_id' => Yii::t('common', 'Category ID'),
            'thumbnail_base_url' => Yii::t('common', 'Thumbnail Base Url'),
            'thumbnail_path_1' => Yii::t('common', 'Thumbnail Path 1'),
            'thumbnail_path_2' => Yii::t('common', 'Thumbnail Path 2'),
            'thumbnail_path_3' => Yii::t('common', 'Thumbnail Path 3'),
            'thumbnail_path_4' => Yii::t('common', 'Thumbnail Path 4'),
            'thumbnail_path_5' => Yii::t('common', 'Thumbnail Path 5'),
            'thumbnail_path_6' => Yii::t('common', 'Thumbnail Path 6'),
            'updater_id' => Yii::t('common', 'Updater ID'),
            'status' => Yii::t('common', 'Status'),
            'published_at' => Yii::t('common', 'Published At'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(CatalogCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @inheritdoc
     * @return CatalogCategoryQuery the active query used by this AR class.
     */
    /*public static function find()
    {
        return new CatalogCategoryQuery(get_called_class());
    }*/
    public function getImageUrl()
    {
       // return rtrim($this->thumbnail_base_url, '/') . '/' . ltrim($this->thumbnail_path_1, '/');
        return 'https://bags-group.de/storage/web/source' . '/' . ltrim($this->thumbnail_path_1, '/');
    }
    public function getImagesUrl($patch)
    {
        //return rtrim($this->base_url, '/') . '/' . ltrim($patch, '/');
        return 'https://bags-group.de/storage/web/source' . '/' . ltrim($patch, '/');
    }

     public function getDateLang($lang)
    {
        $date_lang = array();

        if ($lang == 'ru') {

            $date_lang['title'] = $this->title_ru;
            $date_lang['description'] = $this->description_ru;
            $date_lang['keywords'] = $this->keywords_ru;
            $date_lang['body'] = $this->body_ru;
            $date_lang['id'] = $this->id;
            $date_lang['currency'] = '&#8372;';

        } elseif($lang == 'en') {

            $date_lang['title'] = $this->title_en;
            $date_lang['description'] = $this->description_en;
            $date_lang['keywords'] = $this->keywords_en;
            $date_lang['body'] = $this->body_en;
            $date_lang['id'] = $this->id;
            $date_lang['currency'] = '$';

        }elseif($lang == 'de'){

            $date_lang['title'] = $this->title_de;
            $date_lang['description'] = $this->description_de;
            $date_lang['keywords'] = $this->keywords_de;
            $date_lang['body'] = $this->body_de;
            $date_lang['id'] = $this->id;
            $date_lang['currency'] ="&#8364;";

        }elseif($lang == 'pl'){

            $date_lang['title'] = $this->title_pl;
            $date_lang['description'] = $this->description_pl;
            $date_lang['keywords'] = $this->keywords_pl;
            $date_lang['body'] = $this->body_pl;
            $date_lang['id'] = $this->id;
            $date_lang['currency'] ="zł";

        }
        
        return $date_lang;
    }
}
