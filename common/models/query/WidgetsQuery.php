<?php


namespace common\models\query;

use common\models\Widgets;
use yii\db\ActiveQuery;

class WidgetsQuery extends ActiveQuery
{
    public function published()
    {
        $this->andWhere(['status' => Widgets::STATUS_PUBLISHED]);
        
        return $this;
    }
}
