<?php

namespace common\models\query;

use common\models\CatalogCategory;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\CatalogCategory]].
 *
 * @see \common\models\CatalogCategory
 */
class CatalogCategoryQuery extends ActiveQuery
{
    
    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['status' => CatalogCategory::STATUS_ACTIVE]);

        return $this;
    }

    /**
     * @return $this
     */
    public function noParents()
    {
        $this->andWhere('{{%catalog_category}}.parent_id IS NULL');

        return $this;
    }
}
