<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/4/14
 * Time: 2:31 PM
 */

namespace common\models\query;

use common\models\Products;
use yii\db\ActiveQuery;

class ProductQuery extends ActiveQuery
{
    public function published()
    {
        $this->andWhere(['status' => Products::STATUS_PUBLISHED]);
        $this->andWhere(['<', '{{%product}}.published_at', time()]);
        return $this;
    }
}