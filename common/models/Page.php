<?php

namespace common\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $slug
 * @property string $keywords_ru
 * @property string $keywords_en
 * @property string $keywords_de
 * @property string $keywords_pl
 * @property string $description_ru
 * @property string $description_en
 * @property string $description_de
 * @property string $description_pl
 * @property string $title
 * @property string $title_en
 * @property string $title_de
 * @property string $title_pl
 * @property string $body
 * @property string $body_en
 * @property string $body_de
 * @property string $body_pl
 * @property string $view
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 */
class Page extends ActiveRecord
{
    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;

    public $hederimage;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
                'immutable' => true
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'hederimage',
                'pathAttribute' => 'img_heder_path',
                'baseUrlAttribute' => 'img_heder_base_url'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'body'], 'required'],
            [['body','body_en','body_de','body_pl',
            'keywords_ru','keywords_en','keywords_de','keywords_pl',
            'description_ru','description_en','description_de','description_pl',
            'title_en','title_de','title_pl'], 'string'],
            [['status'], 'integer'],
            [['slug'], 'unique'],
            [['slug'], 'string', 'max' => 2048],
            [['title','title_en','title_de'], 'string', 'max' => 512],
            [['view'], 'string', 'max' => 255],
            [['hederimage'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'slug' => Yii::t('common', 'Slug'),
            'title' => Yii::t('common', 'Title'),
            'title_en' => Yii::t('common', 'Title_EN'),
            'title_de' => Yii::t('common', 'Title_DE'),
            'title_pl' => Yii::t('common', 'Title_PL'),
            'body' => Yii::t('common', 'Body'),
            'body_en' => Yii::t('common', 'Body_EN'),
            'body_de' => Yii::t('common', 'Body_DE'),
            'body_pl' => Yii::t('common', 'Body_PL'),
            'keywords_ru' => Yii::t('common','Keywords_RU'),
            'keywords_de' => Yii::t('common','Keywords_DE'),
            'keywords_en' => Yii::t('common','Keywords_EN'),
            'keywords_pl' => Yii::t('common','Keywords_PL'),
            'description_ru' => Yii::t('common','Description_RU'),
            'description_en' => Yii::t('common','Description_EN'),
            'description_de' => Yii::t('common','Description_DE'),
            'description_pl' => Yii::t('common','Description_PL'),
            'view' => Yii::t('common', 'Page View'),
            'status' => Yii::t('common', 'Active'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),
            'hederimage' => Yii::t('common', 'Image of heder page'),
        ];
    }

      public function getDateLang($lang)
    {
        $date_lang = array();

        if ($lang == 'ru') {

            $date_lang['title'] = $this->title;
            $date_lang['description'] = $this->description_ru;
            $date_lang['keywords'] = $this->keywords_ru;
            $date_lang['body'] = $this->body;

        } elseif($lang == 'en') {

            $date_lang['title'] = $this->title_en;
            $date_lang['description'] = $this->description_en;
            $date_lang['keywords'] = $this->keywords_en;
            $date_lang['body'] = $this->body_en;

        } elseif($lang == 'de'){

            $date_lang['title'] = $this->title_de;
            $date_lang['description'] = $this->description_de;
            $date_lang['keywords'] = $this->keywords_de;
            $date_lang['body'] = $this->body_de;

        } elseif($lang == 'pl'){

            $date_lang['title'] = $this->title_pl;
            $date_lang['description'] = $this->description_pl;
            $date_lang['keywords'] = $this->keywords_pl;
            $date_lang['body'] = $this->body_pl;

        }
        
        return $date_lang;
    }

    public function getHederImageUrl()
    {
        //return rtrim($this->img_heder_base_url, '/') . '/' . ltrim($this->img_heder_path, '/');
        return 'https://bags-group.de/storage/web/source' . '/' . ltrim($this->img_heder_path, '/');
    }

}
