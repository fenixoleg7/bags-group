<?php

namespace common\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "seo".
 *
 * @property integer $id
 * @property string $slug
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_de
 * @property string $title_pl
 * @property string $description_ru
 * @property string $description_en
 * @property string $description_de
 * @property string $description_pl
 * @property string $keywords_ru
 * @property string $keywords_en
 * @property string $keywords_de
 * @property string $keywords_pl
 * @property string $thumbnail_base_url
 * @property string $thumbnail_path
 */
class Seo extends \yii\db\ActiveRecord
{
     /**
     * @var array
     */
    public $thumbnail;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['title_ru','title_en','title_de','title_pl','slug'], 'required'],
            [['slug'], 'unique'],
            [['description_ru','description_en','description_de','description_pl', 'keywords_ru','keywords_en','keywords_de','keywords_pl',], 'string'],          
            [['slug', 'thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],  
            [['thumbnail'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'slug' => Yii::t('common', 'ЧПУ'),
            'title_ru' => Yii::t('common', 'Title RU'),
            'title_en' => Yii::t('common', 'Title EN'),
            'title_de' => Yii::t('common', 'Title DE'),
            'title_pl' => Yii::t('common', 'Title PL'),
            'keywords_ru' => Yii::t('common','Keywords_RU'),
            'keywords_de' => Yii::t('common','Keywords_DE'),
            'keywords_en' => Yii::t('common','Keywords_EN'),
            'keywords_pl' => Yii::t('common','Keywords_PL'),
            'description_ru' => Yii::t('common','Description_RU'),
            'description_en' => Yii::t('common','Description_EN'),
            'description_de' => Yii::t('common','Description_DE'),
            'description_pl' => Yii::t('common','Description_PL'),
            'thumbnail' => Yii::t('common', 'Image of heder page'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\SeoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\SeoQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            //TimestampBehavior::className(),
            [
                'class'=>SluggableBehavior::className(),
                'attribute'=>'title_ru',
                'immutable' => true
            ],
            
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'thumbnail',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url'
            ]
        ];
    }

     /**
     * @return string
     */
    public function getImageUrl()
    {
       // return rtrim($this->thumbnail_base_url, '/') . '/' . ltrim($this->thumbnail_path, '/');
        return 'https://bags-group.de/storage/web/source' . '/' . ltrim($this->thumbnail_path, '/');
    }

    public function getDateLang($lang)
    {
        $date_lang = array();

        if ($lang == 'ru') {

            $date_lang['title'] = $this->title_ru;
            $date_lang['description'] = $this->description_ru;
            $date_lang['keywords'] = $this->keywords_ru;

        } elseif($lang == 'en') {

            $date_lang['title'] = $this->title_en;
            $date_lang['description'] = $this->description_en;
            $date_lang['keywords'] = $this->keywords_en;

        }elseif($lang == 'de'){

            $date_lang['title'] = $this->title_de;
            $date_lang['description'] = $this->description_de;
            $date_lang['keywords'] = $this->keywords_de;

        }elseif($lang == 'pl'){

            $date_lang['title'] = $this->title_pl;
            $date_lang['description'] = $this->description_pl;
            $date_lang['keywords'] = $this->keywords_pl;

        }
        
        return $date_lang;
    }
    

}
