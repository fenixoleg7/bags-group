<?php

namespace common\models;

use Yii;
//use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;

/**
 * This is the model class for table "site_info".
 *
 * @property integer $id
 * @property string $name_site
 * @property string $name_company
 * @property string $adress
 * @property string $email
 * @property string $phone_1
 * @property string $phone_2
 * @property string $fax
 * @property string $skype_url
 * @property string $linkedIn_url
 * @property string $Facebook_url
 * @property string $xing_url
 * @property string $logo_base_url
 * @property string $logo_heder_path
 * @property string $logo_footer_path
 * @property decimal $currency_1
 * @property decimal $currency_2
 * @property string $date_3
 * @property string $date_4
 * @property string $date_5
 */
class SiteInfo extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $attachments;
    public $logo_heder;
    public $logo_footer;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_info';
    }

     public function behaviors()
    {
        return [
            //TimestampBehavior::className(),
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'attachments',
                'multiple' => true,
                'uploadRelation' => 'siteinfoAttachments',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url',
                'orderAttribute' => 'order',
                'typeAttribute' => 'type',
                'sizeAttribute' => 'size',
                'nameAttribute' => 'name',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'logo_heder',
                'pathAttribute' => 'logo_heder_path',
                'baseUrlAttribute' => 'logo_base_url'
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'logo_footer',
                'pathAttribute' => 'logo_footer_path',
                'baseUrlAttribute' => 'logo_base_url'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_site', 'name_company'], 'required'],
            [['adress'], 'string'],
            [['name_site', 'name_company', 'email', 'phone_1', 'phone_2', 'fax', 'skype_url', 'linkedIn_url', 'Facebook_url', 'xing_url'], 'string', 'max' => 250],
            [['logo_base_url', 'logo_heder_path', 'logo_footer_path'], 'string', 'max' => 1024],
            [['date_3', 'date_4', 'date_5'], 'string', 'max' => 255],
            [['currency_1', 'currency_2'], 'number', 'numberPattern' => '/^\s*[-+]?[0-9]*[.,]?[0-9]+([eE][-+]?[0-9]+)?\s*$/'],
            [['attachments','logo_heder', 'logo_footer'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name_site' => Yii::t('common', 'Name Site'),
            'name_company' => Yii::t('common', 'Name Company'),
            'adress' => Yii::t('common', 'Adress'),
            'email' => Yii::t('common', 'Email'),
            'phone_1' => Yii::t('common', 'Phone 1'),
            'phone_2' => Yii::t('common', 'Phone 2'),
            'fax' => Yii::t('common', 'Fax'),
            'skype_url' => Yii::t('common', 'Skype Url'),
            'linkedIn_url' => Yii::t('common', 'LinkedIn Url'),
            'Facebook_url' => Yii::t('common', 'Facebook Url'),
            'xing_url' => Yii::t('common', 'Xing Url'),
            'logo_heder_path' => Yii::t('common', 'Logo Heder'),
            'logo_footer_path' => Yii::t('common', 'Logo Footer'),

        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\SiteInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\SiteInfoQuery(get_called_class());
    }

    public function getHederLogo()
    {
        //return rtrim($this->logo_base_url, '/') . '/' . ltrim($this->logo_heder_path, '/');
        return 'https://bags-group.de/storage/web/source' . '/' . ltrim($this->logo_heder_path, '/');
    }

    public function getFooterLogo()
    {
        //return rtrim($this->logo_base_url, '/') . '/' . ltrim($this->logo_footer_path, '/');
        return 'https://bags-group.de/storage/web/source' . '/' . ltrim($this->logo_footer_path, '/');
    }

     public function getSiteinfoAttachments()
    {
        return $this->hasMany(SiteinfoAttachment::className(), ['siteinfo_id' => 'id']);
    }

}
