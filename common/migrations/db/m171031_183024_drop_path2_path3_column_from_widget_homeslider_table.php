<?php

use yii\db\Migration;

/**
 * Handles dropping path2_path3 from table `widget_homeslider`.
 */
class m171031_183024_drop_path2_path3_column_from_widget_homeslider_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('widget_homeslider', 'path2');
        $this->dropColumn('widget_homeslider', 'path3');
        $this->addColumn('widget_homeslider', 'icon_span', $this->string(254));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('widget_homeslider', 'path2',$this->string(254));
        $this->addColumn('widget_homeslider', 'path3',$this->string(254));
        $this->dropColumn('widget_homeslider', 'icon_span');
    }

}
