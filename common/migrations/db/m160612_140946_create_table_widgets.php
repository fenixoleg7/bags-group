<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_widgets`.
 */
class m160612_140946_create_table_widgets extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%widgets}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(512)->notNull(),
            'page' => $this->integer(),
            'quantity' => $this->integer(),
            'status' => $this->smallInteger()->notNull(),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%widgets}}');
    }
}
