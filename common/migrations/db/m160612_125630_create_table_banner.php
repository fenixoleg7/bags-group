<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_banner`.
 */
class m160612_125630_create_table_banner extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
         $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%banner}}', [
            'id' => $this->primaryKey(),
            'base_url'=>$this->string(1024),
            'path'=>$this->string(1024),
            'type'=>$this->string(),
            'url' => $this->string(1024),
            'name_ru'=>$this->string(),
            'name_en'=>$this->string(),
            'name_de'=>$this->string(),
            'caption_ru' => $this->string(1024),
            'caption_en' => $this->string(1024),
            'caption_de' => $this->string(1024),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'page' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%banner}}');
    }
}
