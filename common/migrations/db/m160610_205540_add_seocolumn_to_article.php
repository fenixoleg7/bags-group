<?php

use yii\db\Migration;

/**
 * Handles adding seocolumn to table `article`.
 */
class m160610_205540_add_seocolumn_to_article extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('article', 'description_ru', $this->string());
        $this->addColumn('article', 'keywords_ru', $this->string());
        $this->addColumn('article', 'description_en', $this->string());
        $this->addColumn('article', 'keywords_en', $this->string());
        $this->addColumn('article', 'description_de', $this->string());
        $this->addColumn('article', 'keywords_de', $this->string());
        $this->addColumn('article', 'title_en', $this->string(512)->notNull());
        $this->addColumn('article', 'title_de', $this->string(512)->notNull());
        $this->addColumn('article', 'body_en', $this->text()->notNull());
        $this->addColumn('article', 'body_de', $this->text()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('article', 'description_ru');
        $this->dropColumn('article', 'keywords_ru');
        $this->dropColumn('article', 'description_en');
        $this->dropColumn('article', 'keywords_en');
        $this->dropColumn('article', 'description_de');
        $this->dropColumn('article', 'keywords_de');
        $this->dropColumn('article', 'title_en');
        $this->dropColumn('article', 'title_de');
        $this->dropColumn('article', 'body_en');
        $this->dropColumn('article', 'body_de');
    }
}
