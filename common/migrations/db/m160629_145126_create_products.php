<?php

use yii\db\Migration;

/**
 * Handles the creation for table `products`.
 */
class m160629_145126_create_products extends Migration
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
         $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

         $this->createTable('{{%catalog_category}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(1024)->notNull(),
            'title_ru' => $this->string(512)->notNull(),
            'title_en' => $this->string(512)->notNull(),
            'title_de' => $this->string(512)->notNull(),
            'keywords_ru' => $this->string(),
            'keywords_de' => $this->string(),
            'keywords_en' => $this->string(),
            'description_ru' => $this->string(),
            'description_en' => $this->string(),
            'description_de' => $this->string(),
            'body_ru' => $this->text(),
            'body_en' => $this->text(),
            'body_de' => $this->text(),
            'parent_id' => $this->integer(),
            'thumbnail_base_url' => $this->string(1024),
            'thumbnail_path' => $this->string(1024),
            'heder_image_path' => $this->string(1024),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(1024)->notNull(),
            'title_ru' => $this->string(512)->notNull(),
            'title_en' => $this->string(512)->notNull(),
            'title_de' => $this->string(512)->notNull(),
            'keywords_ru' => $this->string(),
            'keywords_de' => $this->string(),
            'keywords_en' => $this->string(),
            'description_ru' => $this->string(),
            'description_en' => $this->string(),
            'description_de' => $this->string(),
            'body_ru' => $this->text(),
            'body_en' => $this->text(),
            'body_de' => $this->text(),
            'kod' => $this->integer(),
            'size' =>$this->string(),
            'capacity' =>$this->string(),
            'prise_1' =>$this->decimal(2,2),
            'prise_2' =>$this->decimal(2,2),
            'prise_3' =>$this->decimal(2,2),
            'prise_4' =>$this->decimal(2,2),
            'prise_5' =>$this->decimal(2,2),
            'category_id' => $this->integer(),
            'thumbnail_base_url' => $this->string(1024),
            'heder_image_base_url' => $this->string(1024),
            'thumbnail_path_1' => $this->string(1024),
            'thumbnail_path_2' => $this->string(1024),
            'thumbnail_path_3' => $this->string(1024),
            'thumbnail_path_4' => $this->string(1024),
            'thumbnail_path_5' => $this->string(1024),
            'thumbnail_path_6' => $this->string(1024),
            'updater_id' => $this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'published_at' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_catalog_category', '{{%products}}', 'category_id', '{{%catalog_category}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_catalog_category_section', '{{%catalog_category}}', 'parent_id', '{{%catalog_category}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('catalog_products');

       
        $this->dropForeignKey('fk_catalog_category', '{{%products}}');
        $this->dropForeignKey('fk_catalog_category_section', '{{%catalog_category}}');

        $this->dropTable('{{%products}}');
        $this->dropTable('{{%catalog_category}}');

    }
}
