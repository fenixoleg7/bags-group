<?php

use yii\db\Migration;

/**
 * Handles adding seocolumn to table `page`.
 */
class m160610_124244_add_seocolumn_to_page extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('page', 'description_ru', $this->string());
        $this->addColumn('page', 'keywords_ru', $this->string());
        $this->addColumn('page', 'description_en', $this->string());
        $this->addColumn('page', 'keywords_en', $this->string());
        $this->addColumn('page', 'description_de', $this->string());
        $this->addColumn('page', 'keywords_de', $this->string());
        $this->addColumn('page', 'title_en', $this->string(512)->notNull());
        $this->addColumn('page', 'title_de', $this->string(512)->notNull());
        $this->addColumn('page', 'body_en', $this->text()->notNull());
        $this->addColumn('page', 'body_de', $this->text()->notNull());
 
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('page', 'description_ru');
        $this->dropColumn('page', 'keywords_ru');
        $this->dropColumn('page', 'description_en');
        $this->dropColumn('page', 'keywords_en');
        $this->dropColumn('page', 'description_de');
        $this->dropColumn('page', 'keywords_de');
        $this->dropColumn('page', 'title_en');
        $this->dropColumn('page', 'title_de');
        $this->dropColumn('page', 'body_en');
        $this->dropColumn('page', 'body_de');
    }
}
