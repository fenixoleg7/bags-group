<?php

use yii\db\Migration;

/**
 * Handles the creation for table `orders`.
 */
class m160727_131843_create_orders extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('orders', [
            'id' =>            $this->primaryKey(),
            'product_id' =>    $this->string(512)->notNull(),
            'product_name' =>  $this->string(512)->notNull(),
            'product_kod' =>   $this->integer(),
            'product_prise' => $this->decimal(10,6),
            'product_quantity' => $this->integer(),
            'client_name' =>   $this->string(254)->notNull(),
            'client_phone' =>  $this->string(254)->notNull(),
            'client_email' =>  $this->string(254)->notNull(),
            'client_message' => $this->text(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('orders');

    }
}
