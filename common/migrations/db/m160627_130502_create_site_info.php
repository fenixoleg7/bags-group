<?php

use yii\db\Migration;

/**
 * Handles the creation for table `site_info`.
 */
class m160627_130502_create_site_info extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('site_info', [
            'id' => $this->primaryKey(),
            'name_site' => $this->string(250)->notNull(),
            'name_company' => $this->string(250)->notNull(),
            'adress' => $this->text(),
            'email' =>  $this->string(250)->notNull(),
            'phone_1' =>  $this->string(250)->notNull(),
            'phone_2' =>  $this->string(250)->notNull(),
            'fax' =>  $this->string(250)->notNull(),
            'skype_url' =>  $this->string(250)->notNull(),
            'linkedIn_url' =>  $this->string(250)->notNull(),
            'Facebook_url' =>  $this->string(250)->notNull(),
            'xing_url' =>  $this->string(250)->notNull(),
            'logo_base_url' => $this->string(1024),
            'logo_heder_path' => $this->string(1024),
            'logo_footer_path' => $this->string(1024),
            'currency_1' => $this->decimal(),
            'currency_2' => $this->decimal(),
            'date_3' => $this->string()->notNull(),
            'date_4' => $this->string()->notNull(),
            'date_5' => $this->string()->notNull(),
            
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('site_info');
    }
}
