<?php

use yii\db\Migration;

/**
 * Handles the creation for table `widget_work_schema`.
 */
class m160621_111219_create_widget_work_schema extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('widget_work_schema', [
            'id' => $this->primaryKey(),
            'step'=>$this->integer(),
            'base_url'=>$this->string(1024),
            'path'=>$this->string(1024),
            'title_ru'=>$this->string(),
            'title_en'=>$this->string(),
            'title_de'=>$this->string(),
            'text_ru' => $this->text()->notNull(),
            'text_en' => $this->text()->notNull(),
            'text_de' => $this->text()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('widget_work_schema');
    }
}
