<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_seo`.
 */
class m160612_125523_create_table_seo extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%seo}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(2048)->notNull(),
            'title_ru' => $this->string(),
            'title_en' => $this->string(),
            'title_de' => $this->string(),
            'body_ru' => $this->string(),
            'body_en' => $this->string(),
            'body_de' => $this->string(),
            'keywords_ru' => $this->string(),
            'keywords_de' => $this->string(),
            'keywords_en' => $this->string(),
            'description_ru' => $this->string(),
            'description_en' => $this->string(),
            'description_de' => $this->string(),
            'thumbnail_base_url' => $this->string(1024),
            'thumbnail_path' => $this->string(1024),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%seo}}');
    }
}
