<?php

use yii\db\Migration;

/**
 * Handles the creation for table `widget_advantages`.
 */
class m160615_185918_create_widget_advantages extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('widget_advantages', [
            'id' => $this->primaryKey(),
            'name_ru'=>$this->string(),
            'name_en'=>$this->string(),
            'name_de'=>$this->string(),
            'title_ru'=>$this->string(),
            'title_en'=>$this->string(),
            'title_de'=>$this->string(),
            'text_ru' => $this->text()->notNull(),
            'text_en' => $this->text()->notNull(),
            'text_de' => $this->text()->notNull(),
            'patch'=> $this->string(),
            'percent' =>$this->integer(),
            'advantes_id' =>$this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('widget_advantages');
    }
}
