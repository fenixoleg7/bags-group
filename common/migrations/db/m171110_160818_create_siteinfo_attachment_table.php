<?php

use yii\db\Migration;

/**
 * Handles the creation for table `siteinfo_attachment`.
 */
class m171110_160818_create_siteinfo_attachment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

//        $tableOptions = null;
//        if ($this->db->driverName === 'mysql') {
//            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
//        }
//
//        $this->createTable('siteinfo_attachment', [
//            'id' => $this->primaryKey(),
//            'siteinfo_id' => $this->integer()->notNull(),
//            'path' => $this->string()->notNull(),
//            'base_url' => $this->string(),
//            'type' => $this->string(),
//            'size' => $this->integer(),
//            'name' => $this->string(),
//            'created_at' => $this->integer()
//        ], $tableOptions);
//
//        $this->addForeignKey('fk_siteinfo_attachment_siteinfo', '{{%siteinfo_attachment}}', 'siteinfo_id', '{{%site_info}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
//        $this->dropForeignKey('fk_siteinfo_attachment_siteinfo', '{{%siteinfo_attachment}}');
//        $this->dropTable('siteinfo_attachment');
    }
}
