<?php

use yii\db\Migration;

/**
 * Class m200718_112059_add_pl_language
 */
class m200718_112059_add_pl_language extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('page', 'description_pl', $this->string());
        $this->addColumn('page', 'keywords_pl', $this->string());
        $this->addColumn('page', 'title_pl', $this->string(512)->notNull());
        $this->addColumn('page', 'body_pl', $this->text()->notNull());

        $this->addColumn('article', 'description_pl', $this->string());
        $this->addColumn('article', 'keywords_pl', $this->string());
        $this->addColumn('article', 'title_pl', $this->string(512)->notNull());
        $this->addColumn('article', 'body_pl', $this->text()->notNull());

        $this->addColumn('seo', 'description_pl', $this->string());
        $this->addColumn('seo', 'keywords_pl', $this->string());
        $this->addColumn('seo', 'title_pl', $this->string(512)->notNull());
        $this->addColumn('seo', 'body_pl', $this->text()->notNull());

        $this->addColumn('banner', 'name_pl', $this->string());
        $this->addColumn('banner', 'caption_pl', $this->string(1024));
        $this->addColumn('banner', 'path_pl', $this->string());

        $this->addColumn('widget_homeslider', 'title_pl', $this->string());
        $this->addColumn('widget_homeslider', 'caption_pl_1', $this->string());
        $this->addColumn('widget_homeslider', 'caption_pl_2', $this->string(1024));

        $this->addColumn('widget_advantages', 'name_pl', $this->string());
        $this->addColumn('widget_advantages', 'title_pl', $this->string());
        $this->addColumn('widget_advantages', 'text_pl', $this->string());

        $this->addColumn('widget_work_schema', 'title_pl', $this->string());
        $this->addColumn('widget_work_schema', 'text_pl', $this->string());

        $this->addColumn('catalog_category', 'title_pl', $this->string(512));
        $this->addColumn('catalog_category', 'keywords_pl', $this->string(512));
        $this->addColumn('catalog_category', 'description_pl', $this->string());
        $this->addColumn('catalog_category', 'body_pl', $this->string());

        $this->addColumn('products', 'title_pl', $this->string(512));
        $this->addColumn('products', 'keywords_pl', $this->string(512));
        $this->addColumn('products', 'description_pl', $this->string());
        $this->addColumn('products', 'body_pl', $this->string());

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $this->dropColumn('page', 'description_pl');
        $this->dropColumn('page', 'keywords_pl');
        $this->dropColumn('page', 'title_pl');
        $this->dropColumn('page', 'body_pl');

        $this->dropColumn('article', 'description_pl');
        $this->dropColumn('article', 'keywords_pl');
        $this->dropColumn('article', 'title_pl');
        $this->dropColumn('article', 'body_pl');

        $this->dropColumn('seo', 'description_pl');
        $this->dropColumn('seo', 'keywords_pl');
        $this->dropColumn('seo', 'title_pl');
        $this->dropColumn('seo', 'body_pl');

        $this->dropColumn('banner', 'name_pl');
        $this->dropColumn('banner', 'caption_pl');
        $this->dropColumn('banner', 'path_pl');

        $this->dropColumn('widget_homeslider', 'title_pl');
        $this->dropColumn('widget_homeslider', 'caption_pl_1');
        $this->dropColumn('widget_homeslider', 'caption_pl_2');

        $this->dropColumn('widget_advantages', 'name_pl');
        $this->dropColumn('widget_advantages', 'title_pl');
        $this->dropColumn('widget_advantages', 'text_pl');

        $this->dropColumn('widget_work_schema', 'title_pl');
        $this->dropColumn('widget_work_schema', 'text_pl');

        $this->dropColumn('catalog_category', 'title_pl');
        $this->dropColumn('catalog_category', 'keywords_pl');
        $this->dropColumn('catalog_category', 'description_pl');
        $this->dropColumn('catalog_category', 'body_pl');

        $this->dropColumn('products', 'title_pl');
        $this->dropColumn('products', 'keywords_pl');
        $this->dropColumn('products', 'description_pl');
        $this->dropColumn('products', 'body_pl');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200718_112059_add_pl_language cannot be reverted.\n";

        return false;
    }
    */
}
