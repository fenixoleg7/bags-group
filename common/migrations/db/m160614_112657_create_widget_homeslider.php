<?php

use yii\db\Migration;

/**
 * Handles the creation for table `widget_homeslider`.
 */
class m160614_112657_create_widget_homeslider extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('widget_homeslider', [
            'id' => $this->primaryKey(),
            'path1'=>$this->string(254),
            'path2'=>$this->string(254),
            'path3'=>$this->string(254),
            'path4'=>$this->string(254),
            'caption_ru_1' => $this->string(254),
            'caption_ru_2' => $this->string(254),
            'caption_en_1' => $this->string(254),
            'caption_en_2' => $this->string(254),
            'caption_de_1' => $this->string(254),
            'caption_de_2' => $this->string(254),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'order' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('widget_homeslider');
    }
}
