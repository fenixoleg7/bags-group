<?php

use yii\db\Migration;

/**
 * Handles adding path to table `banner`.
 */
class m160905_181242_add_path_column_to_banner_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('banner', 'path_en', $this->string());
        $this->addColumn('banner', 'path_de', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('banner', 'path_en');
        $this->dropColumn('banner', 'path_de');
    }
}
