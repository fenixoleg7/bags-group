<?php

use yii\db\Migration;

/**
 * Class m181204_102917_alter_columns_from_products_table
 */
class m181204_102917_alter_columns_from_products_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('products','price_1','string');
        $this->alterColumn('products','price_2','string');
        $this->alterColumn('products','price_3','string');
        $this->alterColumn('products','price_4','string');
        $this->alterColumn('products','price_5','string');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('products','price_1','decimal');
        $this->alterColumn('products','price_2','decimal');
        $this->alterColumn('products','price_3','decimal');
        $this->alterColumn('products','price_4','decimal');
        $this->alterColumn('products','price_5','decimal');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181204_102917_alter_columns_from_products_table cannot be reverted.\n";

        return false;
    }
    */
}
