<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CatalogCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="catalog-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_de')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keywords_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keywords_de')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keywords_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_de')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body_ru')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'body_en')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'body_de')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'parent_id')->textInput() ?>

    <?= $form->field($model, 'thumbnail_base_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'thumbnail_path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
