jQuery(document).ready(function(){
	jQuery('.flexslider').flexslider({
		animation: "slide"
	  });
	jQuery( "#orderform-product_quantity" ).change(function() {
		var quantity = $( this ).val();
		var prise_1 = $('#price_1').val();
		var prise_2 = $('#price_2').val();
		var prise_3 = $('#price_3').val();
		var prise_4 = $('#price_4').val();
		var prise_5 = $('#price_5').val();
		var sum_prise ='';
		var prise ='';

		if(quantity < 1000)
		{
			prise = prise_1;
			sum_prise = prise_1*quantity;
		}else if(quantity > 1000 && quantity <= 5000){
			prise = prise_2;
			sum_prise = prise_2*quantity;
		}else if(quantity > 5000 && quantity <= 10000){
			prise = prise_3;
			sum_prise = prise_3*quantity;
		}else if(quantity > 10000 && quantity <= 50000){
			prise = prise_4;
			sum_prise = prise_4*quantity;
		}else{
			prise = prise_5;
			sum_prise = prise_5*quantity;
		}

		jQuery('#orderform-product_price').val(prise);
		jQuery('#cost').text(sum_prise);
		jQuery('#total_cost').val(sum_prise);

	});
});