<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;


class FrontendBagsGroupAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        //'css/fractionslider.css',
    ];

    public $js = [

        //'js/jquery.fractionslider.js',
        'js/parallax.js',
        /*'js/circle-progress.js',*/
        'js/main.js',
        
    ];

    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
        'common\assets\Html5shiv',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
