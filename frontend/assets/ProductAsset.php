<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ProductAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [

        'css/flexslider.css',
        /*'css/responsive.css',
        'css/jquery.fancybox.css',
        'css/icomoon.css'
        'css/jquery.bxslider.css'*/

    ];

    public $js = [

        'js/jquery.flexslider.js',
        /*'js/cloudzoom.js',
        'js/jquery.fancybox.js?v=2.1.5',
        'bx-slider/jquery.bxslider.min.js',*/
        //'bx-slider/jquery.bxslider.js',
        'js/product.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
        //'common\assets\Html5shiv',
    ];
}
