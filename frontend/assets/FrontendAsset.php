<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;


class FrontendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/style.css',
        'css/styles.css',
        'css/responsive.css',
        'css/reset.css',
        'css/jasny-bootstrap.css',
        'css/icomoon.css',
        'bx-slider/jquery.bxslider.css'

    ];

    public $js = [
        
        'js/jasny-bootstrap.min.js',
        'js/script.js',
        'js/app.js',
        'js/masked-input-plagin.js',
        'bx-slider/jquery.bxslider.js',
        //'js/bootstrap.min.js',


    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'common\assets\Html5shiv',
    ];
}
