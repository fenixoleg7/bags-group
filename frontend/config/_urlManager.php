<?php
return [
    'class'=>'yii\web\UrlManager',
    'enablePrettyUrl'=>true,
    'showScriptName'=>false,
    //'enableLanguageDetection'=>false,
    'enableLanguagePersistence' => true,
    'rules'=> [
        
        // Pages
        ['pattern'=>'page/<slug>', 'route'=>'page/view'],

        // Articles
        ['pattern'=>'article/index', 'route'=>'article/index'],
        ['pattern'=>'article/attachment-download', 'route'=>'article/attachment-download'],
        ['pattern'=>'article/<slug>', 'route'=>'article/view'],

        // Catalog
        ['pattern'=>'catalog/index', 'route'=>'catalog/index'],
        ['pattern'=>'catalog/category/<slug>', 'route'=>'catalog/category'],
        ['pattern'=>'catalog/products/<slug>', 'route'=>'catalog/products'],
        ['pattern'=>'catalog/product/<id>', 'route'=>'catalog/product'],

        // Search
        ['pattern'=>'search/query_user', 'route'=>'search/index'],
        //['pattern'=>'search/searchtitle', 'route'=>'search/search'],


        // Api
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/article', 'only' => ['index', 'view', 'options']],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/user', 'only' => ['index', 'view', 'options']]
    ]
];
