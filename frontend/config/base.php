<?php
return [
    'id' => 'frontend',
    'basePath' => dirname(__DIR__),
    'components' => [
        //'urlManager' => require(__DIR__.'/_urlManager.php'),
	    'urlManager' => [
	            'class' => 'codemix\localeurls\UrlManager',
	            //'enableLanguageDetection'=>true,
	            // List all supported languages here
	            'enableDefaultLanguageUrlCode' => true,
	            //'languageCookieDuration'=>false,
	            'languages' => ['de', 'en', 'ru','pl'],
	            'enablePrettyUrl'=>true,
			    'showScriptName'=>false,
			    'rules'=> [

			    	['pattern'=>'', 'route'=>'site/index'],
			    	['pattern'=>'contact', 'route'=>'site/contact'],
			    	['pattern'=>'samples', 'route'=>'site/samples'],
			        // Pages
			        ['pattern'=>'page/<slug>', 'route'=>'page/view'],

			        // Articles
			        ['pattern'=>'article/index', 'route'=>'article/index'],
			        ['pattern'=>'article/attachment-download', 'route'=>'article/attachment-download'],
			        ['pattern'=>'article/<slug>', 'route'=>'article/view'],

			        // Catalog
			        ['pattern'=>'catalog/index', 'route'=>'catalog/index'],
			        ['pattern'=>'catalog/category/<slug>', 'route'=>'catalog/category'],
			        ['pattern'=>'catalog/products/<slug>', 'route'=>'catalog/products'],
			        ['pattern'=>'catalog/product/<id>', 'route'=>'catalog/product'],

			        // Search
			        ['pattern'=>'search/query_user', 'route'=>'search/index'],
			        //['pattern'=>'search/searchtitle', 'route'=>'search/search'],


			        // Api
			        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/article', 'only' => ['index', 'view', 'options']],
			        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/user', 'only' => ['index', 'view', 'options']],
			        ['pattern' => 'robots', 'route' => 'robotsTxt/web/index', 'suffix' => '.txt'],
			    ]
	        ],

        'cache' => require(__DIR__.'/_cache.php'),
    ],
    
    
];
