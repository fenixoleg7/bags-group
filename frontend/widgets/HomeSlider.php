<?php
namespace frontend\widgets;

use common\models\WidgetHomeslider;
use yii\bootstrap\Widget;
use yii\helpers\Html;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use Yii;


class HomeSlider extends Widget
{
    /**
     * @var
     */
    public $key="1234";

    public $items = [];

    //public $pager='';

    public $language = '';

    public $titles =[];

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        //$this->language=Yii::$app->request->cookies->getValue('_locale');
        $this->language=Yii::$app->language;


        if (!$this->key) {
            throw new InvalidConfigException;
        }
        $cacheKey = [
            WidgetHomeslider::className(),
            $this->language
        ];
        $items = Yii::$app->cache->get($cacheKey);
        if ($items === false) {
            $items = [];
            $model = WidgetHomeslider::find()->all();
            foreach ($model as $k => $item) {
                $sub_content='';
                $pager='<a data-slide-index="'.$k.'" href="#">';

                if ($item->path1) {
                    $sub_content =$sub_content.Html::img($item->getImageUrl($item->path1));
                }
               
                if($this->language =='ru'){
                    if ($item->caption_ru_1) {
                        $sub_content = $sub_content.Html::tag('p', $item->caption_ru_1, ['class' => 'teaser red']);
                    }
                    if ($item->caption_ru_2) {
                        $sub_content = $sub_content.Html::tag('p', $item->caption_ru_2, ['class' => 'teaser grey']);
                    }

                    $this->titles[]=$item->title_ru;
                    $pager= $pager.$item->icon_span;
                    $pager= $pager.'<span class="visible-lg visible-md" >'.$item->title_ru.'</span> </a>';

                }elseif($this->language =='en'){
                     if ($item->caption_en_1) {
                        $sub_content = $sub_content.Html::tag('p', $item->caption_en_1, ['class' => 'teaser red']);
                    }
                    if ($item->caption_en_2) {
                        $sub_content = $sub_content.Html::tag('p', $item->caption_en_2, ['class' => 'teaser grey']);
                    }

                     $this->titles[]=$item->title_en;
                    $pager= $pager.$item->icon_span;
                    $pager= $pager.'<span class="visible-lg visible-md" >'.$item->title_en.'</span> </a>';

                }elseif($this->language =='de'){
                    if ($item->caption_de_1) {
                        $sub_content = $sub_content.Html::tag('p', $item->caption_de_1, ['class' => 'teaser red']);
                    }
                    if ($item->caption_de_2) {
                        $sub_content = $sub_content.Html::tag('p', $item->caption_de_2, ['class' => 'teaser grey']);
                    }

                    $this->titles[]=$item->title_de;
                    $pager= $pager.$item->icon_span;
                    $pager= $pager.'<span class="visible-lg visible-md" >'.$item->title_de.'</span> </a>';
                }else{
                    if ($item->caption_pl_1) {
                        $sub_content = $sub_content.Html::tag('p', $item->caption_pl_1, ['class' => 'teaser red']);
                    }
                    if ($item->caption_pl_2) {
                        $sub_content = $sub_content.Html::tag('p', $item->caption_pl_2, ['class' => 'teaser grey']);
                    }

                    $this->titles[]=$item->title_pl;
                    $pager= $pager.$item->icon_span;
                    $pager= $pager.'<span class="visible-lg visible-md" >'.$item->title_pl.'</span> </a>';
                }
                
                if ($item->path4) {
                    $items[$k]['content'] = $sub_content.Html::img($item->getImageUrl($item->path4),['class' => 'teaser img-responsive']);
                }

              //$this->pager=$this->pager.$pager;
                 $items[$k]['pager']=$pager;
            }
            
        }
        Yii::$app->cache->set($cacheKey, $items, 60*60*24*365);
        $this->items = $items;
        parent::init();
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        $this->registerPlugin('carousel');
        $content = '';
        $pager='';
        if (!empty($this->items)) {
            $content = implode("\n", [
                $this->renderItems(),
            ]);
            $pager=implode("\n", [
                $this->renderPager(),
            ]);
        }
        return $this->render('homeslider',['content'=>$content, 'titles'=> $this->titles,
            'pager'=> $pager
            ]);
        //return Html::tag('div', $content, $this->options);
    }

    /**
     * Renders carousel items as specified on [[items]].
     * @return string the rendering result
     */
    public function renderItems()
    {
        $items = [];
        for ($i = 0, $count = count($this->items); $i < $count; $i++) {
            $items[] = $this->renderItem($this->items[$i], $i);
        }

        return Html::tag('ul', implode("\n", $items),['class' => 'bxslider']);
    }

    public function renderPager()
    {
        $items_pager = [];
        for ($i = 0, $count = count($this->items); $i < $count; $i++) {
            $items_pager[] = $this->renderItemPager($this->items[$i], $i);
        }

        return Html::tag('div ', implode("\n", $items_pager),['id' => 'bx-pager']);
    }

    /**
     * Renders a single carousel item
     * @param string|array $item a single item from [[items]]
     * @param integer $index the item index as the first item should be set to `active`
     * @return string the rendering result
     * @throws InvalidConfigException if the item is invalid
     */
    public function renderItem($item, $index)
    {
        if (is_string($item)) {
            $content = $item;
            $caption = null;
            $options = [];
        } elseif (isset($item['content'])) {
            $content = $item['content'];
            $caption = ArrayHelper::getValue($item, 'caption');
            if ($caption !== null) {
               $caption = Html::tag('div', $caption, ['class' => 'carousel-caption']);
            }
            $options = ArrayHelper::getValue($item, 'options', []);
        } else {
            throw new InvalidConfigException('The "content" option is required.');
        }
        return Html::tag('li', $content . "\n" . $caption, $options);
    }


     public function renderItemPager($item, $index)
    {

               $caption = $item['pager'];

        return  $caption;
    }
}
