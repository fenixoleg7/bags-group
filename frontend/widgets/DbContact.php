<?php

namespace frontend\widgets;

//use common\models\WidgetText;
use frontend\models\ContactForm;
use frontend\models\FooterContactForm;

use yii\base\Widget;
use Yii;


/**
 * Class DbContact
 * The widget send e-mail und message for admin
 * keys:
 *   footer-contact - show logo in heder
 *   popup-contact - show logo in footer
  */ 
class DbContact extends Widget
{
    /**
     * @var string text block key
     */
    public $key;

    /**
     * @return string
     */
    public function run()
    {
       
       if($this->key == 'footer-contact'){
            $model = new FooterContactForm();
            $otvet="";
            $alert_value='';
            
            if ($model->load(Yii::$app->request->post())&& $model->validate())
                {
                   if ($model->contact(Yii::$app->params['adminEmail']))
                   {
                      $otvet = Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible.');
                      $alert_value = 'alert-success';
                      $model = new ContactForm();
                      $model->scenario = 'footer-contact';
                      
                   }else{
                      $otvet = Yii::t('frontend', 'There was an error sending email.');
                      $alert_value = 'alert-danger';
                      $model = new ContactForm();
                      $model->scenario = 'footer-contact';
                  
                   }
                }
            
            return $this->render('dbcontact-footer', ['model' => $model,'otvet'=>$otvet,'alert_value'=>$alert_value]);
                
            
       }
       else{
            $model = new ContactForm();
            $model->scenario = 'popup-contact';
            $otvet_pop="";
            $alert_value_pop='';
            if ($model->load(Yii::$app->request->post())) {
                if ($model->order(Yii::$app->params['adminEmail'])) {
                    $otvet_pop = Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible.');
                    $alert_value_pop = 'alert-success';
                    $model = new ContactForm();
                    $model->scenario = 'popup-contact';
                } else {
                    $otvet_pop = Yii::t('frontend', 'There was an error sending email.');
                      $alert_value_pop = 'alert-danger';
                      $model = new ContactForm();
                      $model->scenario = 'popup-contact';
                }
            }

            return $this->render('dbcontact', ['model' => $model,'otvet_pop'=>$otvet_pop,'alert_value_pop'=>$alert_value_pop]);
       }
        
       
    }
}
