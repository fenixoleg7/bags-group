<?php
/**
 * Eugine Terentev <eugine@terentev.net>
 */

namespace frontend\widgets;

use common\models\Article;
use yii\bootstrap\Widget;
use yii\helpers\Html;
use Yii;

/**
 * Class Banner
 * @package common\widgets
 */
class DbLastNews extends Widget
{
    /**
     * @var
     */
    public $page = '';
    public $count = 2;
    public $language = '';

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
       // $this->language=Yii::$app->request->cookies->getValue('_locale');
        $this->language=Yii::$app->language;
        $model = Article::find()->orderby(['id'=>SORT_DESC])
        ->published()
        ->limit($this->count)
        ->all();

        return $this->render('dblastnews',['lastnews'=>$model, 'language' => $this->language]);
    }

   
}
