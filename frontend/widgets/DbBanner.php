<?php
/**
 * Eugine Terentev <eugine@terentev.net>
 */

namespace frontend\widgets;

use common\models\Banner;
use yii\bootstrap\Widget;
use yii\helpers\Html;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class Banner
 * @package common\widgets
 */
class DbBanner extends Widget
{
    /**
     * @var
     */
    public $key = 'banner';

    public $page = '';

    public $var='';

     public $language = '';
    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        if ($this->var === null) {
            $this->var = '1';
        }
            parent::init();
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        $this->language=Yii::$app->language;
         $cacheKey = [
           Banner::className(),
            $this->language
        ];

        

        $content = Yii::$app->cache->get($cacheKey);

        if (!$content) {

            $banner =  Banner::findOne(['page' => $this->page, 'status' => Banner::STATUS_ACTIVE]);
            $options = [
                'class'=>'img-responsive'
            ];
            if($this->language == 'ru'){

                $content= Html::img($banner->getImageLangUrl($this->language), ['alt' =>$banner->name_ru, 'class'=>'img-responsive']);

            }elseif($this->language == 'en'){

                $content= Html::img($banner->getImageLangUrl($this->language), ['alt' =>$banner->name_en, 'class'=>'img-responsive']); 
            }else {

                $content= Html::img($banner->getImageLangUrl($this->language), ['alt' =>$banner->name_de, 'class'=>'img-responsive']);
            }

            Yii::$app->cache->set($cacheKey, $content, 60*60*24);

        }

         return $this->render('dbbanner',['content'=>$content, 'var'=>$this->var]);
    }

   
}
