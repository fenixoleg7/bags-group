<?php  ?>
<div class="bg-title-block">
  <h2 class="bg-title-block-h2"><?php echo Yii::t('frontend', 'Product catalog'); ?></h2>
</div>
<?php echo $content; ?>
<div class="row">
  <div class="col-md-12 text-center">
    <a href="/catalog" class="catalog-btn"><?php echo Yii::t('frontend', 'All goods'); ?></a>
  </div>
</div>