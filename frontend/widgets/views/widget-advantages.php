<?php
use yii\helpers\Html; 
?>
<section class="parallax-window" data-parallax="scroll" data-image-src="/images/paralax-img.png">
   <div id="bg-advantages" class="container-fluid bg-advantages">
    <div class="row">
     <div class="col-md-9 text-center">
      <h2 ><?php echo Yii::t('frontend', 'Our Benefits'); ?></h2>
    </div>
    </div>
    <div class="row">
    
      <?php echo $content ; ?>

    </div>
    <div class="row">
      <div  class="col-md-9 text-center">
        <a href="#" class="catalog-btn"><?php echo Yii::t('frontend','advantages') ?></a>
      </div>
    </div>
   </div>         
</section>

<?php $this->registerJs('$(window).scroll(function() {if ($(this).scrollTop() > 500) {'.$percents.'} });'); ?>
        
               
          
   
     