<?php
use yii\helpers\Html; 
use frontend\widgets\DbSiteInfo;
?>
<div class="slider-wrapper hidden-xs">
    <div class="responisve-container home-slider-2">
   		<a class="bg-logo-slider-a" href="#" >
            <?php /*echo Html::img('/images/logo.png',['alt'=>'Logo','class'=>'bg-logo-slider'])*/ ?>
            <?php echo DbSiteInfo::widget([
              'key' => 'logo-heder'
            ]) ?>
        </a> 
        
    	<?php echo  $content;?>  
    
 
      <?php echo  $pager;?>

    </div>
</div> 

