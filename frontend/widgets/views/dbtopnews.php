<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\Article
 */
use yii\helpers\Html;

?>

<div class="b-mod m-lastnews">
    <h3 class="b-mod__title">Популярные новости</h3>
    <ul class="b-mod__list">
        <?php foreach ($topnews as $tnews):?>
		
			<li class="b-mod__item">
				<?php echo Html::a(
                    $tnews->title,
                    ['/article/'.$tnews->slug],
                    ['class' => 'b-mod__item--link']
                )?>
	            
	            <span class="b-mod__item--meta"><?php echo Yii::$app->formatter->asDatetime($tnews->created_at,'dd.MM');?></span>

	        </li>

		<?php endforeach; ?>
    </ul>
</div><!-- b-mod -->