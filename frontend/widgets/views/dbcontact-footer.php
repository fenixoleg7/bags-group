<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\bootstrap\Alert;
use yii\captcha\Captcha;


?>


<?php Pjax::begin(['id' => 'formPjax']) ?>
<?php if($otvet != "")
       {
           echo Alert::widget([
            'options' => [
                'class' =>$alert_value 
            ],
            'body' => $otvet,
            ]);
        }
?>
<?php $form = ActiveForm::begin(['id' => 'contact-form-footer','options' => ['data-pjax' => true]]); ?>

         


          <p>
            <?php echo Yii::t('frontend','Text-contact-footer'); ?>
          </p>
          <div class="input-group">
            <?php echo $form->field($model, 'email')->textInput(['placeholder'=>Yii::t('frontend','Enter your e-mail'), 'class'=>'form-control'])->label(false); ?>
            <span class="input-group-btn">
              <?php echo Html::submitButton('<span class="glyphicon glyphicon-envelope"></span>', ['class' => 'btn bg-btn']) ?>
            </span>
          </div>


<?php ActiveForm::end(); ?>
<?php Pjax::end() ?>
      
