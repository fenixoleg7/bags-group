<section>
   <div class="container-fluid bg-shema-work">
    <div class="row">
      <div class="col-md-9 text-center">
        <div class="bg-title-block">
          <h2 class="bg-title-block-h2"><?php echo Yii::t('frontend', 'Scheme of work'); ?></h2>
        </div>
      </div>
      </div>
    <div class="row">
        <div class="col-md-12 col-lg-12 text-center">
          <div class="row">
            <hr class="red-line hidden-sm hidden-xs">

            <?php echo $content; ?>
            
          </div>
          <div class="row">
            <div class="col-md-12 col-lg-12 text-center">
              <a href="#" class="catalog-btn visible-lg visible-md" data-toggle="modal" data-target="#order1"><?php echo Yii::t('frontend','To order')?></a>
            </div>
          </div>
       </div>
    </div>
    </div>
</section>