<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\Article
 */
use yii\helpers\Html;

?>

<div class="prew-news">
    <h2><?php echo Yii::t('frontend', 'News'); ?></h2>
        <?php foreach ($lastnews as $news):?>
    		<?php if($language == 'ru'): ?>
            
    		    <div class="prew-news-item">
                  <div class="prew-img text-center">
                    <!--<img src="/images/news-prew-1.png"  class="img-responsive" alt="...">-->
                   
                    <?= Html::img($news->getImageUrl(), ['alt' => '']) ?>

                    <span class="prew-news-date">
                        <?php echo Yii::$app->formatter->asDatetime($news->created_at,'dd.MM');?>
                    </span>
                  </div>
                  <a href="/article/<?php echo $news->slug; ?>">
                      <?php echo Html::tag('h3',$news->title_ru) ?>
                      <?= Html::tag('p', \yii\helpers\StringHelper::truncate($news->body_ru,150,'...')) ?>
                      <p class="link-news text-right"><?php echo Yii::t('frontend', 'More') ?><span class="glyphicon glyphicon-play"></span></p>
                   </a>
                </div>

            <?php elseif($language == 'en'): ?>
          
                <div class="prew-news-item">
                  <div class="prew-img text-center">
                    <!--<img src="/images/news-prew-1.png"  class="img-responsive" alt="...">-->
                    <?= Html::img($news->getImageUrl(), ['alt' => '']) ?>
                    <span class="prew-news-date">
                        <?php echo Yii::$app->formatter->asDatetime($news->created_at,'dd.MM');?>
                    </span>
                  </div>
                  <a href="/article/<?php echo $news->slug; ?>">
                      <?= Html::tag('h3', $news->title_en) ?>
                      <?= Html::tag('p', \yii\helpers\StringHelper::truncate($news->body_en,150,'...')) ?>
                      <p class="link-news text-right"><?php echo Yii::t('frontend', 'More') ?><span class="glyphicon glyphicon-play"></span></p>
                  </a>
                </div>

    	    <?php else: ?>
            
                <div class="prew-news-item">
                  <div class="prew-img text-center">
                    <!--<img src="/images/news-prew-1.png"  class="img-responsive" alt="...">-->
                    <?= Html::img($news->getImageUrl(), ['alt' => '']) ?>
                    <span class="prew-news-date">
                        <?php echo Yii::$app->formatter->asDatetime($news->created_at,'dd.MM');?>
                    </span>
                  </div>
                   <a href="/article/<?php echo $news->slug; ?>">
                      <?= Html::tag('h3', $news->title_de) ?>
                      <?= Html::tag('p', \yii\helpers\StringHelper::truncate($news->body_de,150,'...')) ?>
                      <p class="link-news text-right"><?php echo Yii::t('frontend', 'More') ?><span class="glyphicon glyphicon-play"></span></p>
                   </a>
                </div>

            <?php endif; ?>

		<?php endforeach; ?>
        <div class="text-center">
          <a href="/article" class="catalog-btn"><?php echo Yii::t('frontend','All news'); ?></a>
        </div>
    
</div><!-- b-mod -->