<?php
use yii\helpers\Html;
use himiklab\thumbnail\EasyThumbnailImage;
?>
<div class="b-mod m-default clearfix">
    <h4 class="hidden-sm">Кадры из игры</h4>
    <div class="b-images__grid j-images__grid">
            <?php $count=0;?>
            <?php foreach ($model->files as $value) : ?>
            <?php  $count++;?>
                <?php  if($count%2 == 0) : ?>
                    <div class="b-images__item w_180 j-images__item " >
                <?php else: ?>
                    <div class="b-images__item w_166 j-images__item " >
                 <?php endif;?>
                 <?php if ($count>4)
                  {
                    $count=-4;
                     echo '<p></p>';
                 }?>
                
                    <a href="http://backend.gametoponline/galleries<?php echo $value->src; ?>" class="j-popup" data-fancybox-group="gallery" >
                    <img src="http://backend.gametoponline/galleries<?php echo $value->src; ?>"  />
                    </a>
                 </div> 

         <?php endforeach;?>
          <?php/*
                echo EasyThumbnailImage::thumbnailImg(
                    "images/popup-5.jpg",
                    180,
                    180,
                    EasyThumbnailImage::THUMBNAIL_OUTBOUND,
                    ['alt' =>"hello"]
                );
                */
            ?>
        <div class="b-sizer j-sizer"></div>
        <div class="b-gutter j-gutter"></div>
    </div><!-- b-images__grid -->
</div><!-- b-mod -->