<?php
use yii\helpers\Html; 
?>

<div class="row page_content">
  <div class="col-md-8 col-md-offset-2  col-lg-8 col-lg-offset-2">
    <ul class="list-unstyled block-link-page">
       <li class="btn-yelow">
       	<a href="/site/download" class="link-catalog-pdf">
       		<?php echo Yii::t('frontend', 'widget-bgliks-1'); ?>
       	</a>
       	<span class="icon-btn-link-2"></span>
       </li>
       <li class="btn-red">
       	<a href="/site/samples" class="link-samples">
       	 <?php echo Yii::t('frontend', 'widget-bgliks-2'); ?>
       	</a>
       	<span class="icon-btn-link-1"></span>
       </li>
    </ul>
    <p class="text-center page-accent"><?php echo Yii::t('frontend', 'widget-bgliks-3'); ?></p>
    <p class="text-center page-phone"><?php echo $phone; ?></p>
  </div>
</div>