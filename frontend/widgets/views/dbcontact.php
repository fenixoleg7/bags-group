<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Pjax;
use yii\bootstrap\Alert;

?>
<div class="modal fade " id="order1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4><?php echo Yii::t('frontend','Order'); ?></h4>
        <p><?php echo Yii::t('frontend', 'Leave your order or message <br> and we will contact you') ?></p>
      </div>
      <div class="modal-body">
      <?php Pjax::begin(['id' => 'formPjaxPopUp']) ?>
      <?php if($otvet_pop != "")
               {
                   echo Alert::widget([
                    'options' => [
                        'class' =>$alert_value_pop ,
                    ],
                    'body' => $otvet_pop,
                    ]);
                }
        ?>
        <?php $form = ActiveForm::begin(['id' => 'contact-form-popup','options' => ['data-pjax' => true]]); ?>
            <?php echo $form->field($model, 'name')->textInput(['placeholder'=>Yii::t('frontend','Name, company name')])->label(false) ?>
            <?php echo $form->field($model, 'email')->textInput(['placeholder'=>Yii::t('frontend','Your e-mail')])->label(false) ?>
            <?php echo $form->field($model, 'phone')->textInput(['id'=>'bg_phone','placeholder'=>"+380(__)__-__-__"])->label(false) ?>
            <?php echo $form->field($model, 'body')->textArea(['rows' => 6, 'placeholder'=>Yii::t('frontend','Refine your order or Ask our manager')])->label(false)  ?>
           
              <p><?php echo Yii::t('frontend','You can specify in this field:') ?></p>
              <ol>
                <li><?php echo Yii::t('frontend','Type the desired product - such as bag') ?></li>
                <li><?php echo Yii::t('frontend','Commodity code (if you know it is on the site)') ?></li>
                <li><?php echo Yii::t('frontend','Why do you need a bag - such as flour') ?></li>
                <li><?php echo Yii::t('frontend','Under some weight - for example 50kg') ?></li>
                <li><?php echo Yii::t('frontend','What size - for example 105x55') ?></li>
                <li><?php echo Yii::t('frontend','The required amount - for example, 100 000 pieces / month') ?></li>
                <li><?php echo Yii::t('frontend','Whether the application of the logo is required - such as yes / no') ?></li>
                <li><?php echo Yii::t('frontend','Do you need full color printing on the bag - yes / no') ?></li>
              </ol>
             
              <p><?php echo Yii::t('frontend','This and any other information will speed up the formation of an individual quotation for you.') ?> 
            </p>
<!--            --><?php //echo $form->field($model, 'verifyCode')->widget(Captcha::className(), [
//                'template' => '<div class="row"><div class="col-lg-6">{image}</div><div class="col-lg-6">{input}</div></div>',
//            ]) ?>
            <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className()) ?>
            <div class="form-group text-center">
                <?php echo Html::submitButton(Yii::t('frontend', 'Submit'), ['class' => 'btn btn-danger bg-btn', 'name' => 'contact-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
        <?php Pjax::end() ?>
      </div>
      
    </div>
  </div>
</div>
