<?php
use yii\helpers\Html; 
?>

 <ul class="list-unstyled block-link">
   <li class="btn-yelow">
      <a href="/site/download" class="link-catalog-pdf">
     		<?php echo Yii::t('frontend', 'widget-bgliks-1'); ?>
      </a><span class="icon-btn-link-2"></span>
   </li>
   <li class="btn-red"><a href="/site/samples" class="link-samples">
   		<?php echo Yii::t('frontend', 'widget-bgliks-2'); ?>
   </a><span class="icon-btn-link-1"></span></li>
   <li class="bg-phone-block">
     <p class="text-center phone-text">
     	<?php echo Yii::t('frontend', 'widget-bgliks-3'); ?>
     </p>
     <p class="text-center block-link-phone">
     	<?php echo $phone//Yii::t('frontend', 'widget-bgliks-4'); ?>
     </p>
   </li>
 </ul> 