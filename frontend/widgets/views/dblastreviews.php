<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\Article
 */
use yii\helpers\Html;

?>

<div class="b-mod m-reviews">
    <h3 class="b-mod__title">Новые обзоры</h3>
    <ul class="b-mod__list">
        <?php foreach ($lastreviews as $review):?>
		
			<li class="b-mod__item">
				<?php echo Html::a(
                    $review->title,
                    ['/reviews/'.$review->slug],
                    ['class' => 'b-mod__item--link']
                )?>
	            
	            <span class="b-mod__item--meta"><?php echo Yii::$app->formatter->asDatetime($review->created_at,'dd.MM');?></span>

	        </li>

		<?php endforeach; ?>
    </ul>
</div><!-- b-mod -->