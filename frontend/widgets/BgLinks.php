<?php

namespace frontend\widgets;

use common\models\SiteInfo;
use yii\base\Widget;
use Yii;
use yii\helpers\Html;

/**
 * Class DbSiteInfo
 * Return a text block content stored in db
 * keys:
 *   logo-heder - show logo in heder
 *   logo-footer - show logo in footer
 *   footer-info - show contact information in footer
 *   heder-info - show contact information
 *   footer-social - show links for social
 *   footer-social-sm - show links for social(smail window)
 *   sidebar-info - show contact information in right sidebar
 *   tauscher - change currency
 */ 
class BgLinks extends Widget
{
    /**
     * @var string text block key
     */
    public $key;

    public $language = '';

    public $phone;


    /**
     * @return string
     */
    public function run()
    {
       $cacheKey = [
            SiteInfo::className(),
            $this->key
        ];
        $content = Yii::$app->cache->get($cacheKey);
        if (!$content) {
           $model=SiteInfo::findOne(1);
           if ($model) {
                if($this->key == 'bg-link-page')
                {
                    return $this->render('widget-bglinks-page',['phone'=>$model->phone_1]);
                }
                else{
                    return $this->render('widget-bglinks',['phone'=>$model->phone_1]);
                }
            }
        }
    }
}
