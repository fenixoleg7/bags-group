<?php

namespace frontend\widgets;

use common\models\SiteInfo;
use yii\base\Widget;
use Yii;
use yii\helpers\Html;

/**
 * Class DbSiteInfo
 * Return a text block content stored in db
 * keys:
 *   logo-heder - show logo in heder
 *   logo-footer - show logo in footer
 *   footer-info - show contact information in footer
 *   heder-info - show contact information
 *   footer-social - show links for social
 *   footer-social-sm - show links for social(smail window)
 *   sidebar-info - show contact information in right sidebar
 *   tauscher - change currency
 */ 
class DbSiteInfo extends Widget
{
    /**
     * @var string text block key
     */
    public $key;

    public $language = '';

    public $tauscher_value;

    /**
     * @return string
     */
    public function run()
    {
        $cacheKey = [
            SiteInfo::className(),
            $this->key
        ];
        $content = Yii::$app->cache->get($cacheKey);
        if (!$content) {
            $model =  SiteInfo::findOne(1);
            if ($model) {
                if($this->key == 'tauscher'){

                    //$this->language=Yii::$app->request->cookies->getValue('_locale');
                    $this->language=Yii::$app->language;

                    if($this->language == 'ru'){
                        $content =  (double)$this->tauscher_value;

                    }elseif ($this->language == 'en') {
                      
                      $content =  (double)$this->tauscher_value* (double)$model->currency_1;

                    }elseif($this->language == 'de') {
            
                       $content =  (double)$this->tauscher_value* (double)$model->currency_2;

                    }elseif($this->language == 'pl') {
            
                       $content =  (double)$this->tauscher_value* (double)$model->currency_2;
                    }

                }
                if($this->key == 'logo-footer'){
                   
                    $content =  Html::img($model->getFooterLogo(), ['alt' => 'BG Logo footer', 'class' => 'logo-footer img-responsive']);
                    Yii::$app->cache->set($cacheKey, $content, 60*60*24);
                }
                if($this->key == 'logo-heder'){
                   
                    $content =  Html::img($model->getHederLogo(), ['alt' => 'BG Logo heder', 'class' => 'bg-logo-slider']);
                    Yii::$app->cache->set($cacheKey, $content, 60*60*24);
                }
                if($this->key == 'logo-heder-sm'){
                   
                    $content =  Html::img($model->getHederLogo(), ['alt' => 'BG Logo']);
                    Yii::$app->cache->set($cacheKey, $content, 60*60*24);
                }
                if($this->key == 'footer-info'){
                    
                    $content = implode("\n", [
                        Html::tag('h3','Company: Bags Group'),
                        Html::beginTag('dl', ['class' => 'dl-horizontal']),
                        Html::beginTag('dt'),
                        Html::tag('span','',['class' => 'glyphicon glyphicon-home']),
                        Html::endTag('dt'),
                        Html::beginTag('dd'),
                        Html::tag('address', $model->adress),
                        Html::endTag('dd'),
                        Html::beginTag('dt'),
                        Html::tag('span','',['class' => 'glyphicon glyphicon-envelope']),
                        Html::endTag('dt'),
                        Html::beginTag('dd'),
                        Html::tag('address', $model->email),
                        Html::endTag('dd'),
                        Html::beginTag('dt'),
                        Html::tag('span','',['class' => 'glyphicon glyphicon-earphone']),
                        Html::endTag('dt'),
                        Html::beginTag('dd'),
                        Html::beginTag('address'),
                        'Tel.:'.$model->phone_1.'<br>',
                        'Fax.:'.$model->fax,
                        Html::endTag('address'),
                        Html::endTag('dd'),
                        Html::endTag('dl'),

                    ]) . "\n"; 
                    Yii::$app->cache->set($cacheKey, $content, 60*60*24);
                }
                if($this->key == 'heder-info'){
                    
                    $content = implode("\n", [
                       
                        Html::beginTag('dl', ['class' => 'dl-horizontal']),
                        Html::beginTag('dt'),
                        Html::tag('span','',['class' => 'glyphicon glyphicon-home']),
                        Html::endTag('dt'),
                        Html::beginTag('dd'),
                        Html::tag('address', $model->adress),
                        Html::endTag('dd'),
                        Html::beginTag('dt'),
                        Html::tag('span','',['class' => 'glyphicon glyphicon-envelope']),
                        Html::endTag('dt'),
                        Html::beginTag('dd'),
                        Html::tag('address', $model->email),
                        Html::endTag('dd'),
                        Html::beginTag('dt'),
                        Html::tag('span','',['class' => 'glyphicon glyphicon-earphone']),
                        Html::endTag('dt'),
                        Html::beginTag('dd'),
                        Html::beginTag('address'),
                        'Tel.:'.$model->phone_1.'<br>',
                        'Fax.:'.$model->fax,
                        Html::endTag('address'),
                        Html::endTag('dd'),
                        Html::endTag('dl'),

                    ]) . "\n"; 
                    Yii::$app->cache->set($cacheKey, $content, 60*60*24);
                }
                if($this->key == 'footer-social'){
                    $content = implode("\n", [
                        //Html::tag('h2',),
                        Html::beginTag('dl', ['class' => 'dl-horizontal']),
                        Html::tag('dt','Skype:'),
                        Html::beginTag('dd'),
                        Html::a('bagsgroup', ['#']),
                        Html::endTag('dd'),
                        Html::beginTag('dd'),
                        Html::a('LinkedIn', [$model->linkedIn_url]),
                        Html::endTag('dd'),
                        Html::endTag('dd'),
                        Html::beginTag('dd'),
                        Html::a('Facebook', [$model->Facebook_url]),
                        Html::endTag('dd'),
                        Html::beginTag('dd'),
                        Html::a('XING', [$model->xing_url]),
                        Html::endTag('dd'),
                        Html::endTag('dl'),

                    ]) . "\n"; 
                    Yii::$app->cache->set($cacheKey, $content, 60*60*24);
                }
                if($this->key == 'footer-social-sm'){
                    $content = implode("\n", [
                        Html::beginTag('dl', ['class' => 'dl-horizontal']),
                        Html::tag('dt','Skype:'),
                        Html::beginTag('dd'),
                        Html::a('bagsgroup', ['#']),
                        Html::endTag('dd'),
                        Html::beginTag('dd'),
                        Html::a('LinkedIn', [$model->linkedIn_url]),
                        Html::endTag('dd'),
                        Html::endTag('dd'),
                        Html::beginTag('dd'),
                        Html::a('Facebook', [$model->Facebook_url]),
                        Html::endTag('dd'),
                        Html::beginTag('dd'),
                        Html::a('XING', [$model->xing_url]),
                        Html::endTag('dd'),
                        Html::endTag('dl'),

                    ]) . "\n"; 
                    Yii::$app->cache->set($cacheKey, $content, 60*60*24);
                }
                if($this->key == 'sidebar-info'){
                    $content = implode("\n", [

                        Html::tag('h3', Yii::t('frontend','You have questions?')),
                        Html::tag('p', Yii::t('frontend','For information please contact us.')),
                        Html::beginTag('ul', ['class' => 'font_size_phone list-group BG-Blog_right_pan']),
                        Html::tag('li',$model->phone_1, ['class' => 'list-group-item'] ),
                        Html::tag('li',$model->phone_2, ['class' => 'list-group-item'] ),
                        Html::endTag('ul'),
                        Html::beginTag('div',['class' => 'mail_aside_right']),
                        Html::mailto($model->email, $model->email),
                        Html::endTag('div'),

                    ]) . "\n"; 
                    Yii::$app->cache->set($cacheKey, $content, 60*60*24);
                }

                    
            }
        }
        return $content;
    }
}
