<?php
/**
 * Eugine Terentev <eugine@terentev.net>
 */

namespace frontend\widgets;

use \common\models\Contact;
use yii\bootstrap\Widget;
use yii\helpers\Html;
use yii\base\InvalidConfigException;
use Yii;

/**
 * Class Banner
 * @package common\widgets
 */
class DbOffices extends Widget
{
    /**
     * @var
     */
    public $key="offices";

    public $items = [];

    public $language = '';

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
         
       
        parent::init();
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        //$this->language=Yii::$app->request->cookies->getValue('_locale');
        $this->language=Yii::$app->language;

        if (!$this->key) {
            throw new InvalidConfigException;
        }
        $cacheKey = [
            Contact::className(),
            $this->language
        ];

         $content = Yii::$app->cache->get($cacheKey);
        if (!$content) {
            $model = Contact::find()->all();
            if ($model) {
                 foreach ($model as $k => $item) {
                        $icon = $item->getImageUrl($item->thumbnail_path);
                    if($this->language == 'ru'){
                        $items[] = $this->renderItem($icon, $item->body_ru);
                    }elseif ($this->language == 'en') {
                        $items[] = $this->renderItem($icon, $item->body_en);
                    }elseif ($this->language == 'de') {
                        $items[] = $this->renderItem($icon, $item->body_de);
                    }else{
                        $items[] = $this->renderItem($icon, $item->body_pl);
                    }
                };
                $content=implode("\n", $items);  
                Yii::$app->cache->set($cacheKey, $content, 60*60*24);
            }
        }
         return $this->render('dboffices', ['content'=>$content]);
    }

    public function renderItem($icon, $body)
    {
        $item_lang = implode("\n", [

            Html::beginTag('div', ['class' => 'col-sm-6 col-md-6 col-lg-3 contact-block-city']),
            Html::beginTag('div', ['class' => 'contact-block']),
            Html::img($icon, ['alt' =>"offic"]),
            Html::tag('div',$body,['class' => 'caption']),
            Html::endTag('div'),
            Html::endTag('div'),

        ]) . "\n"; 

        return $item_lang;
    }

   
   
}
