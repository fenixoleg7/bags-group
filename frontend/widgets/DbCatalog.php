<?php
/**
 * Eugine Terentev <eugine@terentev.net>
 */

namespace frontend\widgets;

use yii\bootstrap\Widget;
use common\models\CatalogCategory;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use Yii;


/**
 * Class DbCatalog
 * Usage:
 * echo common\widgets\DbCatalog::widget([
 *      'key'=>'home-category-list',
 *      
 * ])
 * key :
 * 1 - 'home-category-list' 
 * 2 - 'sidebar-category-list'
 * 3 - 'sidebar-category-list-smwindow'
 * 4 - 'footer-category-list'
 *
 * @package common\widgets
 */
class DbCatalog extends Widget
{


    /**
     * @var string Key to find menu model
     */
    public $key;

    public $items = [];

    public $language = '';

    public $parent_id;

    public $category_id;
    public $label=false;



    public function run()
    {
         //$this->language=Yii::$app->request->cookies->getValue('_locale');
        $this->language = Yii::$app->language;
        $content="";
        $cacheKey = [
            CatalogCategory::className(),
            $this->key.$this->language
        ];
       // $content = Yii::$app->cache->get($cacheKey);
        //if ($content === false) {
            if($this->key == 'home-category-list')
            {
                $content = Yii::$app->cache->get($cacheKey);
                
                    if (!($model = CatalogCategory::find()->noParents()->all())) {
                        throw new InvalidConfigException;
                    }
                 if (!$content) {   
                    $items[] = Html::beginTag('div', ['class' => 'row']);
                    foreach ($model as $key => $item) {
                        if($key% 3 == 0){
                            $items[] = implode("\n", [
                                Html::endTag('div'),
                                Html::beginTag('div', ['class' => 'row']),
                            ]) . "\n"; 
                        }
                        if($this->language == 'ru'){
                            $icon = $item->getImageUrl();
                            $items[] = $this->renderItem($item->getImageUrl(), $item->title_ru, $item->slug);

                        }elseif ($this->language == 'en') {
                            $icon = $item->getImageUrl();
                           $items[] = $this->renderItem($item->getImageUrl(), $item->title_en, $item->slug);

                        }elseif($this->language == 'de') {
                            $icon = $item->getImageUrl();
                            $items[] = $this->renderItem($item->getImageUrl(), $item->title_de, $item->slug);

                        }elseif($this->language == 'pl') {
                            $icon = $item->getImageUrl();
                            $items[] = $this->renderItem($item->getImageUrl(), $item->title_pl, $item->slug);
                        }
                        
                    }
                    $items[] = Html::endTag('div');

                    $content=implode("\n", $items);  

                    Yii::$app->cache->set($cacheKey, $content, 60*60*24);
                }

                return $this->render('dbcatalog-home', ['content'=>$content, 'model' => $model] );

            }
            if($this->key == 'category-list')
            {
                if (!($model = CatalogCategory::find()->where(['parent_id' => $this->parent_id])->all())) {
                    return "products none";
                }
                

                $items[] = Html::beginTag('div', ['class' => 'row']);
                foreach ($model as $key => $item) {
                    if($key% 3 == 0){
                        $items[] = implode("\n", [
                            Html::endTag('div'),
                            Html::beginTag('div', ['class' => 'row']),
                        ]) . "\n"; 
                    }
                    if($this->language == 'ru'){
                        $icon = $item->getImageUrl();
                        $items[] = $this->renderItemCategory($item->getImageUrl(), $item->title_ru, $item->slug);

                    }elseif ($this->language == 'en') {
                        $icon = $item->getImageUrl();
                       $items[] = $this->renderItemCategory($item->getImageUrl(), $item->title_en, $item->slug);

                    }elseif($this->language == 'de') {
                        $icon = $item->getImageUrl();
                        $items[] = $this->renderItemCategory($item->getImageUrl(), $item->title_de, $item->slug);

                    }elseif($this->language == 'pl') {
                        $icon = $item->getImageUrl();
                        $items[] = $this->renderItemCategory($item->getImageUrl(), $item->title_pl, $item->slug);
                    }
                    
                }
                $items[] = Html::endTag('div');

                $content=implode("\n", $items);  

                Yii::$app->cache->set($cacheKey, $content, 60*60*24);

                return $content;

            }
             if($this->key == 'sidebar-category-list')
            {
                if (!($model_category = CatalogCategory::find()->noParents()->all())) {
                    throw new InvalidConfigException;
                }
                
                $this->label=false;
                $items[] = Html::beginTag('div', ['class' => 'widget-katalog-list']);
                $items[] = Html::tag('h2', Yii::t('frontend', 'Catalog products'));
                $items[] = Html::beginTag('ul',['class' => 'list-group']);
                foreach ($model_category as $key => $item) {
                    
                    if($this->language == 'ru'){

                        $items[] = $this->renderItemList($item->title_ru, $item->slug, $this->label);

                    }elseif ($this->language == 'en') {
                        
                       $items[] = $this->renderItemList($item->title_en, $item->slug, $this->label);

                    }elseif($this->language == 'de') {
                        
                        $items[] = $this->renderItemList($item->title_de, $item->slug, $this->label);

                    }elseif($this->language == 'pl') {
                        
                        $items[] = $this->renderItemList($item->title_pl, $item->slug, $this->label);
                    }
                    
                }
                $items[] = Html::endTag('ul');
                $items[] = Html::endTag('div');


                $content=implode("\n", $items);  

                Yii::$app->cache->set($cacheKey, $content, 60*60*24);

                return $content;

            }
             if($this->key == 'sidebar-subcategory-list')
            {
                if (!($model_subcategory = CatalogCategory::findOne($this->category_id))) {
                    throw new InvalidConfigException;
                }
                 $this->label=false;
                 $category_parent = $model_subcategory->parent;
                 $subcategory_list =$category_parent->catalogCategories;
          

                $items[] = Html::beginTag('div', ['class' => 'widget-subcategory-list']);
                if($this->language == 'ru'){

                    $items[] = Html::tag('h2', $category_parent->title_ru);

                }elseif ($this->language == 'en') {
                    
                   $items[] = Html::tag('h2', $category_parent->title_en);

                }elseif($this->language == 'de') {
                    
                    $items[] = Html::tag('h2', $category_parent->title_de);

                }elseif($this->language == 'pl') {
                    
                    $items[] = Html::tag('h2', $category_parent->title_pl);
                }
               
                $items[] = Html::beginTag('ul',['class' => 'list-group']);


                foreach ($subcategory_list as $key => $item) {
                    
                    if($this->language == 'ru'){

                        $items[] = $this->renderItemList($item->title_ru, $item->slug, $this->label);

                    }elseif ($this->language == 'en') {
                        
                       $items[] = $this->renderItemList($item->title_en, $item->slug, $this->label);

                    }elseif($this->language == 'de') {
                        
                        $items[] = $this->renderItemList($item->title_de, $item->slug, $this->label);

                    }elseif($this->language == 'pl') {
                        
                        $items[] = $this->renderItemList($item->title_pl, $item->slug, $this->label);
                    }
                    
                }
                $items[] = Html::endTag('ul');
                $items[] = Html::endTag('div');


                $content=implode("\n", $items);  

                Yii::$app->cache->set($cacheKey, $content, 60*60*24);

                return $content;

            }
             if($this->key == 'sidebar-category-list-smwindow')
            {
                if (!($model_category = CatalogCategory::find()->noParents()->all())) {
                    throw new InvalidConfigException;
                }
                 $this->label=false;
                $items[] = Html::beginTag('ul',['class' => 'list-group text-center']);
                foreach ($model_category as $key => $item) {
                    
                    if($this->language == 'ru'){

                        $items[] = $this->renderItemList($item->title_ru, $item->slug, $this->label);

                    }elseif ($this->language == 'en') {
                        
                       $items[] = $this->renderItemList($item->title_en, $item->slug, $this->label);

                    }elseif($this->language == 'de') {
                        
                        $items[] = $this->renderItemList($item->title_de, $item->slug, $this->label);

                    }elseif($this->language == 'pl') {
                        
                        $items[] = $this->renderItemList($item->title_pl, $item->slug, $this->label);
                    }
                    
                }
                $items[] = Html::endTag('ul');
               
                $content=implode("\n", $items);  

                Yii::$app->cache->set($cacheKey, $content, 60*60*24);

                return $content;

            }
             if($this->key == 'footer-category-list')
            {
                if (!($model_category = CatalogCategory::find()->noParents()->all())) {
                    throw new InvalidConfigException;
                }
                

                $this->label=true;
                
                $items[] = Html::beginTag('ul',['class' => 'list-unstyled']);
                foreach ($model_category as $key => $item) {
                    
                    if($this->language == 'ru'){

                        $items[] = $this->renderItemList($item->title_ru, $item->slug, $this->label);

                    }elseif ($this->language == 'en') {
                        
                       $items[] = $this->renderItemList($item->title_en, $item->slug, $this->label);

                    }elseif($this->language == 'de') {
                        
                        $items[] = $this->renderItemList($item->title_de, $item->slug, $this->label);

                    }elseif($this->language == 'pl') {
                        
                        $items[] = $this->renderItemList($item->title_pl, $item->slug, $this->label);
                    }
                    
                }
                $items[] = Html::endTag('ul');
             


                $content=implode("\n", $items);  

                Yii::$app->cache->set($cacheKey, $content, 60*60*24);

                return $content;

            }

           
       // }

        
    }

    public function renderItem($catalog_image, $title, $slug)
    {
        $item_lang = implode("\n", [

            Html::beginTag('div', ['class' => 'col-sm-6 col-md-4']),
            Html::beginTag('div', ['class' => 'product']),
            Html::beginTag('div', ['class' => 'product-prev']),
            Html::beginTag('div'),
            Html::a(Html::img($catalog_image, ['class' => 'img-responsive center-block','alt' => $title]),['/catalog/category/'.$slug]),
            Html::endTag('div'),
            Html::endTag('div'),
            Html::beginTag('div', ['class' => 'caption']),
            //Html::tag('h3', $title),
            //Html::a('',['/catalog/'.$slug],['class'=>'catalog-link']),
            Html::a($title,['/catalog/category/'.$slug],['class'=>'catalog-link']),
            Html::endTag('div'),
            Html::endTag('div'),
            Html::endTag('div'),

        ]) . "\n"; 

        return $item_lang;
    }

     public function renderItemCategory($catalog_image, $title, $slug)
    {
        $item_lang = implode("\n", [

            Html::beginTag('div', ['class' => 'col-sm-6 col-md-4']),
            Html::beginTag('div', ['class' => 'product']),
            Html::beginTag('div', ['class' => 'product-prev']),
            Html::beginTag('div'),
            Html::a( Html::img($catalog_image, ['class' => 'img-responsive center-block','alt' => $title]),['/catalog/products/'.$slug]),
            Html::endTag('div'),
            Html::endTag('div'),
            Html::beginTag('div', ['class' => 'caption']),
            //Html::tag('h3', ),
            //Html::a('',['/catalog/'.$slug],['class'=>'catalog-link']),
            Html::a('<span class="catalog-title-sp">'.$title,['/catalog/products/'.$slug],['class'=>'catalog-link']),
            Html::endTag('div'),
            Html::endTag('div'),
            Html::endTag('div'),

        ]) . "\n"; 

        return $item_lang;
    }



     public function renderItemList($title, $slug, $label)
    {
        if($label==true){
            $item_lang = implode("\n", [

            Html::beginTag('li', ['class' => '']),
            Html::a($title,['/catalog/category/'.$slug],['class'=>'']),
            Html::endTag('li'),

        ]) . "\n"; 

        }else{
            $item_lang = implode("\n", [

            Html::beginTag('li', ['class' => 'list-group-item']),
            Html::a($title,['/catalog/category/'.$slug],['class'=>'catalog-link']),
            Html::endTag('li'),

        ]) . "\n";  
        }
        

        return $item_lang;
    }
}
