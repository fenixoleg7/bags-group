<?php
/**
 * Eugine Terentev <eugine@terentev.net>
 */

namespace frontend\widgets;

use \common\models\WidgetWorkSchema;
use yii\bootstrap\Widget;
use yii\helpers\Html;
use yii\base\InvalidConfigException;
use Yii;

/**
 * Class Banner
 * @package common\widgets
 */
class DbWorkSchema extends Widget
{
    /**
     * @var
     */
    public $key="work-schema";

    public $items = [];

    public $language = '';

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
         
       
        parent::init();
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        //$this->language=Yii::$app->request->cookies->getValue('_locale');
        $this->language=Yii::$app->language;

        if (!$this->key) {
            throw new InvalidConfigException;
        }
        $cacheKey = [
            WidgetWorkSchema::className(),
            $this->key
        ];

         $content = Yii::$app->cache->get($cacheKey);
        if (!$content) {
            $model = WidgetWorkSchema::find()->orderBy(['step'=>SORT_ASC])->all();
            if ($model) {
                 foreach ($model as $k => $item) {

                    if($this->language == 'ru'){
                        $icon = $item->getImageUrl();
                        if($k!=0)
                            $items[] = $this->renderItem($icon, $item->title_ru, $item->text_ru, $item->step);
                        else
                            $items[] = $this->renderItemFirst($icon, $item->title_ru, $item->text_ru, $item->step);

                    }elseif ($this->language == 'en') {
                        $icon = $item->getImageUrl();
                        $items[] = $this->renderItem($icon, $item->title_en, $item->text_en, $item->step);

                    }else{
                        $icon = $item->getImageUrl();
                        $items[] = $this->renderItem($icon, $item->title_de, $item->text_de, $item->step);
                    }
                   
                };
                        

                $content=implode("\n", $items);  

                Yii::$app->cache->set($cacheKey, $content, 60*60*24);
            }
        }
         return $this->render('dbworkschema', ['content'=>$content] );
    }

    public function renderItem($icon, $title, $text, $step)
    {
        $item_lang = implode("\n", [

            Html::beginTag('div', ['class' => 'col-md-2  bg-shema-work-item']),
            Html::beginTag('div', ['class' => 'icon-shema-work']),
            Html::img($icon, ['alt' => $title]),
            Html::tag('span',$step,['class' => 'circle']),
            Html::endTag('div'),
            Html::tag('h3', $title),
            Html::tag('p', $text),
            Html::endTag('div'),

        ]) . "\n"; 

        return $item_lang;
    }

    public function renderItemFirst($icon, $title, $text, $step)
    {
        $item_lang = implode("\n", [

            Html::beginTag('div', ['class' => 'col-md-2 col-sm-12  bg-shema-work-item']),
            Html::beginTag('div', ['class' => 'icon-shema-work']),
            Html::img($icon, ['alt' => $title]),
            Html::tag('span',$step,['class' => 'circle']),
            Html::endTag('div'),
            Html::tag('h3', $title),
            Html::tag('p', $text),
            Html::beginTag('div', ['class' => 'text-center']),
            Html::a('Заказать', ['#'], ['class' => 'catalog-btn visible-xs visible-sm', ' data-toggle'=>'modal', 'data-target'=>'#order1']),
            Html::endTag('div'),
            Html::endTag('div'),

        ]) . "\n"; 

        return $item_lang;
    }
   
}
