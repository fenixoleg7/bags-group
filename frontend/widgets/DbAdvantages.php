<?php

namespace frontend\widgets;

use \common\models\WidgetAdvantages;
use yii\base\Widget;
use yii\helpers\Html; 
use Yii;

/**
 * Class DbText
 * Return a text block content stored in db
 * @package common\widgets\text
 */
class DbAdvantages extends Widget
{
    /**
     * @var string text block key
     */

    public $key = 'w_advantages';

    public $items = array();

    public $percent_items = array();

    public $language = '';

    /**
     * @return string
     */

    public function run()
    {
        //$cacheKey = [
        //    WidgetAdvantages::className(),
        //    $this->key
        //];

        //$this->language=Yii::$app->request->cookies->getValue('_locale');
        $this->language=Yii::$app->language;
        $content="";
        //$content = Yii::$app->cache->get($cacheKey);
        //if (!$content) {
            $model = WidgetAdvantages::find()->all();
            if ($model) {
                 foreach ($model as $k => $item) {

                    if($this->language == 'ru'){

                        $items[] = $this->renderItem($item->name_ru, $item->title_ru, $item->text_ru, $item->id);

                    }elseif ($this->language == 'en') {

                        $items[] = $this->renderItem($item->name_en, $item->title_en, $item->text_en, $item->id);

                    }else{

                        $items[] = $this->renderItem($item->name_de, $item->title_de, $item->text_de, $item->id);
                    }
                    
                    $this->percent_items[$item->id] = "$('#circle".$item->id."').circleProgress({startAngle: -Math.PI / 2 ,value: 0.35,size: 100, fill: {color: '#c8302f'}}).on('circle-animation-progress', function(event, progress) { $(this).find('strong').html('+' + parseInt(".$item->percent." * progress) + '<i>%</i>'); });";
                };
                        
                
                $content=implode("\n", $items);  
                //Yii::$app->cache->set($cacheKey, $content, 60*60*24);
            }
        //}
        return $this->render('widget-advantages',['content'=>$content, 'percents'=>implode("\n",$this->percent_items) ] );
    }

    public function renderItem($name, $title, $text, $id )
    {
        $item_lang = implode("\n", [

            Html::beginTag('div', ['class' => 'col-md-3']),
            Html::beginTag('div', ['class' => 'media']),
            Html::beginTag('div', ['class' => 'circle pull-left','id'=>'circle'.$id]),
            Html::tag('strong','+0<i>%</i>'),
            Html::endTag('div'),
            Html::beginTag('div', ['class' => 'media-body']),
            Html::tag('h3', $name, ['class' => 'media-heading']),
            Html::tag('h4', $title),
            Html::tag('p', $text),
            Html::endTag('div'),
            Html::tag('div','', ['class' => 'clearfix']),
            Html::endTag('div'),
            Html::endTag('div'),

        ]) . "\n"; 

        return $item_lang;
    }
}
