<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\commands\AddToTimelineCommand;


/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $body;
    public $reCaptcha;
    public $skype;


    public function scenarios()
    {
        return [
            'popup-contact' => ['name', 'email', 'phone','body','reCaptcha'],
            'contact-page' => ['name', 'email', 'phone','skype','body','reCaptcha'],
        ];
    }
    

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'phone', 'body', 'reCaptcha'], 'required','on' => 'popup-contact'],
            [['name', 'email', 'phone', 'body', 'reCaptcha '], 'required','on' => 'contact-page'],
             [['skype'], 'string', 'max' => 50],
            //[['name', 'email', 'body'], 'required','on' => 'footer-contact'],
            // We need to sanitize them
            [['name', 'phone', 'body'], 'filter', 'filter' => 'strip_tags'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
            // verifyCode needs to be entered correctly
            ['reCaptcha', \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LdS8H4UAAAAAFC_t3Rqv1-e5E85lFhx4JOq872I']

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('frontend', 'Name'),
            'email' => Yii::t('frontend', 'Email'),
            'phone' => Yii::t('frontend', 'Phone'),
            'body' => Yii::t('frontend', 'Body'),
            'skype' => 'Skype',
            //'verifyCode' => Yii::t('frontend', 'Verification Code'),
            'reCaptcha' => '',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            
                /*Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom(Yii::$app->params['robotEmail'])
                ->setReplyTo([$this->email => $this->name])
                ->setSubject("Contact")
                ->setTextBody($this->body)
                ->send();
                */
            //Добавляем события отправки заказа в админку
            Yii::$app->commandBus->handle(new AddToTimelineCommand([
                'category' => 'order-pop',
                'event' => 'order-pop',
                'data' => [
                    'name' => $this->name,
                    'email' =>$this->email,
                    'phone' => $this->phone,
                    'message' => $this->body,
                ]
            ]));
            
            // Сообщение
            $message = "Пользователь:".$this->name."\r\n".
                       "E-mail:".$this->email."\r\n".
                       "Сообщение:".$this->body."\r\n".
                        "Контактная форма";
                                
            // На случай если какая-то строка письма длиннее 70 символов мы используем wordwrap()
            $message = wordwrap($message, 70, "\r\n");
            
            // Отправляем
           return  mail($email, 'Сообщение с сайта', $message);
                
        } else {
            return false;
        }
    }
    public function order($email)
    {
        if ($this->validate()) {
            
            //Добавляем события отправки заказа в админку
            Yii::$app->commandBus->handle(new AddToTimelineCommand([
                'category' => 'order-pop',
                'event' => 'order-pop',
                'data' => [
                    'name' => $this->name,
                    'email' =>$this->email,
                    'phone' => $this->phone,
                    'message' => $this->body,
                ]
            ]));
            
            // Сообщение
            $message = "Пользователь:".$this->name."\r\n".
                       "E-mail:".$this->email."\r\n".
                       "Телефон:".$this->phone."\r\n".
                       "Сообщение:".$this->body."\r\n".
                       "Order";
                                
            // На случай если какая-то строка письма длиннее 70 символов мы используем wordwrap()
            $message = wordwrap($message, 70, "\r\n");
            
            // Отправляем
           return  mail($email, 'Заказ с сайта', $message);
                
        } else {
            return false;
        }
    }
}
