<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\commands\AddToTimelineCommand;


/**
 * ContactForm is the model behind the contact form.
 */
class SamplesForm extends Model
{
    public $category;
    public $name;
    public $email;
    public $phone;
    public $address;
    public $reCaptcha;



    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           
            // We need to sanitize them
            [['category','name','email', 'phone', 'address', 'reCaptcha'],'required'],

            [['category','name','email', 'phone', 'address'],'string', 'length' => [4, 50]],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['reCaptcha', \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LdS8H4UAAAAAFC_t3Rqv1-e5E85lFhx4JOq872I']

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [

            'name' => Yii::t('frontend', 'Name'),
            'email' => Yii::t('frontend', 'Email'),
            'phone' => Yii::t('frontend', 'Phone'),
            'address' => Yii::t('frontend', 'Address'),
            'reCaptcha' => '',
            //'verifyCode' => Yii::t('frontend', 'Verification Code')
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            //Добавляем события отправки заказа в админку
            Yii::$app->commandBus->handle(new AddToTimelineCommand([
                'category' => 'samples',
                'event' => 'samples',
                'data' => [
                    'name' => $this->name,
                    'email' =>$this->email,
                    'phone' => $this->phone,
                    'address' => $this->address,
                    'category' => $this->category,
                ]
            ]));
            // Сообщение
            $message = "Пользователь:".$this->name."\r\n".
                       "E-mail:".$this->email."\r\n".
                       "Телефон:".$this->phone."\r\n".
                       "Адрес:".$this->address."\r\n".
                       "Образец:".$this->category;
                                
            // На случай если какая-то строка письма длиннее 70 символов мы используем wordwrap()
            $message = wordwrap($message, 70, "\r\n");
            
            // Отправляем
           return  mail($email, 'Сообщение с сайта', $message);
                
        } else {
            return false;
        }
    }
   
}
