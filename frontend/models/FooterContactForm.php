<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\commands\AddToTimelineCommand;


/**
 * ContactForm is the model behind the contact form.
 */
class FooterContactForm extends Model
{

    public $email;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['email'], 'required'],

            ['email', 'email'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('frontend', 'Email'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            
                /*Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom(Yii::$app->params['robotEmail'])
                ->setReplyTo([$this->email => $this->name])
                ->setSubject("Contact")
                ->setTextBody($this->body)
                ->send();
                */
            // Сообщение
            $message = "Рассылка:\r\n".
                       "E-mail:".$this->email."\r\n".
                                
            // На случай если какая-то строка письма длиннее 70 символов мы используем wordwrap()
            $message = wordwrap($message, 70, "\r\n");
            
            // Отправляем
           return  mail($email, 'Сообщение с сайта', $message);
                
        } else {
            return false;
        }
    }
   
}
