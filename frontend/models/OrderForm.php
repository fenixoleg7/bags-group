<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class OrderForm extends Model
{
    public $product_id;
    public $product_name;
    public $product_quantity;
    public $product_price;
    public $price_1;
    public $price_2;
    public $price_3;
    public $price_4;
    public $price_5;
    public $product_kod;
    public $name;
    public $email;
    public $phone;
    public $subject;
    public $body;
    public $cost;
    public $currency;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['product_name', 'product_quantity','name', 'email','phone', 'body'], 'required'],
            [['price_1', 'price_2', 'price_3', 'price_4', 'price_5'], 'number'],
            [['product_id', 'product_name','cost'], 'string', 'max' => 150],
            [['currency'], 'string', 'max'=>10],
            [['product_kod', 'product_quantity'], 'integer'],
            [['product_price'], 'number'],
            // We need to sanitize them
            [['name', 'subject', 'body'], 'filter', 'filter' => 'strip_tags'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'product_name' => Yii::t('frontend', 'Name of product'),
            'product_quantity' => Yii::t('frontend', 'Quantity'),
            'product_price' => Yii::t('frontend', 'Priсe'),
            'name' => Yii::t('frontend', 'Enter your name'),
            'email' => Yii::t('frontend', 'Enter your e-mai'),
            'phone' => Yii::t('frontend', 'Enter your phone'),
            'body' => Yii::t('frontend', 'Comments (if any)'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function order($email)
    {
        if ($this->validate()) {
            /*return Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom(Yii::$app->params['robotEmail'])
                ->setReplyTo([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();*/
                
            // Сообщение
            $message = "Пользователь:".$this->name."\r\n".
                       "E-mail:".$this->email."\r\n".
                       "Товар:".$this->product_name."\r\n".
                       "Количество:".$this->product_quantity."\r\n".
                       "Сумма заказа:".$this->product_price."\r\n".
                       "Сообщение:".$this->body;
                                
            // На случай если какая-то строка письма длиннее 70 символов мы используем wordwrap()
            $message = wordwrap($message, 70, "\r\n");
            
            // Отправляем
           return  mail($email, 'Заказ товара', $message);
                
        } else {
            return false;
        }
    }
    
}
