<?php

namespace frontend\controllers;

use common\models\Article;
use common\models\ArticleAttachment;
use frontend\models\search\ArticleSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use \common\models\Seo;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class ArticleController extends Controller
{
    
    /**
     * @return string
     */
    public function actionIndex()
    {
        /* получаем сеоданные для заголовка*/
        $seomodel = Seo::findOne(1);
        $seodate_lang = $seomodel->getDateLang(Yii::$app->language);
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => ['created_at' => SORT_DESC]
        ];
        return $this->render('index', [
            'dataProvider'=>$dataProvider, 
            'seomodel'=>$seomodel,
            'seodate_lang' => $seodate_lang
            ]);
    }

    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($slug)
    {

        $model = Article::find()->published()->andWhere(['slug'=>$slug])->one();

        

        if (!$model) {
            throw new NotFoundHttpException;
        }

        $model_lang = $model->getDateLang(Yii::$app->language);

        $viewFile = $model->view ?: 'view';
        return $this->render($viewFile, ['model'=>$model, 'model_lang' => $model_lang ]);
    }

    
}
