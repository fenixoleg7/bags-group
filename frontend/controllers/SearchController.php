<?php

namespace frontend\controllers;

use frontend\models\search\ArticleSearch;
use frontend\models\search\CatalogSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use frontend\models\SearchForm;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class SearchController extends Controller
{
    const ITEMS_PER_PAGE = 10;

    public function actionIndex($query_user)
    {
    $query_user=strip_tags($query_user);    
    $lang = Yii::$app->language;
    if($query_user!=''){
        if($lang == 'ru'){
            $query1 = (new Query())
            ->select(["CONCAT('/article/', slug ) AS url",'title_ru', 'body_ru'])
            ->from('article')
            ->where(['like', 'title_ru', $query_user])
            ->limit(10);
    
            $query2 = (new Query())
            ->select(["CONCAT('/catalog/product/', id ) AS url",'title_ru', 'body_ru'])
            ->from('products')
            ->where(['like', 'title_ru', $query_user])
            ->limit(10);
        }elseif($lang == 'en'){
            $query1 = (new Query())
            ->select(["CONCAT('/article/', slug ) AS url",'title_en', 'body_en'])
            ->from('article')
            ->where(['like', 'title_en', $query_user])
            ->limit(10);
    
            $query2 = (new Query())
            ->select(["CONCAT('/catalog/product/', id ) AS url",'title_ru', 'body_en'])
            ->from('products')
            ->where(['like', 'title_en', $query_user])
            ->limit(10);
        }else{
            $query1 = (new Query())
            ->select(["CONCAT('/article/', slug ) AS url",'title_de', 'body_de'])
            ->from('article')
            ->where(['like', 'title_de', $query_user])
            ->limit(10);
    
            $query2 = (new Query())
            ->select(["CONCAT('/catalog/product/', id ) AS url",'title_de', 'body_de'])
            ->from('products')
            ->where(['like', 'title_de', $query_user])
            ->limit(10);
        }
        

        $query=$query1->union($query2);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            
        ]);
     
          return  $this->render('index', array(
                'provider'=>$provider,
                'query_user'=>$query_user
            ));
        }else{
           return $this->render('no_result'); 
        }
     
    }

     public function actionSearch()
    {
        
        
        $searchModel = new ArticleSearch();

        if ($searchModel->load(Yii::$app->request->get())) {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->sort = [
                'defaultOrder' => ['created_at' => SORT_DESC]
                ];
            Yii::$app->view->params['pageClass'][] = 'search';
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
            ]);
        }
    }

    
    
}
