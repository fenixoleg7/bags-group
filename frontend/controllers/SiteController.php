<?php
namespace frontend\controllers;

use Yii;
use frontend\models\ContactForm;
use frontend\models\SamplesForm;
use yii\web\Controller;
use common\models\Widgets;
use common\models\CatalogCategory;
use common\models\SiteinfoAttachment;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $loyaut = '/layouts/main_home';
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'foreColor'=>0xD9534F,
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale'=>[
                'class'=>'common\actions\SetLocaleAction',
                'locales'=>array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {
        $this->view->title = 'custom-title';
        $this->layout = 'main_home';
        /* получаем список виджетов в правой колонке*/
        $widgets =  Widgets::find()
        ->published()
        ->Where(['page'=> 1])
        ->all();

        return $this->render('index',['widgets' => $widgets]);
    }

    public function actionContact()
    {
      //var_dump(Yii::$app->request->post());
        $model = new ContactForm();
        $model->scenario = 'contact-page';

        if ($model->load(Yii::$app->request->post())) {
            if ($model->contact("bagsgroupde@gmail.com")) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible.'),
                    'options'=>['class'=>'alert-success']
                ]);
                return $this->refresh();
            } else {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>\Yii::t('frontend', 'There was an error sending email.'),
                    'options'=>['class'=>'alert-danger']
                ]);
            }
            // Сообщение
            //$message = "Line 1\r\nLine 2\r\nLine 3";
            
            // На случай если какая-то строка письма длиннее 70 символов мы используем wordwrap()
            //$message = wordwrap($message, 70, "\r\n");
            
            // Отправляем
            //mail('yaskool@yandex.ua', 'My Subject', $message);
        }

        return $this->render('contact', [
            'model_contact' => $model
        ]);
    }

    public function actionCfooter()
    {
      //var_dump(Yii::$app->request->post());
        $model = new ContactForm();
        $model->scenario = 'footer-contact';

        if ($model->load(Yii::$app->request->post())) {
            if ($model->contact("bagsgroupde@gmail.com")) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible.'),
                    'options'=>['class'=>'alert-success']
                ]);
                return $this->refresh();
            } else {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>\Yii::t('frontend', 'There was an error sending email.'),
                    'options'=>['class'=>'alert-danger']
                ]);
            }
            // Сообщение
            //$message = "Line 1\r\nLine 2\r\nLine 3";
            
            // На случай если какая-то строка письма длиннее 70 символов мы используем wordwrap()
            //$message = wordwrap($message, 70, "\r\n");
            
            // Отправляем
            //mail('yaskool@yandex.ua', 'My Subject', $message);
        }

        //return $this->render('contact', [
        //    'model_contact' => $model
       // ]);
    }

    public function actionSamples()
    {
      $category = CatalogCategory::find()->active()->all();

       $model = new SamplesForm();
      
       if ($model->load(Yii::$app->request->post())) {
            if ($model->contact("bagsgroupde@gmail.com")) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible.'),
                    'options'=>['class'=>'alert-success']
                ]);
                return $this->refresh();
            } else {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>\Yii::t('frontend', 'There was an error sending email.'),
                    'options'=>['class'=>'alert-danger']
                ]);
            }
            
        }

   /*     return $this->render('contact', [
            'model' => $model
        ]);*/
        return $this->render('samples', [
            'model' => $model, 'categories'=>$category
        ]);
    }

    public function actionDownload() 
    {
        $language=Yii::$app->language;
         if($language == 'ru'){
                $name="BG_Catalog_RU.pdf";
        }elseif($language == 'en'){
                $name="BG_Catalog_EN.pdf";
        }else{
                $name="BG_Catalog_DE.pdf";
        }
        $model = SiteinfoAttachment::find()
         ->where(['name' =>$name])
         ->one();
        if (!$model) {
            throw new NotFoundHttpException;
        }

        return Yii::$app->response->sendStreamAsFile(
            Yii::$app->fileStorage->getFilesystem()->readStream($model->path),
            $model->name
        );
    }
    

}
