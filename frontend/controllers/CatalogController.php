<?php

namespace frontend\controllers;

use Yii;
use common\models\Seo;
use common\models\CatalogCategory;
use common\models\Products;
use common\models\Orders;
use frontend\models\search\CatalogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use frontend\models\OrderForm;
use frontend\widgets\DbSiteInfo;


/**
 * CatalogController implements the CRUD actions for CatalogCategory model.
 */
class CatalogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CatalogCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        /* получаем сеоданные для заголовка*/
        $seomodel = Seo::findOne(2);
        $seodate_lang = $seomodel->getDateLang(Yii::$app->language);
        

        $searchModel = new CatalogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider'=>$dataProvider, 
            'seomodel'=>$seomodel,
            'seodate_lang' => $seodate_lang

        ]);
    }

    

    public function actionCategory($slug)
    {
        
        $model = CatalogCategory::find()->andWhere(['slug'=>$slug])->one();

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
        }
        
        $parent_id = $model->id;

        $model_categories = $model->getCatalogCategories();

        $model_lang = $model->getDateLang(Yii::$app->language);

        
        
        return $this->render('category', ['model'=>$model_lang, 'parent_id'=>$parent_id]);
    }

    public function actionProducts($slug)
    {

        $model = CatalogCategory::find()->andWhere(['slug'=>$slug])->one();

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
        }

        $model_lang = $model->getDateLang(Yii::$app->language);

        $dataProvider = new ActiveDataProvider([
            'query' => Products::find()->where(['category_id' =>$model->id]),
            'pagination' => [
                'pageSize' => 6,
            ],
        ]);

        return $this->render('products', ['model_lang' => $model_lang,'dataProvider'=> $dataProvider]);
    }

     public function actionProduct($id)
    {
        $product= Products::findOne($id);

         if (!$product) {
            throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
        }

        $product_lang = $product->getDateLang(Yii::$app->language);

        $model = new OrderForm();
        $model->product_id = $id;
        $model->product_name = $product_lang['title'];
        $model->product_kod = $product->kod;       
        $model->price_1 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_1 ]);    
        $model->price_2 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_2 ]);
        $model->price_3 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_3 ]);
        $model->price_4 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_4 ]);
        $model->price_5 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_5 ]);
        $model->currency = $product_lang['currency'] ;

        $otvet_order="";
        $alert_order='';
        
        if ($model->load(Yii::$app->request->post())) {  
                if ($model->order(Yii::$app->params['adminEmail'])) {
                    $order = new Orders();
                    $order->product_id = $model->product_id;
                    $order->product_name = $model->product_name;
                    $order->client_name= $model->name;
                    $order->product_kod = $model->product_kod;
                    $order->product_prise = $model->product_price;
                    $order->total_cost = $model->cost;
                    $order->currency = $model->currency;
                    $order->product_quantity = $model->product_quantity;
                    $order->client_phone = $model->phone;
                    $order->client_email = $model->email;
                    $order->client_message = $model->body;
                    if($order->save() && $order->validate()){
                        $order->addToTimeline();
                        $otvet_order = Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible.');
                        $alert_order = 'alert-success';
                        $model = new OrderForm();
                        $model->product_id = $id;
                        $model->product_name = $product_lang['title'];
                        $model->product_kod = $product->kod;       
                        $model->price_1 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_1 ]);    
                        $model->price_2 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_2 ]);
                        $model->price_3 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_3 ]);
                        $model->price_4 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_4 ]);
                        $model->price_5 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_5 ]);
                        $model->currency = $product_lang['currency'] ;

                    } else {
                        $otvet_order = Yii::t('frontend', 'There was an error sending email.');
                        $alert_order = 'alert-danger';
                        $model = new OrderForm();
                        $model->product_id = $id;
                        $model->product_name = $product_lang['title'];
                        $model->product_kod = $product->kod;       
                        $model->price_1 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_1 ]);    
                        $model->price_2 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_2 ]);
                        $model->price_3 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_3 ]);
                        $model->price_4 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_4 ]);
                        $model->price_5 = DbSiteInfo::widget(['key' => 'tauscher','tauscher_value' => $product->price_5 ]);
                        $model->currency = $product_lang['currency'] ;
                    }
            }
        }

        return $this->render('product', ['product_model'=>$product,'product_lang' => $product_lang,'model' => $model,'otvet_order'=>$otvet_order,'alert_order'=>$alert_order ]);
    }

    
    /**
     * Finds the CatalogCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CatalogCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CatalogCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
