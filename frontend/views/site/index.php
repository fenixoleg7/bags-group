<?php

use frontend\widgets\HomeSlider;
use frontend\widgets\DbCatalog;
use frontend\widgets\DbLastNews;
use frontend\widgets\DbBanner;
use frontend\widgets\BgLinks;
use yii\helpers\Html;
use frontend\widgets\DbSiteInfo;

\frontend\assets\FrontendBagsGroupAsset::register($this);
/* @var $this yii\web\View */
$this->title ="BagsGroup";
?>


      <?php echo HomeSlider::widget(); ?>
 

     <div class="container visible-xs xs-heder-logo">
      
        <a class="bg-logo-slider-a-xs" href="/" >
         
          <?php echo DbSiteInfo::widget([
              'key' => 'logo-heder-sm'
            ]) ?>

        </a>    
    
    </div>
<div class="container-fluid content-top">
  
  <div class="row">
    <div class="col-md-9 col-lg-9 content-main catalog-list">
      <div class="row bg-info-block">
      <div class="bg-info-block-h2 col-md-12">
          <h2 class="text-center"><span class="text-warning">
            Bags Group</span><?php echo Yii::t('frontend', 'bg-home-text-1'); ?> 
          </h2>
      </div>
      <div class="col-sm-12 visible-xs visible-sm category-list-sm">
        <?php echo \frontend\widgets\DbCatalog::widget([
                        'key' => 'footer-category-list'
                      ]) ?>
      </div>

      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div id="info-1"   class="top-info ">
          <h3 class="bg-warning bg-info-block-h3">
            <?php echo Yii::t('frontend', 'bg-home-text-2'); ?> 
          </h3>
          <div class="info-prev">
              <?php echo Html::img('/images/info-img1-1.png',['alt'=>'bg-info-1','class'=>'img-responsive']) ?> 
          </div>
          <div class="bg-info-text">
            <h3><?php echo Yii::t('frontend', 'bg-home-text-3'); ?></h3>
            <p><?php echo Yii::t('frontend', 'bg-home-text-4'); ?></p>
          </div>
          <?php echo Html::a(
            Yii::t('frontend', 'More').
            Html::tag('span', '', ['class' => 'glyphicon glyphicon-play']),
            ['/page/view', 'slug'=>'about'],
            ['class' => 'info-link pull-right']) ?>
           </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div id="info-2" class="top-info">
          <h3 class="bg-warning bg-info-block-h3"><?php echo Yii::t('frontend', 'bg-home-text-5'); ?></h3>
          <div class="info-prev">
              <iframe width="100%" height="auto" src="https://www.youtube.com/embed/98F0wU1A3a4" frameborder="0"  allowfullscreen></iframe>
          </div>
          <div  class="bg-info-text">
            <h3><?php echo Yii::t('frontend', 'bg-home-text-6'); ?></h3>
            <p><?php echo Yii::t('frontend', 'bg-home-text-7'); ?></p>
          </div>
           <a class="info-link pull-right" href="#"><?php echo Yii::t('frontend', 'More');?><span class="glyphicon glyphicon-play "></span></a>
          </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div id="info-3"  class="top-info">
          <h3 class="bg-warning bg-info-block-h3"><?php echo Yii::t('frontend', 'bg-home-text-8'); ?></h3>
          <div class="info-prev">
             <?php echo Html::img('/images/info-img2-1.png',['alt'=>'bg-info-2','class'=>'img-responsive']) ?> 
          </div>
          <div  class="bg-info-text">
            <h3><?php echo Yii::t('frontend', 'bg-home-text-9'); ?></h3>
            <p><?php echo Yii::t('frontend', 'bg-home-text-10'); ?></p>
          </div>
          <?php echo Html::a(
            Yii::t('frontend', 'More').
            Html::tag('span', '', ['class' => 'glyphicon glyphicon-play']),
            ['/page/view', 'slug'=>'history'],
            ['class' => 'info-link pull-right']) ?>
           
        </div>
      </div>
  </div> 
      <?php echo DbCatalog::widget(['key' => 'home-category-list']); ?>
      
    </div>
    <div class="col-md-3 col-lg-3 right-sidebar hidden-xs hidden-sm">

      <!-- Widget Banner -->

        <?php echo DbBanner::widget(['page' => '1', 'var'=>'1']); ?>

      <!--Widget BgLinks -->

         <?php echo BgLinks::widget(); ?>
        
      <!---Widget right column -->
        <?php foreach ($widgets as $value)
         {
           if ($value['name'] == 'Последние новости')
                echo \frontend\widgets\DbLastNews::widget(['count'=> $value->quantity]);

         } ?>

    </div> 
  </div>
</div>

<?php  //echo \frontend\widgets\DbAdvantages::widget(); ?>
<section class="parallax-window" data-parallax="scroll" data-image-src="/images/paralax-img-min.png">
  <div id="bg-advantages" class="container-fluid bg-advantages">
    <div class="row">
       <div class="col-md-9 text-center">
        <h2 ><?php echo Yii::t('frontend','bg-home-advantages-text-1'); ?></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-11 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-3 col-lg-offset-0">
        <ul class="media-list">
          <li class="media">
            <div class="pull-left" href="#">
            <span class="bg-advantages-icon icon-chemical-formula"></span>
            </div>
            <div class="media-body">
              <p><?php echo Yii::t('frontend','bg-home-advantages-text-2'); ?></p>
            </div>
          </li>
          <li class="media">
            <div class="pull-left" href="#">
            <span class="bg-advantages-icon icon-law-book"></span>
            </div>
            <div class="media-body">
              <p><?php echo Yii::t('frontend','bg-home-advantages-text-3'); ?></p>
            </div>
          </li>
          <li class="media">
            <div class="pull-left" href="#">
             <span class="bg-advantages-icon icon-clock-speed"></span>
            </div>
            <div class="media-body">
              <p><?php echo Yii::t('frontend','bg-home-advantages-text-4'); ?> </p>
            </div>
          </li>
          
        </ul>
      </div>

      <div class="col-xs-11 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-3 col-lg-offset-0">
        <ul class="media-list">
            <li class="media">
              <div class="pull-left" href="#">
               <span class="bg-advantages-icon icon-2-1"></span>
              </div>
              <div class="media-body">
                <p><?php echo Yii::t('frontend','bg-home-advantages-text-5'); ?></p>
              </div>
            </li>
            <li class="media">
              <div class="pull-left" href="#">
               <span class="bg-advantages-icon icon-34"></span>
              </div>
              <div class="media-body">
                <p><?php echo Yii::t('frontend','bg-home-advantages-text-6'); ?></p>
              </div>
            </li>
            <li class="media visible-lg">
              <div class="pull-left" href="#">
               <span class="bg-advantages-icon icon-44"></span>
              </div>
              <div class="media-body">
                <p><?php echo Yii::t('frontend','bg-home-advantages-text-7'); ?></p>
              </div>
            </li>
        </ul>
      </div>

      <div class="col-xs-11 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-3 col-lg-offset-0">
        <ul class="media-list visible-lg">
            <li class="media">
              <div class="pull-left" href="#">
              <span class="bg-advantages-icon icon-32"></span>
              </div>
              <div class="media-body">
                <p><?php echo Yii::t('frontend','bg-home-advantages-text-8'); ?>
                </p>
              </div>
            </li>
            <li class="media visible-lg">
              <div class="pull-left" href="#">
              <span class="bg-advantages-icon  icon-renewable-energy"></span>
              </div>
              <div class="media-body">
                <p><?php echo Yii::t('frontend','bg-home-advantages-text-9'); ?></p>
              </div>
            </li>
            <li class="media visible-lg">
              <div class="pull-left" href="#">
               <span class="bg-advantages-icon icon-recycle"></span>
              </div>
              <div class="media-body">
                <p>
                  <?php echo Yii::t('frontend','bg-home-advantages-text-10'); ?></p>
              </div>
            </li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div  class="col-md-9 text-center">
        <a href="/page/benefits" class="catalog-btn"><?php echo Yii::t('frontend','all advantages') ?></a>
      </div>
    </div>
  </div>    
</section>


<section>
   <div class="container-fluid bg-shema-work">
    <div class="row">
      <div class="col-md-12 col-lg-12 text-center">
        <div class="bg-title-block">
          <h2 class="bg-title-block-h2"><?php echo Yii::t('frontend','bg-shema-work-text-1'); ?></h2>
        </div>
      </div>
      </div>
    <div class="row">
        <div class="col-md-12 col-lg-12 text-center">
          <div class="row">
            <hr class="red-line hidden-sm hidden-xs">

            <div class="col-md-2 bg-shema-work-item">
              <div class="icon-shema-work">
              <span class="icon-shema icon-clipboard-123"></span>
              <span class="circle">1</span>
              </div>
              <h3><?php echo Yii::t('frontend','bg-shema-work-text-2'); ?></h3>
              <p><?php echo Yii::t('frontend','bg-shema-work-text-3'); ?></p>
              <div class="text-center ">
              <a class="catalog-btn visible-xs visible-sm" href="/ru/site/#"  data-toggle="modal" data-target="#order1">Заказать</a>
              </div>
            </div>

            <div class="col-md-2  bg-shema-work-item">
              <div class="icon-shema-work">
               <span class="icon-shema icon-men-123"></span>
              <div class="circle">2</div>
              </div>
              <h3><?php echo Yii::t('frontend','bg-shema-work-text-4'); ?></h3>
              <p><?php echo Yii::t('frontend','bg-shema-work-text-5'); ?></p>
            </div>

            <div class="col-md-2  bg-shema-work-item">
              <div class="icon-shema-work">
              <span class="icon-shema icon-payment-method"></span>
              <span class="circle">3</span>
              </div>
              <h3><?php echo Yii::t('frontend','bg-shema-work-text-6'); ?></h3>
              <p><?php echo Yii::t('frontend','bg-shema-work-text-7'); ?></p>
            </div>

            <div class="col-md-2  bg-shema-work-item">
              <div class="icon-shema-work">
              <span class="icon-shema icon-truck2"></span>
              <span class="circle">4</span>
              </div>
              <h3><?php echo Yii::t('frontend','bg-shema-work-text-8'); ?></h3>
              <p><?php echo Yii::t('frontend','bg-shema-work-text-9'); ?></p>
            </div>

            <div class="col-md-2  bg-shema-work-item">
              <div class="icon-shema-work">
              <span class="icon-shema icon-check-list"></span>
              <span class="circle">5</span>
              </div>
              <h3><?php echo Yii::t('frontend','bg-shema-work-text-10'); ?></h3>
              <p><?php echo Yii::t('frontend','bg-shema-work-text-11'); ?></p>
            </div>

            <div class="col-md-2  bg-shema-work-item">
              <div class="icon-shema-work">
              <span class="icon-shema icon-save-online"></span>
              <span class="circle">6</span>
              </div>
              <h3><?php echo Yii::t('frontend','bg-shema-work-text-12'); ?></h3>
              <p><?php echo Yii::t('frontend','bg-shema-work-text-13'); ?></p>
            </div>
            
          </div>
          <div class="row">
            
              <a href="#" class="catalog-btn visible-lg visible-md" data-toggle="modal" data-target="#order1"><?php echo Yii::t('frontend','To order'); ?></a>
            
          </div>
       </div>
    </div>
    </div>
</section>


<!-- Display right sidebar for small windows -->
<section class="visible-xs visible-sm">
  <div class="container-fluid">

        <!---Widget right column -->
    <div class="row">
          <div class="col-xs-12 col-sm-6">
              <div class="block-akcia-sm">
                <?php echo DbBanner::widget(['page' => '1', 'var'=>'1']); ?>
              </div>
           </div>
           <div class="col-xs-12 col-sm-6 block-link-xs-sm">
              <?php echo BgLinks::widget(); ?>
         </div>
      </div>
      <!---Widget right column -->
        <?php foreach ($widgets as $value)
         {
           if ($value['name'] == 'Последние новости')
                echo DbLastNews::widget(['count'=> $value->quantity]);

         } ?>
  </div>
</section>


    