<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Samples';
//$this->params['breadcrumbs'][] = $this->title;
$this->params['hederimage']='/images/heder-obrazcy.png';
?>

 <div class="content">
  	<div class="bg-title-block">
  		<h1 class="bg-title-block-h1">
  	      <span><?php echo Yii::t('frontend','samples-text-1'); ?></span>
      </h1>
  	 </div>
     
      <div class="row page_content">
          <div class="col-xs-12 col-md-4 col-lg-4">
            <div class="block-link-obrazcy">
              <div class="link-obrazcy-title"><?php echo Yii::t('frontend','samples-text-2'); ?> </div>
              <p><?php echo Yii::t('frontend','samples-text-3'); ?></p>
              <p><?php echo Yii::t('frontend','samples-text-4'); ?></p>
              <ul class="list-unstyled block-link">
                <li class="btn-yelow">
                  <a href="site/download" class="link-catalog-pdf">
                    <?php echo Yii::t('frontend', 'widget-bgliks-1') ?>
                  </a>
                  <span class="icon-btn-link-2"></span>
                </li>
                
              </ul>
            </div>
          </div>
          <div class="col-xs-12 col-md-8 col-lg-8">
            <?php Pjax::begin(); ?>

              <div class="block-obrazec">
               <?php $form = ActiveForm::begin(['id'=>'block-form-obrazec']); ?>

                  <div class="row heder-form-obrazec"> 
                    <div class="col-sm-6">
                      <div class="obrazec-form-title">
                            <?php echo Yii::t('frontend','samples-text-5'); ?>
                        </div>
                    </div>
                   <div class="col-sm-6">
                      
                      <?php if(Yii::$app->language == 'ru'){
                          echo $form->field($model, 'category')->dropDownList(\yii\helpers\ArrayHelper::map(
                            $categories,
                            'title_ru',
                            'title_ru'
                        ))->label(false) ; 
                        }elseif(Yii::$app->language == 'en'){
                            echo $form->field($model, 'category')->dropDownList(\yii\helpers\ArrayHelper::map(
                            $categories,
                            'title_en',
                            'title_en'
                        ))->label(false) ; 
                        }else{
                           echo $form->field($model, 'category')->dropDownList(\yii\helpers\ArrayHelper::map(
                            $categories,
                            'title_de',
                            'title_de'
                        ))->label(false) ; 

                        }
                       ?>
                    </div> 
                  </div>   
                  <div class="row">
                      <div class="col-sm-6">
                        
                        <div class="form-group field-contactform-name required">
                            <?php echo $form->field($model, 'name')->textInput(['placeholder'=>Yii::t('frontend','Name, company name')])->label(false) ?>
                        </div> 
                        
                        <div class="form-group field-contactform-email required">
                            <?php echo $form->field($model, 'email')->textInput(['id'=>'contactform-email','placeholder'=>Yii::t('frontend','Your e-mail')])->label(false) ?>
                        </div> 
                        
                      </div> 
                      <div class="col-sm-6 ">
                          <div class="form-group field-contactform-name required">
                             <?php echo $form->field($model, 'phone')->textInput(['id'=>'contactform-phone','placeholder'=>Yii::t('frontend','Phone')])->label(false) ?>
                        </div> 
                        <div class="form-group field-contactform-email required">
                             <?php echo $form->field($model, 'address')->textInput(['id'=>'contactform-address','placeholder'=>Yii::t('frontend','Address')])->label(false) ?>
                        </div>  
                      </div>
                  </div> 
                  <div class="row">
<!--                     --><?php //echo $form->field($model, 'verifyCode')->widget(Captcha::className(), [
//                    'template' => '<div class="col-lg-6 col-md-6 col-sm-6">{input}</div><div class="col-lg-6 col-md-6 col-sm-6">{image}</div>'
//                ])->label(FALSE); ?>
                      <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className()) ?>
                  </div>
                  <div class="row">
                    <div class="col-xs-12 text-center">
                      <button type="submit" id="page-obrazec-btn" class="btn btn-danger bg-btn" name="contact-button"><?php echo Yii::t('frontend','To order') ?></button>
                    </div>
                  </div>
              <!--   </div>   -->
              <?php ActiveForm::end(); ?>
            </div>
          <?php Pjax::end() ?>
            
          </div>
          <p class="text-center page-accent"><?php echo Yii::t('frontend','samples-text-6'); ?></p>
          <p class="text-center page-phone">+49(0) 151 279 43 90</p>
      </div>
  </div>