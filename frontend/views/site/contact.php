<?php
use yii\helpers\Html;
use frontend\widgets\DbOffices;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Pjax;
use frontend\widgets\BgLinks;


/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = Yii::t('frontend','Contacts');
//$this->params['breadcrumbs'][] = $this->title;
$this->params['hederimage']='/images/heder-contacts.png';
?>


  <div class="content">
    <div class="bg-title-block">
        <h1 class="bg-title-block-h1">
          <span><?php echo Yii::t('frontend','Contacts'); ?></span>
      </h1>
     </div>
 
      <!--Widget DgOffices -->

      <?php echo DbOffices::widget(); ?>

      <!--End Widget DgOffices -->


      <div class="row page_content ">
          <div class="col-md-8 block-form-contact">
            <div class="contact-title">
               <?php echo  Yii::t('frontend','footer-text-mobile-1'); ?>
            </div>
            
              <?php Pjax::begin(['id' => 'formPjax2']) ?>
              

               <?php $form = ActiveForm::begin(); ?>

       
            
             <div class="container"> 
             <?php if(isset($otvet))
                     {
                         echo Alert::widget([
                          'options' => [
                              'class' =>$alert_value 
                          ],
                          'body' => $otvet,
                          ]);
                      }
              ?>     
              <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group field-contactform-name required">
                      <?php echo $form->field($model_contact, 'name')->textInput(['placeholder'=>Yii::t('frontend','Name, company name')])->label(false) ?>
                        <!-- <input type="text" id="contactform-name" class="form-control" name="ContactForm[name]" placeholder="Имя / Название фирмы">
                        <div class="help-block"></div> -->
                    </div> 
                    <div class="form-group field-contactform-name required">
                         <?php echo $form->field($model_contact, 'phone')->textInput(['id'=>'contactform-phone','placeholder'=>Yii::t('frontend','Phone')])->label(false) ?>
                       <!--  <input type="text" id="contactform-name" class="form-control" name="ContactForm[phone]" placeholder="Телефон">
                       <div class="help-block"></div> -->
                    </div> 
                    <div class="form-group field-contactform-email required">
                      <?php echo $form->field($model_contact, 'email')->textInput(['id'=>'contactform-email','placeholder'=>Yii::t('frontend','Your e-mail')])->label(false) ?>
                        <!-- <input type="text" id="contactform-email" class="form-control" name="ContactForm[email]" placeholder="E-mai">
                        <div class="help-block"></div> -->
                    </div> 
                    <div class="form-group field-contactform-email required">
                         <?php echo $form->field($model_contact, 'skype')->textInput(['id'=>'contactform-skype','placeholder'=>'Skype'])->label(false) ?>
                        <!-- <input type="text" id="contactform-skype" class="form-control" name="ContactForm[skype]" placeholder="Skype">
                        <div class="help-block"></div> -->
                    </div>          
                            
                  </div> 
                  <div class="col-sm-6 bg-textarea">
                      <div class="form-group field-contactform-body required">
                         <?php echo $form->field($model_contact, 'body')->textArea(['id'=>'contactform-body', 'placeholder'=>Yii::t('frontend','Your message')])->label(false)  ?>
                        <!-- <textarea id="contactform-body" class="form-control" name="ContactForm[body]"  placeholder="Ваше сообщение"></textarea>
                                              <div class="help-block"></div> -->
                     </div>        
                  </div>
              </div> 
              <div class="row">
<!--              --><?php //echo $form->field($model_contact, 'verifyCode')->widget(Captcha::className(), [
//                    'template' => '<div class="col-lg-6 col-md-6 col-sm-6 ">{input}</div><div class="col-lg-6 col-md-6 col-sm-6">{image}</div>'
//                ])->label(FALSE); ?>
                  <?= $form->field($model_contact, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className()) ?>
              </div>
              <div class="row">
                <div class="col-sm-12 text-center">
                  <?= Html::submitButton(Yii::t('frontend','To order'), ['class' => 'btn btn-danger bg-btn','id'=>'page-contact-btn']) ?>
                  
                </div>
              </div>
              </div>  
             <?php ActiveForm::end(); ?>
          <?php Pjax::end() ?>
          </div>
           <div class="col-md-4 block-contact-links">
             <!--Widget BgLinks -->

              <?php echo BgLinks::widget(); ?>
        </div>
      </div>
  </div>

