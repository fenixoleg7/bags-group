<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>

<section>
   <div class="container page-error">
    <div class="row">
      <div class="col-md-12 col-lg-12 text-center">
        <p class="error-message"><?php echo nl2br(Html::encode($message)) ?></p>
        <p class="erorr-404">404</p>
        <p class="error-text"><?php echo Yii::t('frontend', 'bg-page-error-1'); ?></p>
        <p><a href="/site/index" class="catalog-btn"><?php echo Yii::t('frontend','Home') ?></a></p>
      </div>
    </div>
   </div>
</section>