<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */
/* @var $form ActiveForm */
?>
<div class="orders">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'product_id') ?>
        <?= $form->field($model, 'product_name') ?>
        <?= $form->field($model, 'client_name') ?>
        <?= $form->field($model, 'client_phone') ?>
        <?= $form->field($model, 'client_email') ?>
        <?= $form->field($model, 'product_kod') ?>
        <?= $form->field($model, 'product_quantity') ?>
        <?= $form->field($model, 'status') ?>
        <?= $form->field($model, 'created_at') ?>
        <?= $form->field($model, 'product_prise') ?>
        <?= $form->field($model, 'client_message') ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- orders -->
