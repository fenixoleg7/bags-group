<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use frontend\widgets\DbSiteInfo;
use frontend\widgets\DbContact;
use frontend\models\ContactForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use cybercog\yii\googleanalytics\widgets\GATracking;


/* @var $this \yii\web\View */
/* @var $content string */

$this->beginContent('@frontend/views/layouts/_clear.php');
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/jpg', 'href' => Url::to(['/bgi.jpg'])]);
?>
    <?php echo DbContact::widget(['key' => 'popup-contact']); ?>
        
      
   <div class="navmenu navmenu-default navmenu-fixed-left offcanvas-sm bags-group-left-menu">
      
      <?php 
        echo Nav::widget([
            'options' => ['class' => 'nav navmenu-nav'],
            'encodeLabels' => false,
            'items' => [
                ['label' => '<span class="icon-home4"></span>'.Yii::t('frontend', 'Home'), 'url' => ['site/index']],
                ['label' => '<span class="icon-work-team"></span>'.Yii::t('frontend', 'Company'), 'url' => ['/page/view', 'slug'=>'about']],
                ['label' => '<span class="icon-options"></span>'.Yii::t('frontend', 'Products'), 'url' => ['/catalog/index']],
                ['label' => '<span class="icon-like"></span>'.Yii::t('frontend', 'Trust'), 'url' => ['/page/benefits']],
                ['label' => '<span class="icon-handshake"></span>'.Yii::t('frontend', 'Services'), 'url' => ['/page/view', 'slug'=>'services']],
                ['label' => '<span class="icon-envelope"></span>'.Yii::t('frontend', 'Contacts'), 'url' => ['/site/contact']],
                //['label' => Yii::t('frontend', 'Request'), 'url' => ['#'],'linkOptions'=>['class'=>'bags-group-cart bags-group-icons','data-toggle'=>'modal', 'data-target'=>'#order1']],
            ]
        ]); 
       ?>
    </div> 
    
    <div class="navbar navbar-default navbar-fixed-top hidden-md hidden-lg">
      <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".navmenu">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <header id="header">
       
     <div id="topNav">
        <div id="topNavContentContainer">
          <div id="topContentContact" class="topNavContent">
           
             <?php echo DbSiteInfo::widget([
                        'key' => 'heder-info'
                      ]) ?>
          </div>
          <div id="topContentService" class="topNavContent">
            <ul class="menu">
              <li class="first"><a href="/page/services"><span><?php echo Yii::t('frontend', 'top-content-service-1'); ?></span></a></li>
              <li><a href="/page/services"><span><?php echo Yii::t('frontend', 'top-content-service-2'); ?></span></a></li>
              <li><a href="/page/services"><span><?php echo Yii::t('frontend', 'top-content-service-3'); ?></span></a></li>
          </div>
          <div id="topContentLanguage" class="topNavContent">
           <?php 
              echo Nav::widget([
              'options' => ['class' => 'menu',' id'=>'langSwitch'],
              'items' => array_map(function ($code) {
                      return [
                          'label' => Yii::$app->params['availableLocales'][$code],

                          'url' => array_merge(
                                        \Yii::$app->request->get(),
                                        [\Yii::$app->controller->route, 'language' => $code]
                                    ),                      
                          'active' => Yii::$app->language === $code
                      ];
                  }, array_keys(Yii::$app->params['availableLocales'])),
                  'encodeLabels' => false
                ]); 
              ?>

          </div>
          <div id="topContentSearch" class="topNavContent">
              <form method="get" action="/search/index" id="topSearchForm">
               
                <input id="sucheTextHeader" type="text" name="query_user" placeholder="<?php echo Yii::t('frontend', 'Search'); ?>">
                <input  type="submit" id="searchSubmitHeader" value="<?php echo Yii::t('frontend', 'Search'); ?>" />
                
              </form>
          </div>
        </div>
        <!-- <div id="topNavItems">
            <div id="topContact" class="topNavItem"><?php //echo Yii::t('frontend', 'Contact'); ?></div>
            <div id="topService" class="topNavItem"><?php //echo Yii::t('frontend', 'Delivery'); ?></div>
            <div id="topLanguage" class="topNavItem"><?php //echo Yii::t('frontend', 'Language'); ?></div>
            <div id="topSearch" class="topNavItem"><?php //echo Yii::t('frontend', 'Search'); ?></div>
            <div class="topNavItem close"></div>
        </div>  -->
        <div id="topNavItems">
            <div id="topOrder"class="topNavItem">
              <span class="bg-icon-top icon-shopping-cart"></span>
              <a id="order-btn"   href="#" data-toggle="modal" data-target="#order1"><?php echo Yii::t('frontend','Request'); ?></a>
            </div>
            <div id="topService" class="topNavItem">
              <span class="bg-icon-top icon-delivery-truck"></span>
              <span class="hidden-xs"><?php echo Yii::t('frontend', 'Delivery'); ?></span>
            </div>
            <div id="topLanguage" class="topNavItem">
              <span class="bg-icon-top icon-earth-grid-symbol"></span>
              <span class="hidden-xs"><?php echo Yii::t('frontend', 'Language'); ?></span>
            </div>
            <div id="topSearch" class="topNavItem">
              <span class="bg-icon-top icon-active-search-symbol"></span>
              <span class="hidden-xs"><?php echo Yii::t('frontend', 'Search'); ?></span>
            </div>
            <div class="topNavItem close"></div>
        </div> 
     </div>
      <?php //echo GATracking::widget([
          //'trackingId' => 'UA-111412678-1',
      //]) ?>
    </header>
    
    <?php echo $content; ?>

 <footer>
      <div class="container-fluid">
        <div class="row ">
          <div class="col-bg-footer col-sm-12 logo-footer-block text-cetner">
            <?php echo DbSiteInfo::widget([
              'key' => 'logo-footer'
            ]) ?>
            <ul class="list-inline">
              <li><a href="https://www.facebook.com/bagsgroup.de/"><span class="icon-facebook"></span></a></li>
              <li><a href="skype:bagsgroup"><span class="icon-skype"></span></a></li>
              <li><a href="https://www.linkedin.com/in/bagsgroup/"><span class="icon-linkedin2"></span></a></li>
              <li><a href="https://login.xing.com/bagsgroup"><span class="icon-xing2"></span></a></li>
            </ul>

          </div>
          <div class="col-bg-footer  bg-address hidden-sm hidden-xs">
              <h2><?php echo Yii::t('frontend', 'Contacts'); ?></h2>
                
                <div class="panel-group" id="accordion">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="contact-footer-link">
                                Deutchland
                                <span class="icon-arrow-right2"></span>
                              </a>
                              
                            </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                      <div class="panel-body">
                        <address>
                          Berlin<br>
                          Tel.  +49 (0) 151 279 430 90<br>
                          E-Mail: info@bags-group.de<br>
                        </address>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="contact-footer-link">
                         Poland 
                         <span class="icon-arrow-right2"></span>
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                      <div class="panel-body">
                        <address>
                          Szczecin<br>
                          Tel.  . +48 (0) 6006 99 436<br>
                          E-Mail: info@bags-group.de<br>
                        </address>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="contact-footer-link">
                                Lithuania 
                                <span class="icon-arrow-right2"></span>
                              </a>

                            </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                      <div class="panel-body">
                         <address>
                          Vilnius<br>
                          Tel.  . +37 (0) 6180 90 06<br>
                          E-Mail: info@bags-group.de<br>
                        </address>
                      </div>
                    </div>
                  </div>
                </div>
          </div>
          <div class="col-bg-footer bg-links hidden-sm hidden-xs">
            <h2><?php echo Yii::t('frontend', 'Catalog products'); ?></h2>
           <?php echo \frontend\widgets\DbCatalog::widget([
                  'key' => 'footer-category-list'
                ]) ?>
          </div>
          <div class="col-bg-footer bg-links hidden-sm hidden-xs">
             <h2><?php echo Yii::t('frontend','footer-link-1');?></h2>
              <ul class="list-unstyled">
                <li><a href="/page/benefits"><?php echo Yii::t('frontend', 'footer-link-2'); ?></a></li>
                <li><a href="/page/services"><?php echo Yii::t('frontend', 'footer-link-3'); ?></a></li>
                <li><a href="/page/services"><?php echo Yii::t('frontend', 'footer-link-4'); ?></a></li>
                <li><a href="/site/download"><?php echo Yii::t('frontend', 'footer-link-5'); ?></a></li>
                <li><a href="/site/samples"><?php echo Yii::t('frontend', 'footer-link-6'); ?></a></li>
                <li><a href="/contact"><?php echo Yii::t('frontend', 'footer-link-7'); ?></a></li>
              </ul>           
          </div> 
          <div class="col-bg-footer bg-contact-form hidden-sm hidden-xs">
            <h2><?php echo Yii::t('frontend','Newsletter'); ?></h2>
             <?php echo DbContact::widget([
                'key' => 'footer-contact'
             ]); ?> 
          </div>
        </div>
       <!--  Dispay the footer for small windows -->
       <div class="row visible-xs visible-sm">
          <div class="col-sm-12">
            <div class="panel-group bg-footer-sm" id="accordion_sm">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h2 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion_sm" href="#collapseOneMob">
                       <?php echo Yii::t('frontend','Contacts');?>
                      </a>
                  </h2>
                </div>
                <div id="collapseOneMob" class="panel-collapse collapse">
                   <div class="panel-body">
                    <div class="panel-group" id="accordion_sm_contact">
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion_sm_contact" href="#collapseOneMob1" class="contact-footer-link">
                                      Deutchland
                                      <span class="icon-arrow-right2"></span>
                                    </a>
                                    
                                  </h4>
                          </div>
                          <div id="collapseOneMob1" class="panel-collapse collapse in">
                            <div class="panel-body">
                              <address>
                                Berlin<br>
                                Tel.  +49 (0) 151 279 430 90<br>
                                E-Mail: info@bags-group.de<br>
                              </address>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion_sm_contact" href="#collapseTwoMob1" class="contact-footer-link">
                               Poland 
                               <span class="icon-arrow-right2"></span>
                              </a>
                            </h4>
                          </div>
                          <div id="collapseTwoMob1" class="panel-collapse collapse">
                            <div class="panel-body">
                              <address>
                                Szczecin<br>
                                Tel.  . +48 (0) 6006 99 436<br>
                                E-Mail: info@bags-group.de<br>
                              </address>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion_sm_contact" href="#collapseThreeMob1" class="contact-footer-link">
                                      Lithuania 
                                      <span class="icon-arrow-right2"></span>
                                    </a>

                                  </h4>
                          </div>
                          <div id="collapseThreeMob1" class="panel-collapse collapse">
                            <div class="panel-body">
                               <address>
                                Vilnius<br>
                                Tel.  . +37 (0) 6180 90 06<br>
                                E-Mail: info@bags-group.de<br>
                              </address>
                            </div>
                          </div>
                        </div>
                      </div>
                  
                </div>
                  
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h2 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion_sm" href="#collapseTwoMob">
                       <?php echo Yii::t('frontend', 'Catalog products') ;?>
                     </a>
                  </h2>
                </div>
                <div id="collapseTwoMob" class="panel-collapse collapse">
                 <div class="panel-body">
                    <div class="bg-address-sm">
                             <?php echo \frontend\widgets\DbCatalog::widget([
                        'key' => 'footer-category-list'
                      ]) ?>
                    </div>
                  </div>
                  </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h2 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion_sm" href="#collapseThreeMob">
                       <?php echo Yii::t('frontend','footer-link-1');?>
                     </a>
                  </h2>
                </div>
                <div id="collapseThreeMob" class="panel-collapse collapse">
                 <div class="panel-body">
                    <div class="bg-address-sm">
                      <ul class="list-unstyled">
                        <li><a href="#"><?php echo Yii::t('frontend', 'footer-link-2'); ?></a></li>
                        <li><a href="#"><?php echo Yii::t('frontend', 'footer-link-3'); ?></a></li>
                        <li><a href="#"><?php echo Yii::t('frontend', 'footer-link-4'); ?></a></li>
                        <li><a href="/site/download"><?php echo Yii::t('frontend', 'footer-link-5'); ?></a></li>
                        <li><a href="/site/samples"><?php echo Yii::t('frontend', 'footer-link-6'); ?></a></li>
                        <li><a href="/contact"><?php echo Yii::t('frontend', 'footer-link-7'); ?></a></li>
                      </ul> 
                    </div>
                  </div>
                  </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h2 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion_sm" href="#collapseFourMob">
                      <?php echo Yii::t('frontend','Newsletter'); ?>
                     </a>
                  </h2>
                </div>
                <div id="collapseFourMob" class="panel-collapse collapse">
                  <div class="panel-body">
                    <div id="formPjax" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">
                       <?php echo DbContact::widget([
                          'key' => 'footer-contact'
                       ]); ?> 
                     </div>      
 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
   
      </div>
    </footer>
     
                           
                         
<?php $this->endContent() ?>