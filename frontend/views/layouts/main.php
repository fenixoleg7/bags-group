<?php
/* @var $this \yii\web\View */
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;
use frontend\widgets\DbSiteInfo;


/* @var $content string */

$this->beginContent('@frontend/views/layouts/base.php')
?>
<!--  Содержимое  основное  -->
   <div class="heder-page">
    <?php if(isset($this->params['hederimage'])) :?>
        <div id="background_logo" class="text-center" style="background-image: url(<?php echo $this->params['hederimage'] ?>)">
            <div class="substrate">
               <img src="/images/logo-page.png" alt="logo-page"  /> 
            </div>
        </div>
        

    <?php else: ?>

        <div id="background_logo" class="text-center">
            <div class="substrate">
               <img src="/images/logo-page.png" alt="logo-page"  /> 
            </div>
        </div>
    
    <?php endif; ?>
   </div>

   <div class="container">

        <?php if(isset($this->params['breadcrumbs']))
        {
             echo Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'options' => ['class'=>'bg-breadcrumbs']
            ]);
         }
        ?>

        <?php if(Yii::$app->session->hasFlash('alert')):?>
            <?php echo \yii\bootstrap\Alert::widget([
                'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
            ])?>
        <?php endif; ?>

        <!-- Example of your ads placing -->
        <?php echo \frontend\widgets\DbText::widget([
            'key' => 'ads-example'
        ]) ?>

        <?php echo $content ?>

   </div> 
<?php $this->endContent() ?>