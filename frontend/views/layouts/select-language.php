<?php

use yii\bootstrap\Html;

/*
    echo Html::a('Go to English', array_merge(
        \Yii::$app->request->get(),
        [\Yii::$app->controller->route, 'language' => 'en']
    ));

    echo Html::a('Перейти на русский', array_merge(
        \Yii::$app->request->get(),
        [\Yii::$app->controller->route, 'language' => 'ru']
    ));
*/


echo Nav::widget([
'options' => ['class' => 'menu',' id'=>'langSwitch'],
'items' => array_map(function ($code) {
        return [
            'label' => Yii::$app->params['availableLocales'][$code],
            'url' => [array_merge(\Yii::$app->request->get(),[\Yii::$app->controller->route, 'language' => $code])],
	        
	        
            'active' => Yii::$app->language === $code
        ];
    }, array_keys(Yii::$app->params['availableLocales']))
  ]); ?>
