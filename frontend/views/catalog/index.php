<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\CatalogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = $seodate_lang['title'];
$this->registerMetaTag([
    'name' => 'description',
    'content' => strip_tags($seodate_lang['description'])
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $seodate_lang['keywords']
]);

$this->params['hederimage'] = $seomodel->getImageUrl();

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content">
    <div class="bg-title-block">
        <h1 class="bg-title-block-h1">
           <span><?php echo $seodate_lang['title'] ?></span>
        </h1>
     </div>
    <div class="page_content">
        <?php echo \yii\widgets\ListView::widget([
        'dataProvider'=>$dataProvider,
        'itemOptions' => ['class' => 'row item-category-block'],
        'pager'=>[
            'hideOnSinglePage'=>true,
        ],
        'layout' => "{items}\n<div class='b-pagenation n-table hidden-sm hidden-xs'>{pager}</div>",
        'itemView'=>'_item'
    ])?>
    </div>
</div>

