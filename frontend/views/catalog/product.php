<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\widgets\DbCatalog;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Pjax;
use frontend\widgets\DbSiteInfo;
use yii\bootstrap\Alert;
use frontend\widgets\BgLinks;

/*....................*/
\frontend\assets\ProductAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\CatalogCategory */


$this->title = $product_lang['title'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Catalog products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


      
<!-- Product view -->

<div class="product-view row">
  <div class="col-sm-6 col-md-6 col-lg-6">
   
      <div class="flexslider">
          <ul class="slides">
            <?php if ($product_model->thumbnail_path_1!=''): ?>
              <li><img src="<?php echo $product_model->getImagesUrl($product_model->thumbnail_path_1) ?>"></li>
            <?php endif ?>
            <?php if ($product_model->thumbnail_path_2!=''): ?>
              <li><img src="<?php echo $product_model->getImagesUrl($product_model->thumbnail_path_2) ?>"></li>
            <?php endif ?>
            <?php if ($product_model->thumbnail_path_3!=''): ?>
              <li><img src="<?php echo $product_model->getImagesUrl($product_model->thumbnail_path_3) ?>"></li>
            <?php endif ?>
            <?php if ($product_model->thumbnail_path_4!=''): ?>
              <li><img src="<?php echo $product_model->getImagesUrl($product_model->thumbnail_path_4) ?>"></li>
            <?php endif ?>
            <?php if ($product_model->thumbnail_path_5!=''): ?>
              <li><img src="<?php echo $product_model->getImagesUrl($product_model->thumbnail_path_5) ?>"></li>
            <?php endif ?>
            <?php if ($product_model->thumbnail_path_6!=''): ?>
              <li><img src="<?php echo $product_model->getImagesUrl($product_model->thumbnail_path_6) ?>"></li>
            <?php endif ?>
             
          </ul>
      </div> 
  </div>
  <div class="col-sm-6 col-md-6 col-lg-6"> 
    <!-- Product label -->
    <div class="product-label">
      <div class="box-wrap">
        <div class="box-wrap-top"></div>
        <div class="box-wrap-bot"></div>
        <div class="box-wrap-center"></div>
        <div class="box">
          <div class="box-content">
            
            <?php echo Html::tag('h1', $product_lang['title']); ?>

              
        <p class="product-kod"> <?php echo Yii::t('frontend', 'Code:').$product_model->kod; ?></p> 
        <p class="product-parametr"> <?php echo Yii::t('frontend', 'The size:').$product_model->size; ?></p> 
        <p class="product-parametr"> <?php echo Yii::t('frontend', 'Roominess:').$product_model->capacity; ?></p> 
        <div class="product-value">
          <table class="table item-table-product">
            <thead>
                <tr>
                    <th><?php echo Yii::t('frontend', 'Quantity, pcs.'); ?></th>
                    <th class="text-center"><?php echo Yii::t('frontend', 'Price, $ / pcs.'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="color_ed">1-100</td>
                    <td class="text-center">
                        <?php echo  DbSiteInfo::widget([
                            'key' => 'tauscher',
                            'tauscher_value' => $product_model->price_1
                        ]) ?></td>
                </tr>
                <tr class="color-f2">
                    <td >100-500</td>
                    <td class="text-center">
                        <?php echo  DbSiteInfo::widget([
                            'key' => 'tauscher',
                            'tauscher_value' => $product_model->price_2
                        ]) ?></td></td>
                </tr>
                <tr>
                    <td >500-1000</td>
                    <td class="text-center">
                        <?php echo  DbSiteInfo::widget([
                            'key' => 'tauscher',
                            'tauscher_value' => $product_model->price_3
                        ]) ?></td></td>
                </tr>
                <tr class="color-f2">
                    <td >1000-5000</td>
                    <td class="text-center">
                        <?php echo  DbSiteInfo::widget([
                            'key' => 'tauscher',
                            'tauscher_value' => $product_model->price_4
                        ]) ?></td></td>
                </tr>
                <tr>
                    <td >от 5000</td>
                    <td class="text-center">
                        <?php echo  DbSiteInfo::widget([
                            'key' => 'tauscher',
                            'tauscher_value' => $product_model->price_5
                        ]) ?></td></td>
                </tr>
            </tbody>
          </table>
          <div class="text-center">
            <?php if(Yii::$app->language == 'de') {
              echo Html::tag('p', Yii::t('frontend', 'Alle Preise verstehen sich inkl. MwSt. und zzgl. Versandkosten'),['class' => 'product-price-aczent']);}?>
             

            <?php echo Html::a(Yii::t('frontend','To order'), ['#'],['class'=>'cart-buttton', 'data-toggle'=>'modal', 'data-target'=>'#order']); ?>
          </div>
         </div>
        </div>
      </div>
    </div>
    <!-- //end Product label --> 
    
    <!-- Description -->
    <div class="product-description">

             <?php if(Yii::$app->language == 'ru') 

                    echo $product_model->body_ru;  

                  elseif(Yii::$app->language == 'en') 

                    echo $product_model->body_en; 

                  elseif(Yii::$app->language == 'pl') 

                    echo $product_model->body_pl; 

                  else  echo $product_model->body_de; 
            ?>

      <!-- AddThis Button BEGIN -->
      </div>
    </div>
   </div>


    <!-- Modal -->
    
    <div class="modal fade bs-example-modal-lg" id="order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg ">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel"><?php echo Yii::t('frontend', 'Checkout'); ?></h4>
            <p><?php echo Yii::t('frontend', 'Leave your order or message <br> and we will contact you') ?></p>
          </div>
          <div class="modal-body">
            <?php Pjax::begin(['id' => 'formPjaxOrder']); ?>
            <?php $form = ActiveForm::begin(['id' => 'order-form-pop','options' => ['data-pjax' => true]]); ?>
            <?php //echo $form->errorSummary($model); ?>
            <?php if($otvet_order != "")
               {
                   echo Alert::widget([
                    'options' => [
                        'class' =>$alert_order,
                    ],
                    'body' => $otvet_order,
                    ]);
                }
             ?>
                <div class="container">
                  <div class="row">
                  
                    <h4><?php echo Yii::t('frontend','Product') ?>: <span><?php if($model->product_id!='') echo $model->product_id ?> </span></h4>
                    <div class="col-md-4"><?php echo $form->field($model, 'product_name') ?></div>
                    <div class="col-md-4"><?php echo $form->field($model, 'product_quantity') ?></div>
                    <div class="col-md-4"><?php echo $form->field($model, 'product_price')->textInput(['readonly' => true]) ?></div>
                  </div>
                  <div class="row">
                    <?php  echo $form->field($model, 'price_1')->hiddenInput(['id'=>'price_1'])->label(false);  ?>
                    <?php  echo $form->field($model, 'price_2')->hiddenInput(['id'=>'price_2'])->label(false);  ?>
                    <?php  echo $form->field($model, 'price_3')->hiddenInput(['id'=>'price_3'])->label(false);  ?>
                    <?php  echo $form->field($model, 'price_4')->hiddenInput(['id'=>'price_4'])->label(false);  ?>
                    <?php  echo $form->field($model, 'price_5')->hiddenInput(['id'=>'price_5'])->label(false);  ?>
                    <?php  echo $form->field($model, 'cost')->hiddenInput(['id'=>'total_cost'])->label(false);  ?>
                    <?php  echo $form->field($model, 'currency')->hiddenInput(['id'=>'currency'])->label(false);  ?>
                    <div class="col-md-12 text-center">
                      <p class="text-price"><?php echo Yii::t('frontend', 'The cost of your order:') ?>
                        <span id="cost"></span>
                        <span class="currency"><?php echo $product_lang['currency'] ?></span>
                      </p> 
                    </div>
                  </div>
                  <div class="row">
                    <h4><?php echo Yii::t('frontend','Contact Information') ?></h4>
                    <div class="col-md-4"><?php echo $form->field($model, 'name') ?></div>
                    <div class="col-md-4"><?php echo $form->field($model, 'email') ?></div>
                    <div class="col-md-4"><?php echo $form->field($model, 'phone') ?></div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                       <?php echo $form->field($model, 'body')->textArea(['rows' => 6]) ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                    <?php echo $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?>
                    </div>
                  </div>
                  <div class="form-group text-center">
                      <?php echo Html::submitButton(Yii::t('frontend', 'Submit'), ['class' => 'btn btn-danger bg-btn', 'name' => 'contact-button']) ?>
                  </div>
              </div>
            <?php ActiveForm::end(); ?> 
            <?php Pjax::end(); ?>
          </div>
          
        </div>
      </div>
    </div>
   
</div>
<!--  Dispay the footer for small windows -->
<div class="row visible-xs visible-sm right-sidbar-widjet-sm">
  
                <?php echo BgLinks::widget([
                  'key' => 'bg-link-page'
                  ]); ?>

</div>

