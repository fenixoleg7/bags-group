<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\widgets\DbCatalog;
use frontend\widgets\BgLinks;

/* @var $this yii\web\View */
/* @var $model common\models\CatalogCategory */


$this->title = $model['title'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Catalog products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marg_left row">
    <div class=" col-xs-12 col-sm-12 col-md-9 col-lg-9 bg-title-block">
        <h1 class="bg-title-block-h1">
           <span><?= Html::encode($model['title']) ?></span>
        </h1>
    </div>
</div>
<div class="marg_left row">
    <div class=" col-lg-9  col-md-9  col-sm-12 col-xs-12 content-block">
    <div class="catalog-list category-page">
        <?php echo DbCatalog::widget(['key' => 'category-list', 'parent_id' => $parent_id]); ?>
    </div>
    <div class="category-content">
        <?php echo $model['body'];?>
    </div>
</div>
<div class=" col-lg-3  col-md-3  hidden-sm hidden-xs right-sidbar">
            
             <?php echo \frontend\widgets\DbCatalog::widget([
              'key' => 'sidebar-category-list'
            ]) ?>

             <?php echo BgLinks::widget(); ?>
       
    </div>
</div>
<!--  Dispay the footer for small windows -->
<div class="row visible-xs visible-sm right-sidbar-widjet-sm">
  
                <?php echo BgLinks::widget([
                  'key' => 'bg-link-page'
                  ]); ?>

</div>

