<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\widgets\DbCatalog;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Pjax;
use frontend\widgets\BgLinks;

/* @var $this yii\web\View */
/* @var $model common\models\CatalogCategory */


$this->title = $model_lang['title'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Catalog products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marg_left row">
    <div class=" col-xs-12 col-sm-12 col-md-9 col-lg-9 bg-title-block">
        <h1 class="bg-title-block-h1">
           <span><?= Html::encode($model_lang['title']) ?></span>
        </h1>
    </div>
</div>
<div class="marg_left row">
    <div class=" col-lg-9  col-md-9  col-sm-12 col-xs-12 content-block">
        <div class="catalog-list category-page">
            <?php //echo DbCatalog::widget(['key' => 'category-list', 'parent_id' => $parent_id]); ?>
            <?php echo \yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'row item-products-block'],
                'layout' => "{items}\n<div class='text-center hidden-sm hidden-xs'>{pager}</div>",
                'itemView' => '_product',
                 'pager' => [

                              'options' => [
                                  'class' => 'pagination',
                              ],

                              'prevPageLabel' => '<span class="prev icon-arrow-left2"></span>',
                              'nextPageLabel' => '<span class="next icon-arrow-right2"></span>',
                              'activePageCssClass' => 'bg-active',
                              'prevPageCssClass' => 'bg-prev ',
                              'nextPageCssClass' => 'bg-next ',
                              'pageCssClass' => 'bg-pagenation__item',
                              'maxButtonCount' => 6,
                            ]
              ]);
             ?>
        </div>
       
    </div>
    <div class=" col-lg-3  col-md-3  hidden-sm hidden-xs right-sidbar">
            
             <?php echo \frontend\widgets\DbCatalog::widget([
              'key' => 'sidebar-category-list'
            ]) ?>

            <?php echo \frontend\widgets\DbCatalog::widget([
              'key' => 'sidebar-subcategory-list',
              'category_id' =>$model_lang['id']
            ]) ?>
       
            <?php echo BgLinks::widget(); ?>
    </div>
 
</div>
<!--  Dispay the footer for small windows -->
<div class="row visible-xs visible-sm right-sidbar-widjet-sm">
  
                <?php echo BgLinks::widget([
                  'key' => 'bg-link-page'
                  ]); ?>

</div>

