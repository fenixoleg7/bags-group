<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\Article
 */
use yii\helpers\Html;
use frontend\widgets\DbSiteInfo;

?>


<div class="product-prev-img col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <?php if ($model->thumbnail_path_1)

       echo Html::img($model->getImageUrl(),
        [
        'class' => 'img-responsive',
        'alt' => 'title'
        ]);
    ?>
</div>
<div class="product-content col-lg-5 col-md-5 col-sm-12 col-xs-12">
        
        <?php if(Yii::$app->language == 'ru') 

                echo Html::tag('h2', $model->title_ru);

              elseif(Yii::$app->language == 'en') 

                echo Html::tag('h2', $model->title_en);

              else echo Html::tag('h2', $model->title_de);

        ?>
        <p class="product-kod"> <?php echo Yii::t('frontend', 'Code:').$model->kod; ?></p> 
        <p class="product-parametr"> 
        <?php echo Yii::t('frontend', 'The size:').$model->size;?><br>
        <?php echo Yii::t('frontend', 'Roominess:').$model->capacity;?></p> 
        <div class="item-category-content">
            <?php if(Yii::$app->language == 'ru') 

                    echo \yii\helpers\StringHelper::truncate(strip_tags($model->body_ru), 150, '...', null, false); 

                  elseif(Yii::$app->language == 'en') 

                    echo \yii\helpers\StringHelper::truncate(strip_tags($model->body_en), 150, '...', null, false); 

                  else echo \yii\helpers\StringHelper::truncate(strip_tags($model->body_de), 150, '...', null, false); 
            ?>
        </div>
        <div class="text-center cart-buttton">
            <?php echo Html::a(Yii::t('frontend','More'), ['catalog/product/'.$model->id],['class'=>'catalog-btn-item']); ?>
        </div>
</div>
<div class="product-value col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <table class="table item-table-product">
            <thead>
                <tr>
                    <th><?php echo Yii::t('frontend', 'Quantity, pcs.'); ?></th>
                    <th class="text-center"><?php echo Yii::t('frontend', 'Price, $ / pcs.'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="color_ed">1-100</td>
                    <td class="text-center">
                        <?php echo  DbSiteInfo::widget([
                            'key' => 'tauscher',
                            'tauscher_value' => $model->price_1
                        ]) ?></td>
                </tr>
                <tr class="color-f2">
                    <td >100-500</td>
                    <td class="text-center">
                        <?php echo  DbSiteInfo::widget([
                            'key' => 'tauscher',
                            'tauscher_value' => $model->price_2
                        ]) ?></td></td>
                </tr>
                <tr>
                    <td >500-1000</td>
                    <td class="text-center">
                        <?php echo  DbSiteInfo::widget([
                            'key' => 'tauscher',
                            'tauscher_value' => $model->price_3
                        ]) ?></td></td>
                </tr>
                <tr class="color-f2">
                    <td >1000-5000</td>
                    <td class="text-center">
                        <?php echo  DbSiteInfo::widget([
                            'key' => 'tauscher',
                            'tauscher_value' => $model->price_4
                        ]) ?></td></td>
                </tr>
                <tr>
                    <td >от 5000</td>
                    <td class="text-center">
                        <?php echo  DbSiteInfo::widget([
                            'key' => 'tauscher',
                            'tauscher_value' => $model->price_5
                        ]) ?></td></td>
                </tr>
            </tbody>
          </table>
         
</div>

