<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\Game
 */
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

?>


<div class="col-md-12">
    <h2><?php echo Html::encode($model['title_ru']) ?></h2>
	<div class="news_content">
            <?php echo \yii\helpers\StringHelper::truncate(strip_tags($model['body_ru']), 250, '...', null, false); ?>
    </div>
    <div class="read_more_a pull-right">
        <?php echo Html::a(Yii::t('frontend','Read completely'), $model['url']); ?>
    </div>
</div>


