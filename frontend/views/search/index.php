<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\GameGenre;
use common\models\GamePlatform;
use yii\widgets\ListView;
use yii\db\Query;
/* @var $this yii\web\View */
$this->title = Yii::t('frontend', 'Search')
?>
<!-- b-header -->

<div class="content content-block">
    <div class="bg-title-block">
        <h1 class="bg-title-block-h1">
           <?php echo Yii::t('frontend','Search Results:')?>
        </h1>
     </div>
     <div class="row">
        <div class="col-md-12 search-block">
           <form method="get" action="/search"class="form-inline" role="form">
              <div class="input-group">
                  <input type="search" name="query_user" class="form-control"  placeholder="<?php echo Yii::t('frontend', 'Search');?>">
                  <span class="input-group-btn">
                    <button type="submit" class="btn  btn-danger search-btn"><span class="glyphicon glyphicon-search"></span></button>
                  </span>
              </div>
            </form>
        </div> 
    </div> 
    
    <div class="row"> 
        <div class="col-md-12">
            <div class="page_content">
                 <?php echo ListView::widget([
                    'dataProvider' => $provider,
                    'itemOptions' => ['class' => 'row item-news-block'],
                    'pager'=>[
                        'hideOnSinglePage'=>true,
                    ],
                    'layout' => "{items}\n<div class='b-pagenation n-table hidden-sm hidden-xs'>{pager}</div>",
                    'itemView' => '_item',
                ]); ?>
            </div>
        </div>
    </div>
</div>


