
<?php
/* @var $this yii\web\View */
$this->title = Yii::t('frontend', 'Клиентские игры')
?>
<!-- b-header -->

<div class="b-page__head">
            <div class="b-page__head--bg">
                <div class="container clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="b-page__title">Поиск <span>по сайту</span></h1>
                            <p class="b-page__desc visible-sm visible-xs">Тера онлайн — это бесплатная фентези ммо rpg нового
                                рафывалфывлдаывфлд лталыфоалоьлд лвталдтывдлат </p>
                            <?php $form = ActiveForm::begin([
                                    'id' => 'search-form',
                                    'options' => ['class' => 'search-form'],
                                ]) ?>
                                    <?php echo $form->field($model, 'name') ?>
                                    <?php echo $form->field($model, 'genre') ?>
                                    <?php echo $form->field($model, 'platform') ?>

                                    <div class="form-group">
                                        <div class="col-lg-offset-1 col-lg-11">
                                            <?php echo Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
                                        </div>
                                    </div>
                                <?php ActiveForm::end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- b-page__head -->
<div class="container">
    <div class="row">
        <div class="b-container col-md-8">
            
            <div class="alert alert-danger"> К сожалению, поиск не дал результатов...</div>

           
        </div><!-- b-container -->
        <div class="b-sidebar col-md-4 hidden-sm hidden-xs">

            <!--Виджет последних новостей-->
            <?php echo \common\widgets\DbLastNews::widget(); ?>

            <!--Виджет последних обзоров-->
            <?php echo \common\widgets\DbLastReviews::widget(); ?>
           
        </div><!-- b-sidebar -->
         

    </div>
</div>
