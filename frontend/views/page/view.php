<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\Page
 */
$this->title = $model_lang['title'];

$this->registerMetaTag([
    'name' => 'description',
    'content' => strip_tags($model_lang['description'])
]);

$this->registerMetaTag([
    'name' => 'keywords',
    'content' =>  $model_lang['keywords']
]);
if($model->img_heder_path != "")
    $this->params['hederimage'] = $model->getHederImageUrl();
?>

<div class="content">
	<div class="bg-title-block">
            <h1 class="bg-title-block-h1"><span>
	       <?php echo $model_lang['title'] ?></span>
	    </h1>
	 </div>
    <div class="page_content">
        <?php echo $model_lang['body']; ?>
    </div>
</div>