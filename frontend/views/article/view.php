<?php
/* @var $this yii\web\View */
/* @var $model common\models\Article */

use frontend\widgets\BgLinks;


$this->title = $model_lang['title'];

$this->registerMetaTag([
    'name' => 'description',
    'content' => strip_tags($model_lang['description'])
]);

$this->registerMetaTag([
    'name' => 'keywords',
    'content' =>  $model_lang['keywords']
]);
if($model->heder_image_path != "")
    $this->params['hederimage'] = $model->getHederImageUrl();

//$this->params['hederimage'] = $model->getImageUrl();
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] =  $model_lang['title'];
?>


<div class="row">
    <div class=" col-lg-9  col-md-8  col-sm-12 col-xs-12 content-block">
        <article class="article-item">
           
                <h1><?php echo $model_lang['title']; ?></h1>
                 <div class="news-meta">
                    <time class="news_time">
                        <?php echo Yii::$app->formatter->asDatetime($model->created_at,'php:d/m/Y'); ?>
                    </time>
                </div>
                <?php if ($model->thumbnail_path): ?>
                    <?php echo \yii\helpers\Html::img(
                       // Yii::$app->glide->createSignedUrl([
                           // 'glide/index',
                            //'path' => $model->thumbnail_path,
                            //'path' => $model->getImageUrl(),
                        //], true),
                        $model->getImageUrl(),
                        ['class' => 'img-responsive']
                    ) ?>
                <?php endif; ?>
                <div class="news_content">
                    <?php echo $model_lang['body']; ?>
                </div>
            
            
            <div class="text-center">
              <a href="/article" class="catalog-btn"><?php echo Yii::t('frontend','All news'); ?></a>
            </div>
        </article>
    </div>
    <div class=" col-lg-3  col-md-4  hidden-sm hidden-xs right-sidbar">

            <?php echo \frontend\widgets\DbCatalog::widget([
              'key' => 'sidebar-category-list'
            ]) ?>
            <?php echo BgLinks::widget(); ?>
            <!-- 
            <div class="bg-widget-inform">
                <?php //echo \frontend\widgets\DbSiteInfo::widget([
                 // 'key' => 'sidebar-info'
                //]) ?>
            </div> -->
    </div>
</div>
<!--  Dispay the footer for small windows -->
<div class="row visible-xs visible-sm right-sidbar-widjet-sm">
  
                <?php echo BgLinks::widget([
                  'key' => 'bg-link-page'
                  ]); ?>

</div>
