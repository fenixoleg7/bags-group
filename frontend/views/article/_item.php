<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\Article
 */
use yii\helpers\Html;

?>

<div class="news-img-prev col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
    <?php if ($model->thumbnail_path): ?>
        <?php echo Html::img(
            Yii::$app->glide->createSignedUrl([
                'glide/index',
               'path' => $model->thumbnail_path,             
            ], true)
            //$model->getImageUrl()
           
        ) ;?>
       <?php //Yii::$app->glide->outputImage($model->thumbnail_path, ['w' => 100])?>
    <?php endif; ?>
</div>
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    <?php if(Yii::$app->language == 'ru') :?>

        <?php echo Html::tag('h2', $model->title_ru) ?>
        <div class="news-meta">
            <time class="news_time">
                <?php echo Yii::$app->formatter->asDatetime($model->created_at,'php:d/m/Y'); ?>
            </time>
        </div>
        <div class="news_content">
            <?php echo \yii\helpers\StringHelper::truncate(strip_tags($model->body_ru), 250, '...', null, false); ?>
        </div>

    <?php elseif(Yii::$app->language == 'en'): ?>

        <?php echo Html::tag('h2', $model->title_en) ?>
        <div class="news-meta">
            <time class="news_time">
                <?php echo Yii::$app->formatter->asDatetime($model->created_at,'php:d/m/Y'); ?>
            </time>
        </div>
        <div class="news_content">
            <?php echo \yii\helpers\StringHelper::truncate(strip_tags($model->body_en), 250, '...', null, false); ?>
        </div>

    <?php else: ?>
        <?php echo Html::tag('h2', $model->title_de) ?>

        <div class="news-meta">
            <time class="news_time">
                <?php echo Yii::$app->formatter->asDatetime($model->created_at,'php:d/m/Y'); ?>
            </time>
        </div>
        <div class="news_content">
            <?php echo \yii\helpers\StringHelper::truncate(strip_tags($model->body_de), 250, '...', null, false); ?>
        </div>

    <?php endif; ?>

    <div class="read_more_a pull-right">
        <?php echo Html::a(Yii::t('frontend','Read completely'), ['view', 'slug'=>$model->slug]); ?>
    </div>
</div>


