<?php
/* @var $this yii\web\View */

use frontend\widgets\BgLinks;

$this->title = $seodate_lang['title'];
$this->registerMetaTag([
    'name' => 'description',
    'content' => strip_tags($seodate_lang['description'])
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $seodate_lang['keywords']
]);
$this->params['hederimage'] = $seomodel->getImageUrl();

?>


<div class="marg_left row">
    <div class=" col-xs-12 col-sm-12 col-md-9 col-lg-9 bg-title-block">
        <h1 class="bg-title-block-h1">
           <?php echo Yii::t('frontend', 'Articles') ?>
        </h1>
    </div>
</div>
<div class="marg_left row">
    <div class=" col-lg-9  col-md-8  col-sm-12 col-xs-12 content-block">
    <?php echo \yii\widgets\ListView::widget([
        'dataProvider'=>$dataProvider,
        'itemOptions' => ['class' => 'row item-news-block'],
        'pager'=>[
            'hideOnSinglePage'=>true,
        ],
        'layout' => "{items}\n<div class='b-pagenation n-table hidden-sm hidden-xs'>{pager}</div>",
        'itemView'=>'_item'
    ])?>
</div>
<div class=" col-lg-3  col-md-4  hidden-sm hidden-xs right-sidbar">
            
             <?php echo \frontend\widgets\DbCatalog::widget([
              'key' => 'sidebar-category-list'
            ]) ?>
            
            <?php echo BgLinks::widget(); ?>
            
    </div>
</div>
<!--  Dispay the footer for small windows -->
<div class="row visible-xs visible-sm">
  <?php echo BgLinks::widget([
      'key' => 'bg-link-page'
      ]); ?>
</div>